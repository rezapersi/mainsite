<?php

namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Confirmmadarek;
use App\Discountcode;
use App\Order;
use App\Reportbagmoney;
use App\Reportedituser;
use App\Requestt;
use App\Rizfacture;
use App\Service;
use App\Setting;
use App\Useddiscountcode;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Api_message\Api_message;
use App\Paymentzarinpal\Paymentzarinpal;
use App\Paymentidpay\Paymentidpay;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;





class RejectmadarekControllers extends Controller
{
    public function rejectmadarekkarshenas(Request $request)
    {

      
        $idkarshenad = $request->idkarshenad;
        $detuser = User::where('id', $idkarshenad)->first();
        $imgnationalcode = $detuser->imgnationalcode;
        $imgmaharat = $detuser->imgmaharat;
        $imgjavazkasb = $detuser->imgjavazkasb;

        $path1 = public_path('images/usersimg/madarek/' . $imgnationalcode);
        if (file_exists($path1)) {
            unlink($path1);
        }

        $path2 = public_path('images/usersimg/madarek/' . $imgmaharat);
        if (file_exists($path2)) {
            unlink($path2);
        }

        $path3 = public_path('images/usersimg/madarek/' . $imgjavazkasb);
        if (file_exists($path3)) {
            unlink($path3);
        }

        User::where('id', $idkarshenad)->update([
            'statusconfirmmadarek' => 3,
        ]);
        Confirmmadarek::where('user_id', $idkarshenad)->delete();

    }

}
