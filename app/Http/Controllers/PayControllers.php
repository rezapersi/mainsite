<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\Requestt;
use App\Paymentzarinpal\Paymentzarinpal;
use App\Paymentidpay\Paymentidpay;


class Paycontrollers extends Controller
{

    public function index($pinsearch)
    {

        $check = Order::where('pinsearch', $pinsearch)->with('requestt')->first();
        if ($check) {

            $param = ['detailsbuy' => $check];
        } else {

            $param = ['detailsbuy' => ''];

        }
        return view('/pay/index', $param);

    }


    public function sendrequestpay(Request $request)
    {
        $pinsearch = $request->pinsearch;
        $typebank = $request->typebank;

        //چک کردن اینکه این سفارش موجود است یا نه با استفاده از پین سرچ جنریت شده نه id سفارش
        $check = Order::where('pinsearch', $pinsearch)->first();
        if ($check) {
            //اگر سفارشی یافت شد.************
            //گرفتن id درخواست
            $idorder = $check->id;
            $idrequest = $check->requestt_id;
            //گرفتن مبلغ  سفارش
            $price = $check->pricepay;
            //گرفتن شماره موبایل مشتری جهت ارسال sms
            $mobile = Requestt::find($idrequest)->phonenumber;
            $description = 'خرید';
            $email = '';


            //zarinpal
            if ($typebank == 1) {
                //از کلاس زرین پال نمونه میگیریم
                $payzarinpal = new Paymentzarinpal();
                //ارسال مقادیر ضروری به تابع درخواست زرین پال
                $resultrequest = $payzarinpal->zarinpalRequest($price, $description, $email, $mobile);
                $Status = $resultrequest['Status'];
                $Authority = $resultrequest['Authority'];
                $Error = $resultrequest['Error'];

//اگر وضعیت درخواست به زرین پال درست بود(100) ریدایرکت به درگاه پرداخت
                if ($Status == 100) {
                    //باید Authority دریافتی از زرین پال جهت استفاده در مراحل بعد را درجدول order آپدیت نماییم
                    Order::where('pinsearch', $pinsearch)->update(['Authority' => $Authority,'bank'=>'زرین پال']);
                    //انتقال به درگاه زرین پال

                    $zarinpalRedirecttoURL=Setting::first()->zarinpalRedirecttoURL;
                    return redirect($zarinpalRedirecttoURL.$Authority);

                } else {
                    Session::flash('errorpay', $Error);
                    return back();
                }

            }

            //idpay ***************************************************************************

            if ($typebank == 2) {

                //از کلاس ای دی پی  نمونه میگیریم
                $payidpay = new Paymentidpay();
                //ارسال مقادیر ضروری به تابع درخواست ای دی پی
                $resultrequest = $payidpay->idpayRequest($idorder, $price, '', $description, $email, $mobile);
                $Error = $resultrequest['Error'];
                $Authority = $resultrequest['Authority'];
                $link = $resultrequest['link'];

                if ($Error == '') {

                    //باید Authority دریافتی از ای دی پی  جهت استفاده در مراحل بعد را درجدول order آپدیت نماییم
                    Order::where('pinsearch', $pinsearch)->update(['Authority' => $Authority,'bank'=>'آی دی پی']);
                    //انتقال به درگاه  آی دی پی
                    return redirect($link);

                } else {

                    Session::flash('errorpay', $Error);
                    return back();
                }



            }


        } else {
            //اگر سفارشی یافت نشد***********.
//بازگت و نشان دادن خط            ا
            Session::flash('errorpay', 'درخواستی با این مشخصات یافت نشد.');
            return back();


        }


    }


}
