<?php

namespace App\Http\Controllers;

use App\Reportbagmoney;
use App\Reportcompany;
use App\Setting;
use App\Useddiscountcode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\Requestt;
use App\Paymentzarinpal\Paymentzarinpal;
use App\Paymentidpay\Paymentidpay;
use App\Api_message\Api_message;


class CheckoutControllers extends Controller
{

    public function checkout(Request $request)
    {
        $Authority = $request->Authority; //zarinpal
        if (!$Authority) {
            $Authority = $request->id; //idpay
        }
        $Status = $request->Status; //zarinpal
        $status = $request->status; //idpay

        if ($Status == 'OK' or $status == 10) {
            $check = Order::where('Authority',$Authority)->first();
            if ($check) {
                $datenow = jdate();
                $idorder = $check->id;
                $pricepay = $check->pricefacture - $check->discount;
                $bank = $check->bank;
                $typefacture = $check->typefacture_id;

                $iddiscountcode=$check->iddiscountcode;
                $discount=$check->discount;

                if ($bank == 'زرین پال') {
                    $object = new Paymentzarinpal();
                    $result = $object->zarinpalVerify($pricepay, $Authority);
                    $Status = $result['Status'];
                    $Error = $result['Error'];
                    $RefID = $result['RefID'];
                    if ($Status == 100) {

                        $this->affterpay($typefacture, $check, $pricepay);
                        Order::where('id', $idorder)->update(['datepay' => $datenow, 'pricepay' => $pricepay, 'receipt' => $RefID, 'statuspay_id' => 2]);

                        if ($iddiscountcode > 0 and $discount > 0) {
                            $checkcode = Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->first();
                            if ($checkcode) {
                                $beforenumberused = $checkcode->numberused;
                                Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->update([
                                    'numberused' => $beforenumberused + 1
                                ]);
                            } else {
                                Useddiscountcode::forcecreate([
                                    'numberused' => 1,
                                    'user_id' => Auth::id(),
                                    'discountcode_id' => $iddiscountcode
                                ]);

                            }
                        }


                        Session::flash('errorsuccess', 'پرداخت با موفقیت انجام شد.');
                        return redirect()->route('detailspay', ['id' => $idorder]);

                    } else {

                        Session::flash('errorpay', $Error);
                        return redirect()->route('detailspay');

                    }


                }
                //------------------------------idpay
                if ($bank == 'آی دی پی') {

                    $object = new Paymentidpay();
                    $result = $object->idpayVerify($Authority, $idorder);

                    $status = $result['status'];
                    $Error = $result['Error'];
                    $RefID = $result['RefID'];

                    if ($status == 100) {
                        $this->affterpay($typefacture, $check, $pricepay);
                        Order::where('id', $idorder)->update(['datepay' => $datenow, 'pricepay' => $pricepay, 'receipt' => $RefID, 'statuspay_id' => 2]);


                        if ($iddiscountcode > 0 and $discount > 0) {
                            $checkcode = Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->first();
                            if ($checkcode) {
                                $beforenumberused = $checkcode->numberused;
                                Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->update([
                                    'numberused' => $beforenumberused + 1
                                ]);
                            } else {
                                Useddiscountcode::forcecreate([
                                    'numberused' => 1,
                                    'user_id' => Auth::id(),
                                    'discountcode_id' => $iddiscountcode
                                ]);

                            }
                        }


                        Session::flash('errorsuccess', 'پرداخت  با موفقیت انجام شد.');
                        return redirect()->route('detailspay', ['id' => $idorder]);

                    } else {

                        Session::flash('errorpay', $Error);
                        return redirect()->route('detailspay');

                    }


                }


            } else {
                Session::flash('errorpay', 'سفارشی با این مشخصات یافت نگردید.');
                return redirect()->route('detailspay');
            }
        } else {
            //error idpay
            if ($status) {
                $ErrorsArray = array(
                    '1' => 'پرداخت انجام نشده است',
                    '2' => ' پرداخت ناموفق بوده است',
                    '3' => 'خطا رخ داده است',
                    '4' => 'بلوکه شده',
                    '5' => 'برگشت به پرداخت کننده',
                    '6' => 'برگشت خورده سیستمی',
                    '10' => ' در انتظار تایید پرداخت',
                    '100' => 'پرداخت تایید شده است',
                    '101' => ' پرداخت قبلا تایید شده است',
                    '200' => 'به دریافت کننده واریز شد',
                );

                $Error = $ErrorsArray[$status];
                Session::flash('errorpay', $Error);
            } else {
                Session::flash('errorpay', 'خطای پرداخت . ');
            }

            return redirect()->route('detailspay');


        }


    }


    public function affterpay($typefacture, $order, $pricepay)
    {


        $beforecashdesk = Setting::first()->cashdesk;
        if ($typefacture == 1) {

            //karshenas
            $idrequest = $order->idrequest;
            $idkarshenas = Requestt::find($idrequest)->idkarshenas;
            $findkarshenas = User::find($idkarshenas);
            $beforebagmoneykarshenas = $findkarshenas->bagmoney;
            User::where('id', $idkarshenas)->update([
                'bagmoney' => $beforebagmoneykarshenas + $order->pricefacture,
            ]);

            Reportbagmoney::forcecreate([
                'title' => 'هزینه درخواست ' . $order->trackingcoderequest . ' ',
                'date' => jdate(),
                'factorcode' => $order->numberorder_showcustomer,
                'price' => $order->pricefacture,
                'newbagmoney' => User::find($idkarshenas)->bagmoney,
                'typereport_id' => 1,
                'user_id' => $idkarshenas,
            ]);


            Reportcompany::forcecreate([
                'title' => 'هزینه درخواست ' . $order->trackingcoderequest . ' ',
                'date' => jdate(),
                'factorcode' => $order->numberorder_showcustomer,
                'price' => $pricepay,
                'newbagmoney' => $beforecashdesk + $pricepay,
                'typereport_id' => 1,
            ]);

            Setting::where('id', 1)->update([
                'cashdesk' => $beforecashdesk + $pricepay
            ]);


            $idrequest = $order->idrequest;
            Requestt::where('id', $idrequest)->update([
                'statusrequest_id' =>5,
            ]);


            /*            $phonecontact = Setting::first()->phonecontact;
                        $namesait = Setting::first()->namesait;
                        $message = 'کارشناس عزیز ، در خواست (' . $order->trackingcoderequest . ') با موفقیت پرداخت گردید .
            با تشکر ' . $namesait . '.
            ' . $phonecontact . '';

                        $sendmessage = Api_message::sendmessage($findkarshenas->mobile, $message);*/

        }

        //charge bagmoney
        if ($typefacture == 2) {

            User::where('id', Auth::id())->update([
                'bagmoney' => Auth::user()->bagmoney + $pricepay,
            ]);

            Reportbagmoney::forcecreate([
                'title' => 'افزایش اعتبار',
                'date' => jdate(),
                'factorcode' => $order->numberorder_showcustomer,
                'price' => $pricepay,
                'newbagmoney' => User::find(Auth::id())->bagmoney,
                'typereport_id' => 1,
                'user_id' => Auth::id(),
            ]);


            Reportcompany::forcecreate([
                'title' => 'افزایش اعتبار',
                'date' => jdate(),
                'factorcode' => $order->numberorder_showcustomer,
                'price' => $pricepay,
                'newbagmoney' => $beforecashdesk + $pricepay,
                'typereport_id' => 1,
            ]);

            Setting::where('id', 1)->update([
                'cashdesk' => $beforecashdesk + $pricepay
            ]);


        }


        //kharid ghete
        if ($typefacture == 3) {


        }


    }


}
