<?php

namespace App\Http\Controllers;

use App\Confirmmadarek;
use App\Discountcode;
use App\Order;
use App\Reportbagmoney;
use App\Reportedituser;
use App\Requestmoney;
use App\Requestt;
use App\Rizfacture;
use App\Service;
use App\Setting;
use App\Useddiscountcode;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Api_message\Api_message;
use App\Paymentzarinpal\Paymentzarinpal;
use App\Paymentidpay\Paymentidpay;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


}
