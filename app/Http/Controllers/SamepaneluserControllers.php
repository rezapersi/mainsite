<?php

namespace App\Http\Controllers;

use App\Confirmmadarek;
use App\Discountcode;
use App\Order;
use App\Reportbagmoney;
use App\Reportedituser;
use App\Requestmoney;
use App\Requestt;
use App\Rizfacture;
use App\Service;
use App\Setting;
use App\Useddiscountcode;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Api_message\Api_message;
use App\Paymentzarinpal\Paymentzarinpal;
use App\Paymentidpay\Paymentidpay;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class SamepaneluserControllers extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function getdetailsuser(Request $request)
    {

        return User::find(Auth::id());

    }

    public function updatedetailsuser(Request $request)
    {
        $type = $request->type;
        $mobile = $request->mobile;
        $email = $request->email;


        $name = $request->name;
        $family = $request->family;
        $nationalcode = $request->nationalcode;
        $namecompany = $request->namecompany;
        $phone = $request->phone;
        $codeposti = $request->codeposti;
        $address = $request->address;
        $ostan = $request->ostan;
        $city = $request->city;

        $namepage = $request->page;

        $error = [];


        if ($type == 'informationkarbari') {

            $emailbefore = Auth::user()->email;
            $mobilebefore = Auth::user()->mobile;

            if ($mobile !== $mobilebefore) {
                $checkbeforeinsertmobile = User::where('mobile', $mobile)->first();
                if ($checkbeforeinsertmobile) {
                    $error[] = 'شماره موبایل قبلا توسط شخص دیگری ثبت گردیده است.';
                } else {
                    $detuser = User::where('id', Auth::id())->first();
                    Reportedituser::forcecreate([
                        'user_id' => Auth::id(),
                        'date' => jdate(),
                        'beforemobile' => $detuser->mobile,
                        'newmobile' => $mobile,
                        'beforeemail' => $detuser->email,
                        'newemail' => $email,
                    ]);


                    User::where('id', Auth::id())->update([
                        'mobile' => $mobile
                    ]);


                }
            }

            if ($email !== $emailbefore) {
                $checkbeforeinsertemail = User::where('email', $email)->first();
                if ($checkbeforeinsertemail) {
                    $error[] = ' ایمیل قبلا توسط شخص دیگری ثبت گردیده است.';

                } else {

                    $detuser = User::where('id', Auth::id())->first();

                    Reportedituser::forcecreate([
                        'user_id' => Auth::id(),
                        'date' => jdate(),
                        'beforemobile' => $detuser->mobile,
                        'newmobile' => $mobile,
                        'beforeemail' => $detuser->email,
                        'newemail' => $email,
                    ]);

                    User::where('id', Auth::id())->update([
                        'email' => $email
                    ]);


                }
            }


        }


        if ($type == 'informationpersonal') {


            //karshenas
            if ($namepage == 'accountuser') {

                User::where('id', Auth::id())->update([

                    'name' => $name,
                    'family' => $family,
                    'nationalcode' => $nationalcode,
                    'namecompany' => $namecompany,
                    'phone' => $phone,
                    'codeposti' => $codeposti,
                    'address' => $address,
                    'ostan' => $ostan,
                    'city' => $city,

                ]);

            }

            //user (customer)
            if ($namepage == 'accountuser-panel') {

                User::where('id', Auth::id())->update([
                    'name' => $name,
                    'family' => $family,
                    'nationalcode' => $nationalcode,
                    'phone' => $phone,
                ]);

            }


        }


        return $error;

    }

    public function getdetailsrequest(Request $request)
    {
        $idrequest = $request->idrequest;
        return Requestt::where('id', $idrequest)->with('user', 'problem', 'reportrequestts')->get();

    }

    public function getallfacturepaneluser(Request $request)
    {
        return Order::where('user_id', Auth::id())->with('statuspay', 'typefacture', 'rizfactures')->orderBy('id', 'desc')->paginate(6);
    }

    public function getdetailsorder(Request $request)
    {

        $get = Order::where('id', $request->idorder)->with('typefacture')->get();
        foreach ($get as $key => $row) {

            $typefacture = $row->typefacture_id;
            if ($typefacture == 1) {
                $idrequest = $row->idrequest;
                $get[$key]['statusrate'] = Requestt::find($idrequest)->statusrate;
            }
        }

        return $get;

    }

    public function getrizfactures(Request $request)
    {

        return Rizfacture::where('order_id', $request->idorder)->get();

    }

    public function checkdiscountcode(Request $request)
    {
        $discountcode = $request->discountcode;
        if ($discountcode == '') {
            return ['error' => 'کد تخفیف را وارد نمایید.', 'success' => '', 'array' => ''];
        } else {

            $code = Discountcode::where('code', $discountcode)->first();
            if ($code) {
                $datenow = jdate()->format('%Y/%m/%d');
                $dateend = $code->dateend;
                if ($datenow > $dateend) {
                    return ['error' => 'مهلت استفاده از کد تخفیف پایان یافته است.', 'success' => '', 'array' => ''];
                } else {

                    $iddiscountcode = $code->id;
                    $maxused = $code->maxused;

                    $check = Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->first();
                    if ($check) {
                        $numberused = $check->numberused;
                        if ($numberused >= $maxused) {
                            return ['error' => 'شما از کد تخفیف استفاده نموده اید.', 'success' => '', 'array' => ''];
                        } else {
                            return ['error' => '', 'success' => 'کد تخفیف با موفقیت اعمال شد.', 'array' => $code];
                        }

                    } else {

                        return ['error' => '', 'success' => 'کد تخفیف با موفقیت اعمال شد.', 'array' => $code];
                    }


                }

            } else {
                return ['error' => 'کد تخفیف معتبر نمی باشد.', 'success' => '', 'code' => ''];
            }

        }


    }

    public function payfacture(Request $request)
    {

        $idfacture = $request->idfacture;
        $discountcode = $request->discountcode;
        $typepay = $request->typepay;
        $rate = $request->rate;


        $getfacture = Order::find($idfacture);
        $pricefacture = $getfacture->pricefacture;
        $typefacture = $getfacture->typefacture_id;


        if ($discountcode == '' or $discountcode == null) {
            $pricepay = $pricefacture;
            $takhfif = 0;
            $iddiscountcode = 0;
        } else {
            $back = $this->checkdiscountcode($request, ['discountcode' => $discountcode]);
            if ($back['error'] == '') {
                $array = $back['array'];
                $percent = $array['percent'];
                $typediscountcode = $array['typediscountcode_id'];
                $pricediscount = $array['price'];

                if ($typediscountcode == 1) {
                    $takhfif = $pricefacture * $percent / 100;
                } else {
                    $takhfif = $pricediscount;
                }

                $pricepay = $pricefacture - $takhfif;
                $iddiscountcode = $array['id'];
            } else {
                $pricepay = $pricefacture;
                $takhfif = 0;
                $iddiscountcode = 0;
            }
        }


        //typepay==0 bagmoney
        //typepay==1 bank
        if ($typepay == 0) {

            $bagmoney = Auth::user()->bagmoney;
            if ($pricepay > $bagmoney) {
                Session::flash('errorpay', 'موجودی کیف پول کافی نمی باشد.');
                return back();
            } else {
                //darkhast
                if ($typefacture == 1) {
                    $idrequest = $getfacture->idrequest;
                    $idkarshenas = Requestt::find($idrequest)->idkarshenas;
                    $findkarshenas = User::where('id', $idkarshenas)->with('services')->first();

                    //moshtari
                    User::where('id', Auth::id())->update([
                        'bagmoney' => Auth::user()->bagmoney - $pricepay,
                    ]);

                    if ($iddiscountcode > 0 and $takhfif > 0) {
                        $checkcode = Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->first();
                        if ($checkcode) {
                            $beforenumberused = $checkcode->numberused;
                            Useddiscountcode::where(['user_id' => Auth::id(), 'discountcode_id' => $iddiscountcode])->update([
                                'numberused' => $beforenumberused + 1
                            ]);
                        } else {
                            Useddiscountcode::forcecreate([
                                'numberused' => 1,
                                'user_id' => Auth::id(),
                                'discountcode_id' => $iddiscountcode
                            ]);

                        }


                    }


                    Reportbagmoney::forcecreate([
                        'title' => 'هزینه درخواست ' . $getfacture->trackingcoderequest . ' ',
                        'date' => jdate(),
                        'factorcode' => $getfacture->numberorder_showcustomer,
                        'price' => $pricepay,
                        'newbagmoney' => User::find(Auth::id())->bagmoney,
                        'typereport_id' => 2,
                        'user_id' => Auth::id(),
                    ]);


                    //karshenas
                    $beforebagmoneykarshenas = $findkarshenas->bagmoney;
                    User::where('id', $idkarshenas)->update([
                        'bagmoney' => $beforebagmoneykarshenas + $pricefacture,
                    ]);

                    Reportbagmoney::forcecreate([
                        'title' => 'هزینه درخواست ' . $getfacture->trackingcoderequest . ' ',
                        'date' => jdate(),
                        'factorcode' => $getfacture->numberorder_showcustomer,
                        'price' => $pricefacture,
                        'newbagmoney' => User::find($idkarshenas)->bagmoney,
                        'typereport_id' => 1,
                        'user_id' => $idkarshenas,
                    ]);

                    Order::where('id', $idfacture)->update([
                        'discount' => $takhfif,
                        'pricepay' => $pricepay,
                        'datepay' => jdate(),
                        'statuspay_id' => 2,
                    ]);


                    Requestt::where('id', $idrequest)->update([
                        'statusrequest_id' => 5,
                    ]);


                    //rate to karshenas
                    if ($rate !== null) {
                        $beforerate = $findkarshenas->rate;
                        $numberrequest = sizeof(Requestt::where('idkarshenas', $idkarshenas)->get());
                        $services = $findkarshenas->services;
                        $userincat = [];
                        foreach ($services as $row) {
                            $get = Service::where('id', $row->id)->with('users')->first();
                            foreach ($get->users as $row2) {
                                $userincat[] = $row2->id;
                            }
                        }
                        //delete Repeat array
                        $userincat = array_unique($userincat, SORT_REGULAR);
                        $numberuserincategory = sizeof($userincat);
                        $k = ($numberrequest / $numberuserincategory) + 1;
                        $newrate = $beforerate + (($k * $rate) / 10);

                        Requestt::where('id', $idrequest)->update([
                            'statusrate' => 1,
                            'rate' => $rate,
                        ]);

                        User::where('id', $idkarshenas)->update([
                            'rate' => $newrate,
                        ]);

                    }


                    //send sms to karshenas
                    /*       $phonecontact = Setting::first()->phonecontact;
                           $namesait = Setting::first()->namesait;

                           $message = 'کارشناس عزیز ، در خواست (' . $getfacture->trackingcoderequest . ') با موفقیت پرداخت گردید .
       با تشکر ' . $namesait . '.
       ' . $phonecontact . '';

                           $sendmessage = Api_message::sendmessage($findkarshenas->mobile, $message);*/
                    Session::flash('successpay', 'صورت حساب ' . $getfacture->trackingcoderequest . ' با موفقیت پرداخت شد.');
                    return back();

                }
                //kharid ghete
                if ($typefacture == 3) {


                }

            }


        } else {

            $typebank = 1;

            $mobile = Auth::user()->mobile;
            if ($typefacture == 1) {

                $idrequest = $getfacture->idrequest;
                $idkarshenas = Requestt::find($idrequest)->idkarshenas;
                $findkarshenas = User::where('id', $idkarshenas)->with('services')->first();

                $description = 'هزینه درخواست';
                if ($rate !== null) {
                    $beforerate = $findkarshenas->rate;
                    $numberrequest = sizeof(Requestt::where('idkarshenas', $idkarshenas)->get());
                    $services = $findkarshenas->services;
                    $userincat = [];
                    foreach ($services as $row) {
                        $get = Service::where('id', $row->id)->with('users')->first();
                        foreach ($get->users as $row2) {
                            $userincat[] = $row2->id;
                        }
                    }
                    //delete Repeat array
                    $userincat = array_unique($userincat, SORT_REGULAR);
                    $numberuserincategory = sizeof($userincat);
                    $k = ($numberrequest / $numberuserincategory) + 1;
                    $newrate = $beforerate + (($k * $rate) / 10);

                    Requestt::where('id', $idrequest)->update([
                        'statusrate' => 1,
                        'rate' => $rate,
                    ]);

                    User::where('id', $idkarshenas)->update([
                        'rate' => $newrate,
                    ]);

                }


            }

            if ($typefacture == 2) {
                $description = 'افزایش اعتبار';
            }
            if ($typefacture == 3) {
                $description = 'خرید قطعه';
            }
            if ($typefacture == 4) {
                $description = 'تسویه حساب';
            }
            $email = Auth::user()->email;


            //zarinpal
            if ($typebank == 1) {
                $payzarinpal = new Paymentzarinpal();
                $resultrequest = $payzarinpal->zarinpalRequest($pricepay, $description, $email, $mobile);
                $Status = $resultrequest['Status'];
                $Authority = $resultrequest['Authority'];
                $Error = $resultrequest['Error'];

                if ($Status == 100) {
                    Order::where('id', $idfacture)->update(['Authority' => $Authority, 'bank' => 'زرین پال', 'discount' => $takhfif, 'iddiscountcode' => $iddiscountcode]);

                    $zarinpalRedirecttoURL = Setting::first()->zarinpalRedirecttoURL;
                    return redirect($zarinpalRedirecttoURL . $Authority);

                } else {
                    Session::flash('errorpay', $Error);
                    return back();
                }
            }
            //idpay
            if ($typebank == 2) {

                $payidpay = new Paymentidpay();
                $resultrequest = $payidpay->idpayRequest($idfacture, $pricepay, '', $description, $email, $mobile);
                $Error = $resultrequest['Error'];
                $Authority = $resultrequest['Authority'];
                $link = $resultrequest['link'];

                if ($Error == '') {
                    //باید Authority دریافتی از ای دی پی  جهت استفاده در مراحل بعد را درجدول order آپدیت نماییم
                    Order::where('id', $idfacture)->update(['Authority' => $Authority, 'bank' => 'آی دی پی', 'discount' => $takhfif, 'iddiscountcode' => $iddiscountcode]);
                    //انتقال به درگاه  آی دی پی
                    return redirect($link);
                } else {

                    Session::flash('errorpay', $Error);
                    return back();
                }


            }


        }


    }

    public function getreportbagmoney(Request $request)
    {
        return Reportbagmoney::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(6);
    }

    public function insertrequestmoney(Request $request)
    {

        $numbershaba = $request->numbershaba;
        $requestmoney = $request->requestmoney;
        $checkbeforerequest = Requestmoney::where(['user_id' => Auth::id(), 'statusrequestmoney_id' => 1])->first();

        $bagmoney = Auth::user()->bagmoney;


        if ($checkbeforerequest) {
            return ['status' => 'error', 'message' => 'شما قبلا درخواستی ثبت نموده اید، لطفا بعد انجام درخواست قبلی مجددا درخواست  خود را ثبت نمایید.', 'bagmoney' => $bagmoney];
        } else {

            //1:check bagmoney
            if ($bagmoney < $requestmoney) {
                return ['status' => 'error', 'message' => 'مبلغ درخواستی بیشتر از موجودی کیف پول شما است.', 'bagmoney' => $bagmoney];
            } else {

                $newbagmoney = $bagmoney - $requestmoney;
                User::where('id', Auth::id())->update([
                    'bagmoney' => $newbagmoney,
                    'moneyblocked' => $requestmoney,
                    'statusrequestmoney' => 1,
                ]);

                $saveorder = Order::forcecreate(
                    [
                        'pricefacture' => $requestmoney,
                        'discount' => 0,
                        'bank' => '---',
                        'numberorder_sendbank' => $this->generatenumberfacture(),
                        'numberorder_showcustomer' => $this->generatenumberfactureforshowcustomer(),
                        'datefactor' => jdate(),
                        'typefacture_id' => 4,
                        'statuspay_id' => 1,
                        'Authority' => '---',
                        'user_id' => Auth::id(),
                    ]
                );

                $savereportbagmoney = Reportbagmoney::forcecreate([
                    'title' => 'درخواست وجه',
                    'date' => jdate(),
                    'factorcode' => $saveorder->numberorder_showcustomer,
                    'price' => $requestmoney,
                    'newbagmoney' => $newbagmoney,
                    'typereport_id' => 2,
                    'user_id' => Auth::id(),
                ]);

                Requestmoney::forcecreate(
                    [
                        'price' => $requestmoney,
                        'numbershaba' => $numbershaba,
                        'daterequest' => jdate(),
                        'idreportbagmoney' => $savereportbagmoney->id,
                        'idorder' => $saveorder->id,
                        'numberfacture' => $saveorder->numberorder_showcustomer,
                        'statusrequestmoney_id' => 1,
                        'user_id' => Auth::id(),
                    ]
                );
                return ['status' => 'ok', 'message' => 'درخواست شما با موفقیت ثبت گردید.', 'bagmoney' => $newbagmoney];

            }


        }


    }

    public function getrequestmoney(Request $request)
    {
        return Requestmoney::with('statusrequestmoney')->orderBy('id', 'desc')->paginate(6);
    }




    public function showmessagecancelrequestmoney(Request $request)
    {
        $idrequest=$request->idrequest;
       $requestmoney=Requestmoney::find($idrequest);
        return ['messagecancelrequest'=>$requestmoney->messagecancelrequest];
    }



    public function increasecredit(Request $request)
    {


        $pricepay = $request->price;
        $typebank = 1; //default zarinpal
        $mobile = Auth::user()->mobile;
        $description = 'افزایش اعتبار';
        $email = Auth::user()->email;

        //zarinpal
        if ($typebank == 1) {

            $payzarinpal = new Paymentzarinpal();
            $resultrequest = $payzarinpal->zarinpalRequest($pricepay, $description, $email, $mobile);
            $Status = $resultrequest['Status'];
            $Authority = $resultrequest['Authority'];
            $Error = $resultrequest['Error'];

            if ($Status == 100) {
                $check = Order::where(['user_id' => Auth::id(), 'typefacture_id' => 2, 'statuspay_id' => 1])->first();

                if ($check) {
                    Order::where('id', $check->id)->update(['pricefacture' => $pricepay, 'Authority' => $Authority, 'bank' => 'زرین پال', 'discount' => 0]);
                } else {

                    Order::forcecreate(
                        [
                            'pricefacture' => $pricepay,
                            'discount' => 0,
                            'bank' => 'زرین پال',
                            'numberorder_sendbank' => $this->generatenumberfacture(),
                            'numberorder_showcustomer' => $this->generatenumberfactureforshowcustomer(),
                            'datefactor' => jdate(),
                            'typefacture_id' => 2,
                            'statuspay_id' => 1,
                            'Authority' => $Authority,
                            'user_id' => Auth::id(),
                        ]
                    );
                }


                $zarinpalRedirecttoURL = Setting::first()->zarinpalRedirecttoURL;
                return redirect($zarinpalRedirecttoURL . $Authority);

            } else {
                Session::flash('errorpay', $Error);
                return back();
            }
        }

        //idpay
        if ($typebank == 2) {

            $payidpay = new Paymentidpay();
            $resultrequest = $payidpay->idpayRequest($this->generatenumberfacture(), $pricepay, '', $description, $email, $mobile);
            $Error = $resultrequest['Error'];
            $Authority = $resultrequest['Authority'];
            $link = $resultrequest['link'];

            if ($Error == '') {
                //باید Authority دریافتی از ای دی پی  جهت استفاده در مراحل بعد را درجدول order آپدیت نماییم

                $check = Order::where(['user_id' => Auth::id(), 'typefacture_id' => 2, 'statuspay_id' => 1])->first();
                if ($check) {
                    Order::where('id', $check->id)->update(['pricefacture' => $pricepay, 'Authority' => $Authority, 'bank' => 'آی دی پی', 'discount' => 0]);

                } else {

                    Order::forcecreate(
                        [
                            'pricefacture' => $pricepay,
                            'discount' => 0,
                            'bank' => 'آی دی پی',
                            'numberorder_sendbank' => $this->generatenumberfacture(),
                            'numberorder_showcustomer' => $this->generatenumberfactureforshowcustomer(),
                            'datefactor' => jdate(),
                            'Authority' => $Authority,
                            'typefacture_id' => 2,
                            'statuspay_id' => 1,
                            'user_id' => Auth::id(),
                        ]
                    );

                }


                //انتقال به درگاه  آی دی پی
                return redirect($link);
            } else {

                Session::flash('errorpay', $Error);
                return back();
            }


        }


    }

    public function changepassworduser(Request $request)
    {
        $message = [
            'password.required' => 'پسورد را وارد نمایید.',
            'password.string' => 'پسور باید ترکیبی از اعداد و حروف باشد.',
            'password.min' => 'پسورد باید بیشتر از 6 کارکتر باشد.',
            'password.confirmed' => 'پسورد ها با هم مطابقت ندارند.',
        ];

        $rules = [
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];


        $valid = Validator::make($request->all(), $rules, $message);

        if ($valid->fails()) {

            return ['Error' => $valid->errors('password'), 'Success' => ''];

        } else {

            $hashpassword = Hash::make($request->password);
            User::where('id', Auth::id())->update(['password' => $hashpassword]);
            return ['Error' => '', 'Success' => 'تغییر پسورد با موفقیت انجام شد.'];

        }


    }

    public function generatenumberfacture()
    {

        $pin = mt_rand(100000, 999999) . mt_rand(1, 9);
        return str_shuffle($pin);
    }

    public function generatenumberfactureforshowcustomer()
    {
        $pin = mt_rand(10000, 99999) . mt_rand(1, 9);
        return 'DYT-' . str_shuffle($pin);
    }

}
