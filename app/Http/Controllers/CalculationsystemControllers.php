<?php

namespace App\Http\Controllers;

use App\Devicecomponent;
use App\Pricecalculatemodeldevice;
use App\Pricecalculateservice;
use App\Servicecomponnentproduct;
use Illuminate\Http\Request;
use App\Maincat;
use App\Setting;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


use App\Pricecalculatemaincat;


class CalculationsystemControllers extends Controller
{

    public function index()
    {
        $khadamat = Maincat::with('problems')->get();
        $pricecalculatemaincat = Pricecalculatemaincat::all();
        $param = ['khadamat' => $khadamat, 'pricecalculatemaincat' => $pricecalculatemaincat, 'namepage' => 'calculationsystem'];
        return view('/service_price', $param);
    }


    public function pricecalculatemaincatchosed(Request $request)
    {
        $idmaincat = $request->idmaincat;
        return Pricecalculatemaincat::where('id', $idmaincat)->with('pricecalculateservices', 'pricecalculatebernaddevices')->first();

    }

    public function getpricecalculatemodeldevice(Request $request)
    {
        $idberand = $request->idberand;
        return Pricecalculatemodeldevice::where('pricecalculatebernaddevice_id', $idberand)->get();
    }

    public function cecalculatecost(Request $request)
    {
        $idmaincat = $request->idmaincat;
        $idberand = $request->idberand;
        $idservice = $request->idservice;
        $idmodeldevice = $request->idmodeldevice;

        //first get product device
        $getdevice = Pricecalculatemodeldevice::find($idmodeldevice);
        $namedevice = $getdevice->title;
        $pricedevice = $getdevice->pricedevice;
        if ($namedevice !== 'سایر') {

            $getservice = Pricecalculateservice::find($idservice);
            $typecalculate = $getservice->typecalculate_id;
            $travelexpenses = Setting::first()->travelexpenses; // hazine ayab zahab
            $fixedwageservice = $getservice->fixedwage; //dastmozd sabet
            $nerkh_0_500k = $getservice['0_500000'];
            $nerkh_500k_1500000m = $getservice['500000_1500000'];
            $nerkh_1500000m_3m = $getservice['1500000_3000000'];
            $nerkh_3m_5m = $getservice['3000000_5000000'];
            $nerkh_5m_8m = $getservice['5000000_8000000'];
            $nerkh_8m_12m = $getservice['8000000_12000000'];
            $nerkh_12m_20m = $getservice['12000000_20000000'];
            $nerkh_20m_up = $getservice['20000000_1000000000'];


            //gereftan tamam ghatat mahsol ya dastgah


            $getallcomponnetdevice = Devicecomponent::where('pricecalculatemodeldevice_id', $idmodeldevice)->with('servicecomponnentproducts')->get();
            if (sizeof($getallcomponnetdevice) > 0) {

                $idsfinalcomponnetdevice = [];
                foreach ($getallcomponnetdevice as $row) {
                    $id = $row->id;
                    $get = Servicecomponnentproduct::where(['devicecomponent_id' => $id, 'pricecalculateservice_id' => $idservice])->get();
                    if (sizeof($get) > 0) {
                        $idsfinalcomponnetdevice[] = $id;
                    }
                }

                $totalpricecomponnent = 0;
                foreach ($idsfinalcomponnetdevice as $iddevicecomponnet) {
                    $de = Devicecomponent::where('id', $iddevicecomponnet)->with('componnentbank')->first();
                    $pricecomponnet = $de['componnentbank']->price;
                    $totalpricecomponnent = $totalpricecomponnent + $pricecomponnet;
                }

            } else {

                $totalpricecomponnent = 0;

            }


            //dastmozd sabet
            if ($typecalculate == 1) {
                $pricekol = ($fixedwageservice + $totalpricecomponnent + $travelexpenses);
                $dastmozdtamir=$fixedwageservice;

            } else {

                if ($pricedevice >= 0 and $pricedevice < 500000) {
                    $baze = $nerkh_0_500k;

                }

                if ($pricedevice >= 500000 and $pricedevice < 1500000) {
                    $baze = $nerkh_500k_1500000m;
                }

                if ($pricedevice >= 1500000 and $pricedevice < 3000000) {
                    $baze = $nerkh_1500000m_3m;
                }

                if ($pricedevice >= 3000000 and $pricedevice < 5000000) {
                    $baze = $nerkh_3m_5m;
                }

                if ($pricedevice >= 5000000 and $pricedevice < 8000000) {
                    $baze = $nerkh_5m_8m;
                }

                if ($pricedevice >= 8000000 and $pricedevice < 12000000) {
                    $baze = $nerkh_8m_12m;
                }

                if ($pricedevice >= 12000000 and $pricedevice < 20000000) {
                    $baze = $nerkh_12m_20m;
                }

                if ($pricedevice >= 20000000 and $pricedevice < 1000000000) {
                    $baze = $nerkh_20m_up;
                }

                if ($typecalculate == 2) {
                    // gheymati
                    $nerkh=$baze;
                    $drsad = 0;
                    $dastmozdtamir=$nerkh;

                } else {
                    // darsadi
                    $drsad = $baze;
                    $nerkh=0;
                    $dastmozdtamir=$pricedevice * ($drsad / 100);
                }

                $pricekol = ($travelexpenses + $totalpricecomponnent+$nerkh) + ($pricedevice * ($drsad / 100));

            }
            return ['pricekol'=>$pricekol,'dastmozdtamir'=>$dastmozdtamir,'totalpricecomponnent'=>$totalpricecomponnent,'travelexpenses'=>$travelexpenses,'status'=>'ok'];

        } else {

            return ['pricekol'=>0,'dastmozdtamir'=>0,'totalpricecomponnent'=>0,'travelexpenses'=>0,'status'=>'notfounddevice'];

        }


    }


}
