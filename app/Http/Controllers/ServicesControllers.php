<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


use App\Maincat;
use App\Problem;//this is servise category for  send request


class ServicesControllers extends Controller
{

    
    public function Index($id)
    {

        if($id==''){
            return redirect('/');
        }


        $khadamat = Maincat::with('problems')->get();
        $khedmat = Maincat::where('id',$id)->with('problems')->first();
        $param = ['khadamat'=>$khadamat,'khedmat'=>$khedmat,'namepage'=>'service'];
        return view('/service', $param);

    }



}
