<?php

namespace App\Http\Controllers;

use App\Addressuser;
use App\Bernaddevice;
use App\Modeldevice;
use App\Confirmmadarek;
use App\Maincat;
use App\Order;
use App\Problem;//this is servise category for  send request
use App\Reportrequestt;
use App\Requestt;
use App\Rizfacture;
use App\Service; //this is service karshenas
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Api_message\Api_message;
use App\Statusmessage;
use Illuminate\Support\Facades\Crypt;

class PaneluserControllers extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'Accesstopaneluser']);
    }


    public function dashboard(Request $request)
    {

        $activerequest=Requestt::where('user_id',Auth::id())->whereIn('statusrequest_id',[3,4])->with( 'maincat', 'bernaddevice', 'modeldevice')->orderBy('id', 'desc')->take(2)->get();
        $param = ['namepage' => 'dashboarduser','activerequest'=>$activerequest];
        return view('/paneluser/dashboard/index', $param);
    }

    public function accountuser(Request $request)
    {

        $service = Service::all();
        $param = ['namepage' => 'accountuser-panel', 'service' => $service];
        return view('/paneluser/accountuser/index', $param);
    }

    public function facture(Request $request)
    {
        $param = ['namepage' => 'facture'];
        return view('/paneluser/facture/index', $param);
    }


    public function managerequests(Request $request)
    {
        $param = ['namepage' => 'managerequests-panel'];
        return view('/paneluser/managerequests/index', $param);
    }

    public function getrequestcustomer(Request $request)
    {

        $get = Requestt::where('user_id', Auth::id())->with('statusrequest', 'maincat', 'bernaddevice', 'modeldevice', 'problem')->orderBy('id', 'desc')->paginate(6);
        $v = json_decode(json_encode($get), true);
        $b = $v['data'];

        foreach ($b as $key => $row) {

            $idkarshenas = $row['idkarshenas'];

            if ($idkarshenas > 0) {
                $detuser = User::find($idkarshenas);
                $b[$key]['namekarshenas'] = ' ' . $detuser->name . ' ' . $detuser->family . '';
            } else {
                $b[$key]['namekarshenas'] = 'مشخص نشده';
            }


        }

        $get['newdata'] = $b;
        return $get;


    }

    public function getreportsrequestcustomer(Request $request)
    {
        $idrequest = $request->idrequest;
        return Reportrequestt::where('requestt_id', $idrequest)->orderBy('id', 'desc')->paginate(4);
    }


    public function bagmoney(Request $request)
    {
        $param = ['namepage' => 'bagmoney'];
        return view('/paneluser/bagmoney/index', $param);
    }



    public function insertaddress(Request $request)
    {
        $codeposti = $request->codeposti;
        $address = $request->address;
        $ostan = $request->ostan;
        $city = $request->city;
        $plak = $request->plak;
      
      if(Auth::user()){

        Addressuser::forcecreate([
            'ostan' => $ostan,
            'city' => $city,
            'address' => $address,
            'codeposti' => $codeposti,
            'plak' => $plak,
            'user_id' => Auth::id(),
        ]);

        $getlastaddres=Addressuser::where('user_id', Auth::id())->orderBy('id','desc')->first();

        return ['idaddress'=>$getlastaddres->id,'status'=>'ok'];
        

      }else{

        return ['idaddress'=>'','status'=>'error'];

      }
      
      
      



    }

    public function getaddress(Request $request)
    {
        return Addressuser::where('user_id', Auth::id())->get();
    }

    public function removeaddress(Request $request)
    {
        Addressuser::where('id', $request->id)->delete();
    }


    public function insertrequest()
    {


        $maincat = Maincat::all();
        $problem=Problem::all();
        $addresuser=Addressuser::where('user_id', Auth::id())->get();

        $param = ['namepage' => 'insertrequest', 'maincat' => $maincat,'problem'=>$problem,'addresuser'=>$addresuser];
        return view('/paneluser/insertrequest/index', $param);

    }

 







     //تابع ساخت کد پیگیری
    public function generatetrackingcode()
    {

        $pin = mt_rand(10000, 99999) . mt_rand(1, 9);
        return 'DY-' . str_shuffle($pin);
    }



    public
    function delevery($recId)
    {
        $Deliver = Api_message::GetDelivery($recId);
        $Resultcode = $Deliver['Resultcode'];
        $Error = $Deliver['Error'];
        Statusmessage::where(['recId' => $recId])->update(['titlestatusdelivery' => $Error, 'codestatusdelivery' => $Resultcode]);
        return ['Error' => $Error, 'recId' => $recId];
    }


}
