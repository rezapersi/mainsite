<?php

namespace App\Http\Controllers;

use App\Confirmmadarek;
use App\Order;
use App\Reportrequestt;
use App\Requestt;
use App\Rizfacture;
use App\Service;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Api_message\Api_message;

class PanelkarshenasanControllers extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','Accesstopanelkarshenas']);
    }


    public function dashboard(Request $request)
    {
        $param = ['namepage' => 'dashboardkarshenasan'];
        return view('/panelkarshenasan/dashboard/index', $param);
    }

    public function accountuser(Request $request)
    {

        $service = Service::all();
        $detailskarshenas=Auth::user();
        $param = ['namepage' => 'accountuser', 'service' => $service,'detailskarshenas'=>$detailskarshenas];
        return view('/panelkarshenasan/accountuser/index', $param);
    }


    public function updateservicekarshenas(Request $request)
    {

        $idservice = $request->servicekarshenas;
        $user = User::find(Auth::id());
        $user->services()->sync($idservice);
        return back();

    }

    public function updatemadarekkarshenas(Request $request)
    {

        $imgnationalcart = $request->imgnationalcart;
        $imgmaharat = $request->imgmaharat;
        $imgjavaz = $request->imgjavaz;

        $message = [
            'imgnationalcart.required' => ' کارت ملی  را انتخاب نمایید.',
            'imgmaharat.required' => ' گواهی مهارت  را انتخاب نمایید.',
            'imgjavaz.required' => ' جواز کسب  را انتخاب نمایید.',
        ];
        $rules = [

            'imgnationalcart' => 'required',
            'imgmaharat' => 'required',
            'imgjavaz' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {


            Session::flash('erroruploadimg', 'تصاویر را به طور کامل ارسال نمایید.');
            return back();


        } else {


            $getnameimgkartmali = $imgnationalcart->getclientoriginalname();
            $newnamekartmali = time() . '_' . $getnameimgkartmali;

            $getnameimgmaharat = $imgmaharat->getclientoriginalname();
            $newnamemaharat = time() . '_' . $getnameimgmaharat;

            $getnameimgjavaz = $imgjavaz->getclientoriginalname();
            $newnamejavaz = time() . '_' . $getnameimgjavaz;


            $extkartmali = $imgnationalcart->getclientoriginalextension();
            $extmaharat = $imgmaharat->getclientoriginalextension();
            $extjavaz = $imgjavaz->getclientoriginalextension();


            $getsizekartmali = $imgnationalcart->getsize();
            $getsizemaharat = $imgmaharat->getsize();
            $getsizejavaz = $imgjavaz->getsize();


            if ($extkartmali == 'jpg' or $extkartmali == 'png' or $extkartmali == 'jpeg' or $extkartmali == 'gif' and $getsizekartmali <= 530000) {
                $canuploadkartmeli = 'yes';
            } else {
                $canuploadkartmeli = 'no';
            }

            if ($extmaharat == 'jpg' or $extmaharat == 'png' or $extmaharat == 'jpeg' or $extmaharat == 'gif' and $getsizemaharat <= 530000) {
                $canuploadmaharat = 'yes';
            } else {
                $canuploadmaharat = 'no';
            }


            if ($extjavaz == 'jpg' or $extjavaz == 'png' or $extjavaz == 'jpeg' or $extjavaz == 'gif' and $getsizejavaz <= 530000) {
                $canuploadjavaz = 'yes';
            } else {
                $canuploadjavaz = 'no';

            }

            if ($canuploadkartmeli == 'yes' and $canuploadmaharat == 'yes' and $canuploadjavaz == 'yes') {

                User::where('id', Auth::id())->update([
                    'imgnationalcode' => $newnamekartmali,
                    'imgmaharat' => $newnamemaharat,
                    'imgjavazkasb' => $newnamejavaz,
                    'statusconfirmmadarek' => 1,
                ]);


                $imgnationalcart->move('images/usersimg/madarek/', $newnamekartmali);
                $imgmaharat->move('images/usersimg/madarek/', $newnamemaharat);
                $imgjavaz->move('images/usersimg/madarek/', $newnamejavaz);

                Confirmmadarek::forcecreate([
                    'user_id' => Auth::id()
                ]);

                Session::flash('successuploadimg', 'تصاویر با موفقیت ارسال شد.');
                return back();


            }


        }


    }


    public function managerequests(Request $request)
    {
        $param = ['namepage' => 'managerequests'];
        return view('/panelkarshenasan/managerequests/index', $param);
    }


    public function getamarrequest(Request $request)
    {

        $request = Requestt::where('idkarshenas', Auth::id())->get();
        $numberrequestkol = sizeof($request);

        $request2 = Requestt::where(['idkarshenas' => Auth::id(), 'statusrequest_id' => 3])->get();
        $numberrequestdarhalanjam = sizeof($request2);

        $request3 = Requestt::where(['idkarshenas' => Auth::id(), 'statusrequest_id' => 4])->get();
        $numberrequestanjamshode = sizeof($request3);

        return ['numberrequestkol' => $numberrequestkol, 'numberrequestdarhalanjam' => $numberrequestdarhalanjam, 'numberrequestanjamshode' => $numberrequestanjamshode];


    }

    public function getrequesttkarshenasan(Request $request)
    {
        return Requestt::where('idkarshenas', Auth::id())->with('statusrequest', 'maincat', 'bernaddevice', 'modeldevice', 'problem')->orderBy('id','desc')->paginate(6);
    }


    public function confirmrequestbykarshenas(Request $request)
    {
        $idrequest = $request->idrequest;

        Requestt::where('id', $idrequest)->update([
            'statusrequest_id' => 3
        ]);


    }


    public function getreportsrequest(Request $request)
    {
        $idrequest = $request->idrequest;
        return Reportrequestt::where('requestt_id', $idrequest)->orderBy('id', 'desc')->paginate(4);
    }


    public function insertreportrequest(Request $request)
    {
        $report = $request->report;
        $idrequest = $request->idrequest;
        Reportrequestt::forcecreate([
            'requestt_id' => $idrequest,
            'date' => jdate(),
            'report' => $report,
        ]);

    }

    public function deletereportrequest(Request $request)
    {
        $idrequest = $request->idrequest;
        Reportrequestt::where('id', $idrequest)->delete();
    }


    public function sodorfactureforrequest(Request $request)
    {

        $idrequest = $request->idrequest;
        $babat = $request->babat;
        $price = $request->price;

        $message = [
            'idrequest.required' => ' ',
            'babat.required' => '',
            'price.required' => '',
        ];
        $rules = [
            'idrequest' => 'required',
            'babat' => 'required',
            'price' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            Session::flash('errorinsertfacture', 'خطا در ثبت فاکتور');
            return back();
        } else {

            $detrequest = Requestt::where('id', $idrequest)->with('user')->get();

            $save = Order::forcecreate(
                [
                    'numberorder_sendbank' => $this->generatenumberfacture(),
                    'numberorder_showcustomer' => $this->generatenumberfactureforshowcustomer(),
                    'datefactor' => jdate(),
                    'idrequest' => $idrequest,
                    'typefacture_id' => 1,
                    'statuspay_id' => 1,
                    'user_id' => $detrequest[0]->user->id,
                    'trackingcoderequest' => $detrequest[0]->trackingcode,
                ]
            );


            $pricefacture = 0;
            foreach ($babat as $key => $row) {
                Rizfacture::forcecreate(
                    [
                        'babat' => $row,
                        'price' => $price[$key],
                        'order_id' => $save->id,
                    ]
                );
                $pricefacture = $pricefacture + $price[$key];

            }
            Order::where('id', $save->id)->update([
                'pricefacture' => $pricefacture

            ]);

            Requestt::where('id', $idrequest)->update([
                'pricefacture' => $pricefacture,
                'statusrequest_id' => 4
            ]);

            //send sms to moshtari
            $phonecontact = Setting::first()->phonecontact;
            $namesait = Setting::first()->namesait;


            $message = '  صورت حسابی توسط کارشناس تعمیر دستگاه  با شماره درخواست ' . $detrequest[0]->trackingcode . ' ثبت گردید.لطفا با ورود به پنل کاربری خود اقدام به پرداخت صورت حساب نمایید.
با تشکر ' . $namesait . '.
' . $phonecontact . '';



try {
               
    $sendmessage = Api_message::sendmessage($detrequest[0]->user->id, $message);
    $resultsms = $sendmessage['resultsms'];
    $recId = $sendmessage['recId'];
    $Error = $sendmessage['Error'];

    Session::flash('successinsertfacture', 'فاکتور با موفقیت برای درخواست ' . $detrequest[0]->trackingcode . ' ثبت شد.');
    return back();

} catch(\Exception $e) {
    Session::flash('successinsertfacture', 'فاکتور با موفقیت برای درخواست ' . $detrequest[0]->trackingcode . ' ثبت شد.');
    return back();

}


            

           
        }


    }


    public function getfacturerequest(Request $request)
    {
        $idrequest = $request->idrequest;
        $get = Order::where('idrequest', $idrequest)->with('rizfactures', 'statuspay')->get();

        foreach ($get as $key => $row) {
            $rizfactures = $row->rizfactures;

            $pricekol = 0;
            foreach ($rizfactures as $riz) {
                $pricekol = $pricekol + $riz->price;
            }

            $get[$key]['pricekol'] = $pricekol;

        }

        return $get;

    }


    public function moneymanagement(Request $request)
    {
        $param = ['namepage' => 'bagmoney'];
        return view('/panelkarshenasan/bagmoney/index', $param);
    }

    public
    function partsbank(Request $request)
    {
        $param = ['namepage' => 'partsbank'];
        return view('/panelkarshenasan/partsbank/index', $param);
    }


    public
    function support(Request $request)
    {
        $param = ['namepage' => 'support'];
        return view('/panelkarshenasan/support/index', $param);
    }


    public function facture(Request $request)
    {
        $param = ['namepage' => 'facture'];
        return view('/panelkarshenasan/facture/index', $param);
    }

    public function bagmoney(Request $request)
    {
        $param = ['namepage' => 'bagmoney'];
        return view('/panelkarshenasan/bagmoney/index', $param);
    }








    public function generatenumberfacture()
    {

        $pin = mt_rand(100000, 999999) . mt_rand(1, 9);
        return str_shuffle($pin);
    }

    public function generatenumberfactureforshowcustomer()
    {
        $pin = mt_rand(10000, 99999) . mt_rand(1, 9);
        return 'DYT-' . str_shuffle($pin);
    }





}







