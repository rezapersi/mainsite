<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Setting;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected function authenticated(Request $request, $user)
    {
       
        $role = Auth::user()->leveluser_id;
        if ($role == '5') {
            return redirect('/panelkarshenasan/managerequests');
        } elseif ($role == '7') {
            return redirect('/panel/dashboard');
        }
        else {
            return redirect('https://admin.dgyr.ir/login');
        }


    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/login';


    public function username()
    {

        return 'mobile';
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
