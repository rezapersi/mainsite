<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addressuser;
use App\Bernaddevice;
use App\Modeldevice;
use App\Confirmmadarek;
use App\Maincat;
use App\Order;
use App\Problem;//this is servise category for  send request
use App\Reportrequestt;
use App\Requestt;
use App\Rizfacture;
use App\Service; //this is service karshenas
use App\Setting;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Api_message\Api_message;
use App\Statusmessage;
use Illuminate\Support\Facades\Crypt;

class InsertrequestControllers extends Controller
{


    public function getservice(Request $request)
    {

        $idmaincat = $request->idmaincat;
        $search = $request->post('search');
        if ($search) {

            return Problem::where('title', 'like', '%' . $search . '%')->where('maincat_id', $idmaincat)->paginate(6);

        } else {


            return Problem::where('maincat_id', $idmaincat)->paginate(6);

        }


    }

    public function getberand(Request $request)
    {
        $idmaincat = $request->idmaincat;
        $search = $request->post('search');


        if ($search) {

            return Bernaddevice::where('title', 'like', '%' . $search . '%')->where('maincat_id', $idmaincat)->paginate(6);

        } else {


            return Bernaddevice::where('maincat_id', $idmaincat)->paginate(6);

        }


    }

    public function getmodel(Request $request)
    {

        $idberand = $request->idberand;
        $search = $request->post('search');

        if ($search) {

            return Modeldevice::where('title', 'like', '%' . $search . '%')->where('bernaddevice_id', $idberand)->paginate(6);

        } else {


            return Modeldevice::where('bernaddevice_id', $idberand)->paginate(6);

        }


    }


    public function detailsuser()
    {

        return User::find(Auth::id());

    }


    public function updateuser(Request $request)
    {

        $mobile = $request->mobile;
        $name = $request->name;
        $family = $request->family;


        if (Auth::user()) {
            $mobilebefore = Auth::user()->mobile;
            if ($mobile !== $mobilebefore) {
                $checkbeforeinsertmobile = User::where('mobile', $mobile)->first();
                if ($checkbeforeinsertmobile) {
                    return ['alert' => 'شماره موبایل قبلا توسط شخص دیگری ثبت گردیده است.'];
                } else {

                    $detuser = User::where('id', Auth::id())->first();
                    Reportedituser::forcecreate([
                        'user_id' => Auth::id(),
                        'date' => jdate(),
                        'beforemobile' => $detuser->mobile,
                        'newmobile' => $mobile,
                        'beforeemail' => $detuser->email,

                    ]);

                    User::where('id', Auth::id())->update([

                        'name' => $name,
                        'family' => $family,
                        'mobile' => $mobile,

                    ]);

                    return ['alert' => 'ok'];

                }

            } else {


                User::where('id', Auth::id())->update([

                    'name' => $name,
                    'family' => $family,
                    'mobile' => $mobile,

                ]);

                return ['alert' => 'ok'];

            }

        } else {

            return ['alert' => 'لطفا مجددا وارد حساب کاربری خود شوید'];

        }


    }


    public function addrequestbyuser(Request $request)
    {

        $khadamat = $request->khadamat;
        $service = $request->service;
        $berand = $request->berand;
        $model = $request->model;
        $description = $request->description;
        $idaddress = $request->idaddress;

        $addressuser = Addressuser::where('id', $idaddress)->first();

        $iduser = Auth::id();

        $ostan = $addressuser->ostan;
        $city = $addressuser->city;
        $plak = $addressuser->plak;
        $codeposti = $addressuser->codeposti;
        $address = $addressuser->address;

        $datenow = jdate();

        $namesait = Setting::first()->namesait;
        $phonecontact = Setting::first()->phonecontact;

        $trackingcode = $this->generatetrackingcode();
        $save = Requestt::forcecreate([
            'description' => $description,
            'trackingcode' => $trackingcode,
            'user_id' => $iduser,
            'ostan' => $ostan,
            'city' => $city,
            'address' => Crypt::encryptString($address),
            'plak' => $plak,
            'codeposti' => $codeposti,
            'maincat_id' => $khadamat,
            'bernaddevice_id' => $berand,
            'modeldevice_id' => $model,
            'problem_id' => $service,
            'typerequestt_id' => 2,
            'statusrequest_id' => 1,
            'date' => $datenow
        ]);


        $getrequestt = Requestt::find($save->id);

        $namecustomers = Auth::user()->name . '' . Auth::user()->family;

        $mobile = Auth::user()->mobile;


        $message = '' . $namecustomers . ' عزیز درخواست شما با موفقیت ثبت شد.
    کارشناسان ما در حال بررسی مشکل شما می باشند.
    لطفا منتظر بمانید.
    با تشکر ' . $namesait . '.
    کدپیگیری:' . $trackingcode . '
    ' . $phonecontact . '';


        try {
            $sendmessage = Api_message::sendmessage($mobile, $message);
            $resultsms = $sendmessage['resultsms'];
            $recId = $sendmessage['recId'];
            $Error = $sendmessage['Error'];


            if ($resultsms == 0) {
                $idrequest = $save->id;
                Statusmessage::forcecreate(['requestt_id' => $idrequest, 'date' => $datenow, 'mobile' => $mobile, 'message' => $message, 'statussend' => $Error, 'recId' => $recId]);
                //get Deliver
                $this->delevery($recId);

            }


            return ['trackingcode' => $trackingcode];

        } catch (\Exception $e) {

            return ['trackingcode' => $trackingcode];


        }


    }


    public function insertrequestbygust(Request $request)
    {

        $mobile = $request->mobile;
        $name = $request->name;
        $family = $request->family;
        $khadamat = $request->khadamat;
        $service = $request->service;
        $berand = $request->berand;
        $model = $request->model;
        $description = $request->description;
        $codeposti = $request->codeposti;
        $address = $request->address;
        $ostan = $request->ostan;
        $city = $request->city;
        $plak = $request->plak;

        $datenow = jdate();

        $checkuser = User::where('mobile', $mobile)->first();
        $phonecontact = Setting::first()->phonecontact;
        $namesait = Setting::first()->namesait;
        if ($checkuser) {

            //check user or not
            $level=$checkuser->leveluser_id;
            if($level==5){
                $caninsertrequest='no';
                $iduser ='0';
            }else{
                $caninsertrequest='yes';
                $iduser = $checkuser->id;
                User::where('id', $iduser)->update([
                    'name' => $name,
                    'family' => $family,
                ]);
            }



        } else {
            $caninsertrequest='yes';
            $password = $this->generatepassword();
            $saveuseruser = User::forcecreate([
                'name' => $name,
                'family' => $family,
                'mobile' => $mobile,
                'password' => bcrypt($password),
                'bagmoney'=>0,
                'moneyblocked'=>0,
                'leveluser_id' => 7,
            ]);
            $iduser = $saveuseruser->id;
            //insert address
            Addressuser::forcecreate([
                'ostan' => $ostan,
                'city' => $city,
                'address' => $address,
                'plak' => $plak,
                'codeposti' => $codeposti,
                'user_id' => $iduser,
            ]);
        }


        if($caninsertrequest=='yes'){

            $trackingcode = $this->generatetrackingcode();
            $save = Requestt::forcecreate([

                'description' => $description,
                'trackingcode' => $trackingcode,
                'user_id' => $iduser,
                'ostan' => $ostan,
                'city' => $city,
                'address' => Crypt::encryptString($address),
                'plak' => $plak,
                'codeposti' => $codeposti,
                'maincat_id' => $khadamat,
                'bernaddevice_id' => $berand,
                'modeldevice_id' => $model,
                'problem_id' => $service,
                'typerequestt_id' => 1,
                'statusrequest_id' => 1,
                'date' => $datenow
            ]);
            $getrequestt = Requestt::find($save->id);
            $trackingcode = $getrequestt->trackingcode;
            $namecustomers = $name . ' ' . $family;
            if ($checkuser) {
                //ارسال پیام به مشتری
                $message = '' . $namecustomers . ' عزیز درخواست شما با موفقیت ثبت شد.
کارشناسان ما در حال بررسی مشکل شما می باشند.
لطفا منتظر بمانید.
با تشکر ' . $namesait . '.
کدپیگیری:' . $trackingcode . '
' . $phonecontact . '';
            } else {

                $message = '' . $namecustomers . ' عزیز درخواست شما با موفقیت ثبت شد.
کارشناسان ما در حال بررسی مشکل شما می باشند.
لطفا منتظر بمانید.
اطلاعات ورود به پنل کاربری :
یوزرنیم:' . $mobile . '
پسورد:' . $password . '
کدپیگیری درخواست:' . $trackingcode . '
با تشکر ' . $namesait . '.
' . $phonecontact . '';
            }

            try {

                $sendmessage = Api_message::sendmessage($mobile, $message);
                $resultsms = $sendmessage['resultsms'];
                $recId = $sendmessage['recId'];
                $Error = $sendmessage['Error'];
                if ($resultsms == 0) {
                    $idrequest = $save->id;
                    Statusmessage::forcecreate(['requestt_id' => $idrequest, 'date' => $datenow, 'mobile' => $mobile, 'message' => $message, 'statussend' => $Error, 'recId' => $recId]);
                    //get Deliver
                    $this->delevery($recId);
                }

                return ['status'=>'success','trackingcode' => $trackingcode];

            } catch (\Exception $e) {
                return ['status'=>'success','trackingcode' => $trackingcode];
            }



        }else{
            return ['status'=>'error','trackingcode' => null];
        }




    }


    public function generatepassword()
    {
        $pin = mt_rand(10000, 99999) . mt_rand(1, 99);
        return str_shuffle($pin);
    }


    //تابع ساخت کد پیگیری
    public function generatetrackingcode()
    {

        $pin = mt_rand(10000, 99999) . mt_rand(1, 9);
        return 'DY-' . str_shuffle($pin);
    }


    public
    function delevery($recId)
    {
        $Deliver = Api_message::GetDelivery($recId);
        $Resultcode = $Deliver['Resultcode'];
        $Error = $Deliver['Error'];
        Statusmessage::where(['recId' => $recId])->update(['titlestatusdelivery' => $Error, 'codestatusdelivery' => $Resultcode]);
        return ['Error' => $Error, 'recId' => $recId];
    }

    public function getmaincatwithidservice(Request $request)
    {

        $idservice = $request->idservice;
        return Problem::find($idservice)->maincat_id;


    }

}
