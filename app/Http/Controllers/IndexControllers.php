<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Requestt;
use App\Setting;
use App\Api_message\Api_message;
use App\Statusmessage;
use App\Postblog;
use App\Email;
use Illuminate\Support\Facades\Crypt;
use App\Maincat;
use App\Problem;//this is servise category for  send request

class IndexControllers extends Controller
{

    public function shop()
    {

        if (auth()->user()) {
            return redirect('/shop');
        } else {
          dd('لطفا لاگین کنید');

        }
    }


    public function Index()
    {
        $postblog = Postblog::orderBy('ID', 'desc')->take(3)->get();
        foreach ($postblog as $key => $row) {

            //$final = preg_replace('#(<p>).*?(</p>)#', '$1$2', $row->post_content);

            $filter = [
                '<p>',
                '</p>',
                '<!-- wp:paragraph -->',
                '<!-- /wp:paragraph -->',
                '<!-- wp:quote -->',
                '<!-- /wp:quote -->',
                '<blockquote class="wp-block-quote">',
                '</blockquote>',
                '<!-- wp:heading -->',
                '<!-- /wp:heading -->',
                '<h1>',
                '</h1>',
                '<h2>',
                '</h2>',
                '<h3>',
                '</h3>',
                '<h4>',
                '</h4>',
                '<h5>',
                '</h5>',
                '<h6>',
                '</h6>',
                '<!-- wp:heading {"level":3} -->',
            ];
            $final = str_replace($filter, '', $row->post_content);
            $postblog[$key]['content'] = $final;
        }

        $khadamat = Maincat::with('problems')->get();

        $param = ['postblog' => $postblog, 'khadamat' => $khadamat, 'namepage' => 'index'];
        return view('/index', $param);

    }

    public function validdetailsformsaverequest(Request $request)
    {
        $validate = $this->validformrequest($request->all());
        return $validate->errors();

    }

    public function validformrequest($request)
    {
        $message = [
            'name.required' => 'لطفا نام خود را به صورت کامل وارد نمایید.',
            'name.string' => 'نام خود را به صورت صحیح وارد نمایید.',
            'family.required' => 'لطفا نام خانوادگی خود را  به صورت کامل وارد نمایید.',
            'family.string' => 'نام خانوادگی خود را به صورت صحیح وارد نمایید.',
            'phonenumber.required' => 'شماره موبایل نباید خالی باشد. ',
            'phonenumber.numeric' => 'فرمت شماره موبایل صحیح نمی باشد.',
            'phonenumber.digits' => ' شماره موبایل باید 11 رقم باشد.',
            'address.required' => 'آدرس نباید خالی باشد.',
            'description.required' => 'توضیحات نباید خالی باشد.',
        ];
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'family' => ['required', 'string', 'max:255'],
            'phonenumber' => ['required', 'numeric', 'digits:11'],
            'address' => ['required'],
            'description' => ['required'],
        ];
        return $validator = Validator::make($request, $rules, $message);


    }


    public function saveformrequest(Request $request)
    {

        $name = $request->name;
        $family = $request->family;
        $phonenumber = $request->phonenumber;
        $address = $request->address;
        $description = $request->description;
        $datenow = jdate();

        $validate = $this->validformrequest($request->all());
        if ($validate->fails()) {
            return ['error' => 'ثبت درخواست با خطا مواجه شد لطفا فرم را با دقت تکمیل نمایید.', 'success' => ''];
        } else {

            $trackingcode = $this->generatetrackingcode();
            //افزودن درخواست
            $save = Requestt::forcecreate([
                'namecustomers' => $name,
                'familycustomers' => $family,
                'phonenumber' => Crypt::encryptString($phonenumber),
                'address' => $address,
                'address' => Crypt::encryptString($address),
                'description' => $description,
                'trackingcode' => $trackingcode,
                'user_id' => 1,
                'statusrequest_id' => 1,
                'date' => $datenow
            ]);
            $getrequestt = Requestt::find($save->id);
            $trackingcode = $getrequestt->trackingcode;
            $namecustomers = $getrequestt->namecustomers;
            $phonecontact = Setting::first()->phonecontact;
            $namesait = Setting::first()->namesait;

            //ارسال پیام به مشتری

            $message = '' . $namecustomers . ' عزیز درخواست شما با موفقیت ثبت شد.
کارشناسان ما در حال بررسی مشکل شما می باشند.
لطفا منتظر بمانید.
با تشکر ' . $namesait . '.
کدپیگیری:' . $trackingcode . '
' . $phonecontact . '';


            try {

                $sendmessage = Api_message::sendmessage($phonenumber, $message);
                $resultsms = $sendmessage['resultsms'];
                $recId = $sendmessage['recId'];
                $Error = $sendmessage['Error'];
                if ($resultsms == 0) {
                    $idrequest = $save->id;
                    Statusmessage::forcecreate(['requestt_id' => $idrequest, 'date' => $datenow, 'mobile' => $phonenumber, 'message' => $message, 'statussend' => $Error, 'recId' => $recId]);
                    //get Deliver
                    $this->delevery($recId);
                }

                return ['error' => '', 'success' => 'درخواست با موفقیت ثبت  گردید.منتظر پاسخ توسط تیم پشتیبانی باشید.'];


            } catch (\Exception $e) {

                return ['error' => '', 'success' => 'درخواست با موفقیت ثبت  گردید.منتظر پاسخ توسط تیم پشتیبانی باشید.'];


            }


        }

    }


    //تابع ساخت کد پیگیری
    public function generatetrackingcode()
    {

        $pin = mt_rand(10000, 99999) . mt_rand(1, 9);
        return 'DY-' . str_shuffle($pin);
    }


    public function delevery($recId)
    {
        $Deliver = Api_message::GetDelivery($recId);
        $Resultcode = $Deliver['Resultcode'];
        $Error = $Deliver['Error'];
        Statusmessage::where(['recId' => $recId])->update(['titlestatusdelivery' => $Error, 'codestatusdelivery' => $Resultcode]);
        return ['Error' => $Error, 'recId' => $recId];
    }


    public function saveemail(Request $request)
    {
        $message = [
            'email.required' => 'لطفا ایمیل خود را وارد نمایید.',
            'email.email' => 'فرمت ایمیل صحیح نیست.',
        ];
        $rules = [
            'email' => ['required', 'email'],
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return ['error' => $validator->errors('email')->first(), 'success' => ''];
        } else {

            $allemail = Email::all();
            $check = 'yes';
            foreach ($allemail as $row) {
                $email = $row->email;
                $decriptemail = Crypt::decryptString($email);
                if ($request->email == $decriptemail) {
                    $check = 'no';
                }
            }

            if ($check == 'yes') {
                $encriptemail = Crypt::encryptString($request->email);
                Email::forcecreate(['email' => $encriptemail]);
                return ['error' => '', 'success' => 'ایمیل شما با موفقیت ثبت گردید.'];
            } else {


                return ['error' => 'متاسفانه این ایمیل قبلا ثبت گردیده است ', 'success' => ''];


            }


        }


    }


    public function getsearchkhadamat(Request $request)
    {


        $search = $request->search;
        return Maincat::where('title', 'like', '%' . $search . '%')->get();


    }


}

