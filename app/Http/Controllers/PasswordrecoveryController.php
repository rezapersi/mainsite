<?php

namespace App\Http\Controllers;

use App\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Api_message\Api_message;

class PasswordrecoveryController extends Controller
{
    public function index()
    {
        $param = ['namepage' => 'passwordrecovery'];
        return view('/passwordrecovery/index', $param);
    }


    public function checkmobile(Request $request)
    {
        $mobile = $request->mobile;
        $detuser = User::where('mobile', $mobile)->first();
        if ($detuser) {
            $leveluser = $detuser->leveluser_id;
            $phonenumber = $mobile;
            $iduser = $detuser->id;
            $datesendverifiedcode = $detuser->datesendverifiedcode;
            $timesendverifiedcode = date('H:i:s', $detuser->timesendverifiedcode + 150);
            if ($datesendverifiedcode !== '' and $timesendverifiedcode !== '') {

                $datenow = jdate()->format('%Y/%m/%d');
                $timenow = date('H:i:s', time());


                if ($datenow == $datesendverifiedcode) {
                    if ($timenow > $timesendverifiedcode) {
                        return $this->sendverifiedcode($phonenumber, $iduser);
                    } else {


                        return ['error' => 'شما به تازگی کد تایید دریافت نموده اید.لطفا جهت دریافت مجدد کد تایید بعد از ساعت  ('.$timesendverifiedcode.') اقدام نمایید.', 'success' => '', 'status' => 'error'];
                    }

                } else {
                    return $this->sendverifiedcode($phonenumber, $iduser);
                }

            } else {

                return $this->sendverifiedcode($phonenumber, $iduser);

            }


        } else {
            return ['error' => 'شماره موبایل یافت نشد.', 'success' => '', 'status' => 'error'];
        }

    }


    public function sendverifiedcode($phonenumber, $iduser)
    {
        $verifiedcode = $this->generateverifiedcode();
        $phonecontact = Setting::first()->phonecontact;
        $namesait = Setting::first()->namesait;

        $message = ' کد تایید :' . $verifiedcode . '
با تشکر ' . $namesait . '.
' . $phonecontact . '';


try {

    $sendmessage = Api_message::sendmessage($phonenumber, $message);
    $resultsms = $sendmessage['resultsms'];
    $recId = $sendmessage['recId'];
    $Error = $sendmessage['Error'];

    if ($resultsms == 0) {

        User::where('id', $iduser)->update(['verifiedcode' => $verifiedcode,
            'datesendverifiedcode' => jdate()->format('%Y/%m/%d'),
            'timesendverifiedcode' => time(),]);

        return ['error' => '', 'success' => 'کد تایید به موبایل  ' . $phonenumber . ' ارسال گردید.', 'status' => 'success'];

    } else {

        return ['error' => 'خطا در ارسال کد تایید.', 'success' => '', 'status' => 'error'];

    }

} catch(\Exception $e) {

    return ['error' => 'خطا در ارسال کد تایید.', 'success' => '', 'status' => 'error'];

}




    }


    public function getagainverifiedcode(Request $request)
    {

        $mobile = $request->mobile;
        $backresult = $this->checkmobile(Request::create('string', 'post', ['mobile' => $mobile]));
        return $backresult;

    }


    public function checkverifiedcode(Request $request)
    {

        $mobile = $request->mobile;
        $verifiedcode = $request->verifiedcode;
        $detuser = User::where(['mobile' => $mobile, 'verifiedcode' => $verifiedcode])->first();
        if ($detuser) {
            return ['error' => '', 'success' => '', 'status' => 'success'];
        } else {
            return ['error' => ' کد اعتبارسازی نامعتبر است .', 'success' => '', 'status' => 'error'];
        }
    }


    public function changepasswordrecovery(Request $request)
    {

        $mobile = $request->mobile;
        $verifiedcode = $request->verifiedcode;
        $password = $request->password;
        $detuser = User::where(['mobile' => $mobile, 'verifiedcode' => $verifiedcode])->first();
        if ($detuser) {
            $hashpassword = bcrypt($password);
            User::where(['mobile' => $mobile, 'verifiedcode' => $verifiedcode])->update([
                'password' => $hashpassword
            ]);
            return ['error' => '', 'success' => 'پسورد با موفقیت تغییر یافت.', 'status' => 'success'];
        } else {
            return ['error' => 'خطا در بازیابی رمز عبور .', 'success' => '', 'status' => 'error'];
        }


    }


    public function generateverifiedcode()
    {
        $pin = mt_rand(10000, 99999);
        return str_shuffle($pin);
    }


}
