<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Setting;

class Accesstopaneluser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $role = Auth::user()->leveluser->id;
        if ($role == '7') {
        }else {
            return redirect('/login');
        }

        return $next($request);


    }
}
