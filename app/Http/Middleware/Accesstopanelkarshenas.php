<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Setting;

class Accesstopanelkarshenas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $role = Auth::user()->leveluser->id;
        if ($role == '5') {
        }else {
            return redirect('/login');
        }

        return $next($request);


    }
}
