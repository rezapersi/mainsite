<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {

        //     if (Auth::check()) {
        //         $role = Auth::user()->leveluser_id;

        //         if ($role == '5') {

        //             return redirect('/panelkarshenasan/managerequests');

        //         } elseif ($role == '7') {
        //             return redirect('/panel/dashboard');
        //         }
        //         else {
        //             return redirect('https://crm.dgyr.ir/login');
        //         }
        //     }

        // }

        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
        return $next($request);

    }
}
