<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Statusrequestmoney;
class Requestmoney extends Model
{

    public $timestamps=false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function statusrequestmoney()
    {
        return $this->belongsTo(Statusrequestmoney::class);
    }


}
