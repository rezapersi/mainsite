<?php

namespace App;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Requestt;
use App\Leveluser;
use App\Service;
use App\Confirmmadarek;
use App\Reportedituser;
use App\Order;
use App\Reportbagmoney;
use App\Addressuser;
use App\Useddiscountcode;
use App\Requestmoney;
class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function requests(){
        return $this->hasMany(Requestt::class);
    }

    public function leveluser(){
        return $this->belongsTo(Leveluser::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }


    public function services()
    {
        return $this->belongsToMany(Service::class);
    }


    public function confirmmadareks()
    {
        return $this->hasMany(Confirmmadarek::class);
    }

    public function reporteditusers()
    {
        return $this->hasMany(Reportedituser::class);
    }


    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function reportbagmoneys()
    {
        return $this->hasMany(Reportbagmoney::class);
    }

    public function addressusers()
    {
        return $this->hasMany(Addressuser::class);
    }

    public function useddiscountcodes()
    {
        return $this->hasMany(Useddiscountcode::class);
    }

    public function requestmoneys()
    {
        return $this->hasMany(Requestmoney::class);
    }

}
