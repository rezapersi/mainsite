<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\User;
class Service extends Model
{
    public $timestamps = false;
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
