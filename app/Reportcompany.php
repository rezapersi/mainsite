<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Typereport;
class Reportcompany extends Model
{

    public $timestamps = false;
    
    public function typereport()
    {
        return $this->belongsTo(Typereport::class);
    }
}
