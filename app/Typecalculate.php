<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pricecalculateservice;

class Typecalculate extends Model
{

    public function pricecalculateservices()
    {
        return $this->hasMany(Pricecalculateservice::class);
    }

}
