<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Requestmoney;

class Statusrequestmoney extends Model
{

    public $timestamps=false;

    public function requestmoneys(){
        return $this->hasMany(Requestmoney::class);
    }


}
