<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
class Typefacture extends Model
{
    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }


}
