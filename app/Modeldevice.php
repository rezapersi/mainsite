<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Requestt;
use App\Bernaddevice;

class Modeldevice extends Model
{
    public $timestamps = false;
    public function requestts()
    {
        return $this->hasMany(Requestt::class);
    }

    public function bernaddevice()
    {
        return $this->belongsTo(Bernaddevice::class);
    }



}
