<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Pricecalculatebernaddevice;
use App\Devicecomponent;

class Pricecalculatemodeldevice extends Model
{

    public function pricecalculatebernaddevice()
    {
        return $this->belongsTo(Pricecalculatebernaddevice::class);
    }

    public function devicecomponents()
    {
        return $this->hasMany(Devicecomponent::class);
    }


}
