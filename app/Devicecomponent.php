<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pricecalculatemodeldevice;
use App\Componnentbank;
use App\Servicecomponnentproduct;
class Devicecomponent extends Model
{

    public function pricecalculatemodeldevice()
    {
        return $this->belongsTo(Pricecalculatemodeldevice::class);
    }

    public function componnentbank()
    {
        return $this->belongsTo(Componnentbank::class);
    }

    public function servicecomponnentproducts()
    {
        return $this->hasMany(Servicecomponnentproduct::class);
    }



}
