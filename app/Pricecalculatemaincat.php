<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pricecalculatebernaddevice;
use App\Pricecalculateservice;

class Pricecalculatemaincat extends Model
{

    public function pricecalculateservices()
    {
        return $this->hasMany(Pricecalculateservice::class);
    }

    public function pricecalculatebernaddevices()
    {
        return $this->hasMany(Pricecalculatebernaddevice::class);
    }

}
