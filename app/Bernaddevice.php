<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Requestt;
use App\Maincat;
use App\Modeldevice;

class Bernaddevice extends Model
{

    public function requestts()
    {
        return $this->hasMany(Requestt::class);
    }


    public function maincat()
    {
        return $this->belongsTo(Maincat::class);
    }


    public function modeldevices()
    {
        return $this->hasMany(Modeldevice::class);
    }

}
