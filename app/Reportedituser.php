<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\User;
class Reportedituser extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
