<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Pricecalculatemaincat;
use App\Pricecalculatemodeldevice;
class Pricecalculatebernaddevice extends Model
{
    public $timestamps=false;

    public function pricecalculatemaincat()
    {
        return $this->belongsTo(Pricecalculatemaincat::class);
    }


    public function pricecalculatemodeldevices()
    {
        return $this->hasMany(Pricecalculatemodeldevice::class);
    }

}
