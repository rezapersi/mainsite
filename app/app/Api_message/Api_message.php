<?php

namespace App\Api_message;

use App\Setting;

//use nusoap_client;  //composer require econea/nusoap:dev-master;
//active soap in php.ini
use SoapClient;

class Api_message
{

    public static function webservice()
    {
        ini_set("soap.wsdl_cache_enabled", "0");
        return $sms_client = new SoapClient('http://payamak-service.ir/SendService.svc?wsdl', array('encoding' => 'UTF-8'));
    }

    public static function sendmessage($mobile, $message)
    {
        $sms_client = self::webservice();
        $param = [
            'userName' => Setting::first()->username_sendsms,
            'password' => Setting::first()->password_sendsms,
            'fromNumber' => Setting::first()->number_sendsms,
            'toNumbers' => [$mobile],
            'messageContent' => "$message",
            'isFlash' => false,
        ];
        $send = $sms_client->SendSMS($param);
        $resultsms = $send->SendSMSResult;
        $recId = '';
        if ($resultsms == 0) {
            $recId = $send->recId->long;
        }

        $ErrorsArray = array(
            '0' => 'ارسال با موفقیت انجام شد',
            '1' => 'نام کاربر یا کلمه عبور نامعتبر می باشد',
            '2' => 'کاربر مسدود شده است',
            '3' => 'شماره فرستنده نامعتبر است',
            '4' => 'محدودیت در ارسال روزانه',
            '5' => 'تعداد گیرندگان حداکثر 100 شماره می باشد',
            '6' => 'خط فرسنتده غیرفعال است',
            '7' => 'متن پیامک شامل کلمات فیلتر شده است',
            '8' => 'اعتبار کافی نیست',
            '9' => 'سامانه در حال بروز رسانی می باشد',
            '10' => 'وب سرویس غیرفعال است',
        );
        $Error = $ErrorsArray[$resultsms];
        $array = ['resultsms' => $resultsms, 'recId' => $recId, 'Error' => $Error];
        return $array;

    }

    public static function GetDelivery($recId)
    {
        $sms_client = self::webservice();
        $param = [
            'userName' => Setting::first()->username_sendsms,
            'password' => Setting::first()->password_sendsms,
            'recId' => $recId,
        ];
        $delivery = $sms_client->GetDelivery($param);
        $Resultcode = $delivery->GetDeliveryResult;

        $ErrorsArray = array(
            '-5' => ' برای گرفتن گزارش تحویل حداقل یک دقیقه بعد از ارسال اقدام نمایید',
            '-4' => 'به علت اینکه پیام در صف ارسال مخابرات می باشد، امکان گرفتن گزارش تحویل وجود ندارد',
            '-3' => 'به علت اینکه مهلت یک هفته ای گرفتن گزارش پایان یافته است، امکان گرفتن گزارش تحویل وجود ندارد',
            '-2' => ' پیام با این کد وجود ندارد (batchSmsId نامعتبر است)',
            '-1' => 'خطا در ارتباط با سرویس دهنده',
            '0' => 'ارسال شده به مخابرات',
            '1' => 'رسیده به گوشی',
            '2' => 'نرسیده به گوشی',
            '3' => 'خطای مخابراتی',
            '4' => 'خطای نامشخص',
            '5' => 'رسیده به مخابرات',
            '6' => 'نرسیده به مخابرات ',
            '7' => 'مسدود شده توسط مقصد',
            '8' => 'نامشخص',
            '9' => 'مخابرات پیام را مردود اعالم کرد',
            '10' => 'کنسل شده توسط اپراتور ',
            '11' => 'ارسال نشده',
        );
        $Error = $ErrorsArray[$Resultcode];
        $array = array('Resultcode' => $Resultcode, 'Error' => $Error);
        return $array;
    }

    public static function GetMessages($numbersend)
    {
        $sms_client = self::webservice();
        $param = [
            'userName' => Setting::first()->username_sendsms,
            'password' => Setting::first()->password_sendsms,
            'messageType' => '2',
            'fromNumbers' => [$numbersend],
            'index' => '',
            'count' => '100',
        ];
        $back = $sms_client->GetMessages($param)->GetMessagesResult;
        dd($back);
    }

}

?>