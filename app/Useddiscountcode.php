<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Discountcode;
use App\User;
class Useddiscountcode extends Model
{
    public $timestamps = false;
    
    public function discountcode()
    {
        return $this->belongsTo(Discountcode::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
