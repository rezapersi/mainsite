<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pricecalculatemaincat;
use App\Typecalculate;
use App\Servicecomponnentproduct;

//use App\Servicecomponent;
class Pricecalculateservice extends Model
{
    public function pricecalculatemaincat()
    {
        return $this->belongsTo(Pricecalculatemaincat::class);
    }

    public function typecalculate()
    {
        return $this->belongsTo(Typecalculate::class);
    }

   public function servicecomponnentproducts()
    {
        return $this->hasMany(Servicecomponnentproduct::class);
    }



}
