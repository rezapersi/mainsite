<?php

namespace App\Paymentidpay;

use App\Setting;

class Paymentidpay
{


    public function idpayRequest($orderid, $amount, $name, $description, $email, $phone)
    {


        $callback = Route('checkout');
        $params = array(
            'order_id' => $orderid, // *
            'amount' => $amount,//   *
            'name' => $name, //not important
            'phone' => $phone, //not important
            'mail' => $email, //not important
            'desc' => $description, //not important
            'callback' => $callback, // *
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-API-KEY: '.  $setting = Setting::first()->Apikeyidpay.'',  //api key for test 6a7f99eb-7c20-4412-a972-6dfb7cd253a4
            'X-SANDBOX: 1'  //for test sandbox 1
        ));
        $result_json = curl_exec($ch);
        curl_close($ch);
        $result_converttoarray = json_decode($result_json);
        //now we have stdclass . we mute convert with under code
        $result = get_object_vars($result_converttoarray);

        $Authority = '';
        $Error = '';
        $link = '';
        if (array_key_exists('id', $result)) {
            $Authority = $result['id'];
            $link = $result['link'];
        } else {
            $errorcode = $result['error_code'];
            $errormessage = $result['error_message'];
            $Error = $errormessage;
        }
        $array = array('Authority' => $Authority, 'Error' => $Error, 'link' => $link);
        return $array;

    }

    public function idpayVerify($Authority, $idorder)
    {


        $params = array(
            'id' => $Authority,
            'order_id' => $idorder,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment/verify');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-API-KEY: 6a7f99eb-7c20-4412-a972-6dfb7cd253a4',
            'X-SANDBOX: 1',
        ));

        $result_json = curl_exec($ch);
        curl_close($ch);
        $result_converttoarray = json_decode($result_json);
        //now we have stdclass . we mute convert with under code
        $result = get_object_vars($result_converttoarray);

        $Error = '';
        $status = '';
        $track_id='';
        if (array_key_exists('status', $result)) {
            $status = $result['status'];
            $track_id = $result['track_id']; //refid
        } else {
            $errorcode = $result['error_code'];
            $errormessage = $result['error_message'];
            $Error = $errormessage;

        }

        $array = array('status' => $status, 'Error' => $Error, 'RefID' => $track_id);
        return $array;

    }


}


?>