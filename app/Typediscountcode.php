<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Discountcode;
class Typediscountcode extends Model
{
    public $timestamps = false;
    public function discountcodes()
    {
        return $this->hasMany(Discountcode::class);
    }


}
