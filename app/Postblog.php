<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postblog extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'wp_posts';
    public $timestamps = false;
}
