<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Devicecomponent;
use App\Pricecalculateservice;

class Servicecomponnentproduct extends Model
{

    public function devicecomponent()
    {
        return $this->belongsTo(Devicecomponent::class);
    }

    public function pricecalculateservice()
    {
        return $this->belongsTo(Pricecalculateservice::class);
    }


}
