<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

use App\Order;
class Rizfacture extends Model
{
    public $timestamps = false;
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
