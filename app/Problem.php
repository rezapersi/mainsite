<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Requestt;
use App\Maincat;
class Problem extends Model

{

    public $timestamps = false;

    public function requestts()
    {
        return $this->hasMany(Requestt::class);
    }

    public function maincat()
    {
        return $this->belongsTo(maincat::class);
    }

}