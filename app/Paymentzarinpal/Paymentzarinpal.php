<?php

namespace App\Paymentzarinpal;
use App\Setting;
use Illuminate\Routing\Route;
use nusoap_client;  //composer require econea/nusoap:dev-master
class Paymentzarinpal
{

    //webaddres for test:https://sandbox.zarinpal.com/pg/services/WebGate/wsdl

    /**
     * @param $Amount
     * @param $Description
     * @param $Email
     * @param $Mobile
     * @return array
     */
    function zarinpalRequest($Amount, $Description, $Email, $Mobile)
    {

        $setting = Setting::first();
        $zarinpalMerchantID = $setting->zarinpalMerchantID;
       $zarinpalWebAdress = $setting->zarinpalWebAdress;

       // $zarinpalMerchantID ='XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX';
        //$zarinpalWebAdress ='https://sandbox.zarinpal.com/pg/services/WebGate/wsdl';

        $client = new nusoap_client($zarinpalWebAdress, 'wsdl');
        $client->soap_defencoding = 'UTF-8';
        $callback = Route('checkout');
        $params = array(

            'MerchantID' =>$zarinpalMerchantID,
            'Amount' => $Amount,
            'Description' => $Description,
            'Email' => $Email,
            'Mobile' => $Mobile,
            'CallbackURL' => $callback

        );

        $result = $client->call('PaymentRequest', $params);
        $Status = $result['Status'];
        $ErrorsArray =array(
            '-1' => 'اطلاعات ارسال شده ناقص شده است',
            '-2' => 'IP یا مرچنت کد صحیح نیست',
            '-3' => 'سطح تایید پذیرنده کمتر از نقره ای است',
            '-4' => 'سطح تاييد پذيرنده پايين تر از سطح نقره اي است.',
            '-11' => 'درخواست مورد نظر يافت نشد.',
            '-12' => 'امكان ويرايش درخواست ميسر نمي باشد.',
            '-21' => 'هيچ نوع عمليات مالي براي اين تراكنش يافت نشد.',
            '-22' => 'تراكنش نا موفق مي باشد.',
            '-23' => 'رقم تراكنش با رقم پرداخت شده مطابقت ندارد.',
            '-34' => 'سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است.',
            '-35' => 'جازه دسترسي به متد مربوطه وجود ندارد.',
            '-41' => 'اطلاعات ارسال شده مربوط به AdditionalData غيرمعتبر ميباشد.',
            '-42' => 'مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.',
            '-54' => 'درخواست مورد نظر آرشيو شده است.',
            '-101' => 'عمليات پرداخت موفق بوده و قبلا PaymentVerification تراكنش انجام شده است.',

        );
        $Authority = '';
        $Error = '';
        if ($Status != 100) {
            $Error = $ErrorsArray[$Status];
        }
        if ($Status == 100) {
            $Authority = $result['Authority'];
        }
        $array = array('Status' => $Status, 'Authority' => $Authority, 'Error' => $Error);
        return $array;
    }

    function zarinpalVerify($Amount, $Authority)
    {
        $setting = Setting::first();
        $zarinpalMerchantID = $setting->zarinpalMerchantID;
        $zarinpalWebAdress = $setting->zarinpalWebAdress;

       // $zarinpalMerchantID ='XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX';
       // $zarinpalWebAdress ='https://sandbox.zarinpal.com/pg/services/WebGate/wsdl';

        $client = new nusoap_client($zarinpalWebAdress, 'wsdl');
        $client->soap_defencoding = 'UTF-8';
        $result = $client->call('PaymentVerification', array(

            'MerchantID' =>$zarinpalMerchantID,
            'Amount' => $Amount,
            'Authority' => $Authority
        ));

        $Status = $result['Status'];
        $Error = '';
        $RefID = '';

        if ($Status != 100) {
            $ErrorsArray = array(
                '-1' => 'اطلاعات ارسال شده ناقص شده است',
                '-2' => 'IP یا مرچنت کد صحیح نیست',
                '-3' => 'سطح تایید پذیرنده کمتر از نقره ای است',
                '-4' => 'سطح تاييد پذيرنده پايين تر از سطح نقره اي است.',
                '-11' => 'درخواست مورد نظر يافت نشد.',
                '-12' => 'امكان ويرايش درخواست ميسر نمي باشد.',
                '-21' => 'هيچ نوع عمليات مالي براي اين تراكنش يافت نشد.',
                '-22' => 'تراكنش نا موفق مي باشد.',
                '-23' => 'رقم تراكنش با رقم پرداخت شده مطابقت ندارد.',
                '-34' => 'سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است.',
                '-35' => 'جازه دسترسي به متد مربوطه وجود ندارد.',
                '-41' => 'اطلاعات ارسال شده مربوط به AdditionalData غيرمعتبر ميباشد.',
                '-42' => 'مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.',
                '-54' => 'درخواست مورد نظر آرشيو شده است.',
                '-101' => 'عمليات پرداخت موفق بوده و قبلا PaymentVerification تراكنش انجام شده است.',

            );
            @$Error = $ErrorsArray[$Status];
        }
        if ($Status == 100) {
            $RefID = $result['RefID'];
        }
        $array = array('Status' => $Status, 'Error' => $Error, 'RefID' => $RefID);
        return $array;
    }

}

?>