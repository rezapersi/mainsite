<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Statuspay;
use App\Rizfacture;
use App\Typefacture;
use App\User;

class Order extends Model
{

    public $timestamps = false;
    
    public function statuspay()
    {
        return $this->belongsTo(Statuspay::class);
    }

    public function typefacture()
    {
        return $this->belongsTo(Typefacture::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function rizfactures()
    {
        return $this->hasMany(Rizfacture::class);
    }


}
