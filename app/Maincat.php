<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Requestt;
use App\Bernaddevice;
use App\Problem;

class Maincat extends Model
{

    public $timestamps = false;
    
    public function problems()
    {
        return $this->hasMany(problem::class);
    }
    
    public function requestts()
    {
        return $this->hasMany(Requestt::class);
    }



    public function bernaddevices()
    {
        return $this->hasMany(Bernaddevice::class);
    }




}
