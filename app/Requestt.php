<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Statusrequest;
use App\User;
use App\Order;
use App\Statusmessage;

use App\Maincat;
use App\Bernaddevice;
use App\Modeldevice;
use App\Problem;
use App\Typerequestt;

use App\Reportrequestt;

class Requestt extends Model
{

    public $timestamps = false;


    public function statusrequest()
    {
        return $this->belongsTo(Statusrequest::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }



    public function statusmessages()
    {
        return $this->hasMany(Statusmessage::class);
    }

    public function maincat()
    {
        return $this->belongsTo(Maincat::class);
    }

    public function bernaddevice()
    {
        return $this->belongsTo(Bernaddevice::class);
    }

    public function modeldevice()
    {
        return $this->belongsTo(Modeldevice::class);
    }

    public function problem()
    {
        return $this->belongsTo(Problem::class);
    }

    public function typerequestt()
    {
        return $this->belongsTo(Typerequestt::class);
    }


    public function reportrequestts()
    {
        return $this->hasMany(Reportrequestt::class);
    }

}
