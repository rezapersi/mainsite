<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Typediscountcode;
use App\Useddiscountcode;

class Discountcode extends Model
{
    public $timestamps = false;
    
    public function typediscountcode()
    {
        return $this->belongsTo(Typediscountcode::class);
    }

    public function useddiscountcodes()
    {
        return $this->hasMany(Useddiscountcode::class);
    }


}
