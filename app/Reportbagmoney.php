<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\User;
use App\Typereport;
class Reportbagmoney extends Model
{
    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function typereport()
    {
        return $this->belongsTo(Typereport::class);
    }
}
