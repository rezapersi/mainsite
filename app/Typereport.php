<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reportbagmoney;
use App\Reportcompany;
class Typereport extends Model
{
    public $timestamps = false;
    
    public function reportcompanys()
    {
        return $this->hasMany(Reportcompany::class);
    }
    public function reportbagmoneys()
    {
        return $this->hasMany(Reportbagmoney::class);
    }
}
