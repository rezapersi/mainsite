<?php


use App\Requestt;
use App\User;
use App\Service;

Route::get('/rate', function () {

    $findkarshenas = User::where('id', 3)->with('services')->first();
    $rate = 1;
    $beforerate = $findkarshenas->rate;
    $numberrequest = sizeof(Requestt::where('idkarshenas', 3)->get());
    $services = $findkarshenas->services;
    $userincat = [];
    foreach ($services as $row) {
        $get = Service::where('id', $row->id)->with('users')->first();
        foreach ($get->users as $row2) {
            $userincat[] = $row2->id;
        }
    }
    //delete Repeat array
    $userincat = array_unique($userincat, SORT_REGULAR);
    $numberuserincategory = sizeof($userincat);
    $k = ($numberrequest / $numberuserincategory) + 1;
    $newrate =$beforerate+(($k*$rate)/10);
    print_r($newrate);

});

Route::get('/removecache', function () {
    //   $exitCode = Artisan::call('cache:clear');
    //  $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/', 'IndexControllers@index');
Route::get('service/{id}', 'ServicesControllers@index');

//Route::get('/home', function () {
//    return redirect('/login');
//});

Route::post('index', 'IndexControllers@index');
Route::post('shop', 'IndexControllers@shop');
Route::prefix('index')->group(function () {
    Route::post('validdetailsformsaverequest', 'IndexControllers@validdetailsformsaverequest');
    Route::post('saveformrequest', 'IndexControllers@saveformrequest');
    Route::post('saveemail', 'IndexControllers@saveemail');
});
Route::post('/getsearchkhadamat', 'IndexControllers@getsearchkhadamat');
Route::get('/passwordrecovery', 'PasswordrecoveryController@index');
Route::post('/checkmobile', 'PasswordrecoveryController@checkmobile');
Route::post('/getagainverifiedcode', 'PasswordrecoveryController@getagainverifiedcode');
Route::post('/checkverifiedcode', 'PasswordrecoveryController@checkverifiedcode');
Route::post('/changepasswordrecovery', 'PasswordrecoveryController@changepasswordrecovery');


Route::post('getdetailsuser', 'SamepaneluserControllers@getdetailsuser');
Route::post('updatedetailsuser', 'SamepaneluserControllers@updatedetailsuser');
Route::post('getdetailsrequest', 'SamepaneluserControllers@getdetailsrequest');
Route::post('getallfacturepaneluser', 'SamepaneluserControllers@getallfacturepaneluser');
Route::post('getdetailsorder', 'SamepaneluserControllers@getdetailsorder');
Route::post('getrizfactures', 'SamepaneluserControllers@getrizfactures');
Route::post('checkdiscountcode', 'SamepaneluserControllers@checkdiscountcode');
Route::post('payfacture', 'SamepaneluserControllers@payfacture')->name('payfacture');
Route::post('getreportbagmoney', 'SamepaneluserControllers@getreportbagmoney');
Route::post('insertrequestmoney', 'SamepaneluserControllers@insertrequestmoney');
Route::post('getrequestmoney', 'SamepaneluserControllers@getrequestmoney');
Route::post('showmessagecancelrequestmoney', 'SamepaneluserControllers@showmessagecancelrequestmoney');
Route::post('increasecredit', 'SamepaneluserControllers@increasecredit')->name('increasecredit');
Route::post('/changepassworduser', 'SamepaneluserControllers@changepassworduser');




//rout insert request in index page and paneluser

Route::post('getservice', 'InsertrequestControllers@getservice');
Route::post('getberand', 'InsertrequestControllers@getberand');
Route::post('getmodel', 'InsertrequestControllers@getmodel');
Route::post('detailsuser', 'InsertrequestControllers@detailsuser');
Route::post('updateuser', 'InsertrequestControllers@updateuser');
Route::post('addrequestbyuser', 'InsertrequestControllers@addrequestbyuser');
Route::post('insertrequestbygust', 'InsertrequestControllers@insertrequestbygust');
Route::post('getmaincatwithidservice', 'InsertrequestControllers@getmaincatwithidservice');


Route::prefix('panel')->group(function () {

    Route::get('insertrequest', 'PaneluserControllers@insertrequest');
    Route::get('dashboard', 'PaneluserControllers@dashboard');

    //accountuser
    Route::get('accountuser', 'PaneluserControllers@accountuser');
    Route::post('updateservicekarshenas', 'PaneluserControllers@updateservicekarshenas');
    Route::post('updatemadarekkarshenas', 'PaneluserControllers@updatemadarekkarshenas')->name('updatemadarekkarshenas');
    Route::post('insertaddress', 'PaneluserControllers@insertaddress');
    Route::post('getaddress', 'PaneluserControllers@getaddress');
    Route::post('removeaddress', 'PaneluserControllers@removeaddress');

    //facture
    Route::get('facture', 'PaneluserControllers@facture');


    //managerequest
    Route::get('managerequests', 'PaneluserControllers@managerequests');
    Route::post('getrequestcustomer', 'PaneluserControllers@getrequestcustomer');
    Route::post('/getreportsrequestcustomer', 'PaneluserControllers@getreportsrequestcustomer');


    Route::get('bagmoney', 'PaneluserControllers@bagmoney');
    Route::get('partsbank', 'PaneluserControllers@partsbank');
    Route::get('support', 'PaneluserControllers@support');

});
Route::prefix('panelkarshenasan')->group(function () {

    Route::get('dashboard', 'PanelkarshenasanControllers@dashboard');

    //accountuser
    Route::get('accountuser', 'PanelkarshenasanControllers@accountuser');
    Route::post('updateservicekarshenas', 'PanelkarshenasanControllers@updateservicekarshenas');
    Route::post('updatemadarekkarshenas', 'PanelkarshenasanControllers@updatemadarekkarshenas')->name('updatemadarekkarshenas');

    //managerequest
    Route::post('getrequesttkarshenasan', 'PanelkarshenasanControllers@getrequesttkarshenasan');
    Route::post('getamarrequest', 'PanelkarshenasanControllers@getamarrequest');
    Route::post('confirmrequestbykarshenas', 'PanelkarshenasanControllers@confirmrequestbykarshenas');

    Route::post('/getreportsrequest', 'PanelkarshenasanControllers@getreportsrequest');
    Route::post('/insertreportrequest', 'PanelkarshenasanControllers@insertreportrequest');
    Route::post('/deletereportrequest', 'PanelkarshenasanControllers@deletereportrequest');

    Route::post('/sodorfactureforrequest', 'PanelkarshenasanControllers@sodorfactureforrequest')->name('sodorfactureforrequest');

    Route::post('/getfacturerequest', 'PanelkarshenasanControllers@getfacturerequest');


    Route::get('managerequests', 'PanelkarshenasanControllers@managerequests')->middleware('Blockpanel');;

    //facture
    Route::get('facture', 'PanelkarshenasanControllers@facture')->middleware('Blockpanel');;
    Route::get('bagmoney', 'PanelkarshenasanControllers@bagmoney')->middleware('Blockpanel');;
    Route::get('partsbank', 'PanelkarshenasanControllers@partsbank')->middleware('Blockpanel');;
    Route::get('support', 'PanelkarshenasanControllers@support');

});


/*Route::prefix('pay')->group(function () {

    Route::get('/{trackingcode}', 'PayControllers@index');
    Route::post('/sendrequestpay', 'PayControllers@sendrequestpay')->name('sendrequestpay');
});*/

Route::any('/checkout', 'CheckoutControllers@checkout')->name('checkout');
Route::get('/detailspay/{idorder?}', 'DetailspayControllers@index')->name('detailspay');


Auth::routes();

Route::get('/home', function () {
    if(\Illuminate\Support\Facades\Auth::user()){
        $role = Auth::user()->leveluser_id;
        if ($role == '5') {
            return redirect('/panelkarshenasan/managerequests');
        } elseif ($role == '7') {
            return redirect('/panel/dashboard');
        }
        else {
            return redirect('https://admin.dgyr.ir/login');
        }
    }else{
        return redirect('/');
    }

});


//Calculationsystem
Route::get('service_price', 'CalculationsystemControllers@index');
Route::post('pricecalculatemaincatchosed', 'CalculationsystemControllers@pricecalculatemaincatchosed');
Route::post('getpricecalculatemodeldevice', 'CalculationsystemControllers@getpricecalculatemodeldevice');
Route::post('cecalculatecost', 'CalculationsystemControllers@cecalculatecost');


Route::get('/addresskarshenasan', function () {
    return view('address');
});

