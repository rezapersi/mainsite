/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import swal from 'sweetalert';


import rate from 'vue-rate';

Vue.use(rate)


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


//for setup pagination laravel : npm install laravel-vue-pagination
//for vhange color pagination in mdb.css class :.pagination .page-item.active .page-link
//and insert flow code vue.component

Vue.component('pagination', require('laravel-vue-pagination'));


const app = new Vue({
    el: '#app',
    data: {

        getsearchkhadamat: '',
        searchkhadamat: {},
        withwindow: '',
        namecustomer: '',
        familycustomer: '',
        phonenumbercustomer: '',
        addresscustomer: '',
        descriptioncustomer: '',
        email: '',

        newpasswordforchangepassword: '',
        repeatpasswordforchangepassword: '',


        //accountuser


        //part insert request

        selectedmaincat: '',
        selectedservice: '',
        selectedberand: '',
        selectedmodel: '',
        allservice: {},
        searchservice: '',
        allberand: {},
        searchberand: '',
        allmodel: {},
        searchmodel: '',
        descriptionrequest: '',
        idchosedaddress: 0,


        emailuser: '',
        mobileuser: '',
        nameuser: '',
        familyuser: '',
        nationalcodeuser: '',
        namecompanyuser: '',
        phoneuser: '',
        codepostiuser: '',
        addressuser: '',

        detailsorder: '',
        rizfactures: '',
        discountcode: '',
        allreportbagmoney: {},


        //insert address
        codeposti: '',
        fulladdress: '',
        plak: '',
        alladdress: {},


        // panel user -->start

        allfacturepaneluser: {},
        allrequestcustomer: {},
        allrequestcustomernewdata: {},
        rate: '',

        // panel user -->end


        //managerequest
        allrequesttkarshenasan: {},
        detailsrequest: '',
        allreportrequest: {},
        idrequest: '',
        textreportrequest: '',
        priceaddrizfacture: '',
        babataddrizfacture: '',
        facturerequest: '',


        //recoverypassword

        mobilerecpass: '',
        Verifiedcode: '',
        passwordrecovery: '',
        repeatpasswordrecovery: '',

        //pagebagmoney

        numbershaba: '',
        requestmoney: '',
        allrequestmoney: {},

    },


    beforeMount() {


        var withscreen = $(window).width();
        this.withwindow = withscreen;
        if (withscreen <= 630) {
            $('.flat').show();
            $('.table-responsive').hide();

        } else {

            $('.flat').hide();
            $('.flat').css('display', 'none');
            $('.table-responsive').show();
            $('.table-responsive2').show();

        }

    },


    mounted() {


        var pageURL = window.location.href;
        var url = pageURL.split('/');
        var nameurl = url[3];

        if (nameurl == 'detailspay') {

            window.location.hash = "no-back-button";
            window.location.hash = "Again-No-back-button"; //again because google chrome don't insert first hash into history
            window.onhashchange = function () {
                window.location.hash = "no-back-button";
            }

            var mainseconds = 15;
            counter();

            function counter() {
                var x = setInterval(function () {
                    var seconds = mainseconds - 1;

                    document.getElementById("counter").innerHTML = seconds;

                    if (mainseconds == 1) {
                        clearInterval(x);
                        document.getElementById("buttomredirecttohome").click()

                    } else {

                        mainseconds = mainseconds - 1
                    }

                }, 1000);

            }

        }


        this.showtypesidebarpanels();

        var page = $('#page').val();

        if (page == 'accountuser') {
            this.getdetailsuser();
        }

        if (page == 'accountuser-panel') {
            this.getdetailsuser();
            this.getaddress();
        }


        if (page == 'managerequests') {
            this.getrequesttkarshenasan();
        }

        if (page == 'facture') {
            this.getallfacturepaneluser();
        }

        if (page == 'managerequests-panel') {
            this.getrequestcustomer();
        }

        if (page == 'bagmoney') {
            this.getreportbagmoney();

        }

    },
    methods: {

        closemodalemaked: function () {
            $('.modalmaked').hide();
            $('#modelmorerequest').css('display', 'block');
        },

        showmodelendrequest: function () {
            $('#modelendrequest').show();
            $('#modelshowfacture').hide();
            $('#modelmorerequest').css('display', 'none');
        },
        showmodelfacture: function () {
            $('#modelshowfacture').show();
            $('#modelendrequest').hide();
            $('#modelmorerequest').css('display', 'none');
        },

        showloading: function () {
            $('#largeloading').show();
        },

        //part insert request panel user


        defaultinsertrequest: function () {

            $('.section1_chosedmaincat').show();
            $('.insertrequestbody section').hide();
            $('.section1').show();
            $('.section1_stepsdown >div').hide();
            $('.section1_stepsdown_step1').show();

            $('.items').removeClass('active');

            $('.ulselectservice li').removeClass('active');
            $('.ulselectberand li').removeClass('active');
            $('.ulselectmodel li').removeClass('active');

            this.selectedservice = '';
            this.selectedberand = '';
            this.selectedmodel = '';


        },


        selectmaincat: function (id) {


            $('.section1_chosedmaincat').show();
            $('.section1_stepsdown >div').hide();
            $('.section1_stepsdown_step1').show();

            $('.items').removeClass('active');

            $('.ulselectservice li').removeClass('active');
            $('.ulselectberand li').removeClass('active');
            $('.ulselectmodel li').removeClass('active');


            $('.section1_chosedmaincat_left').find('.items[dataid="' + id + '"]').addClass('active');


            this.selectedservice = '';
            this.selectedberand = '';
            this.selectedmodel = '';
            this.selectedmaincat = id;


            //get brand

            this.getservice();
            this.getberand();


        },

        getservice: function (page = 1) {
            var id = this.selectedmaincat;
            $('#largeloading').show();
            axios.post('/getservice?page=' + page, {
                idmaincat: id,
            }).then(response => {
                this.allservice = response.data;
                $('#largeloading').hide();
            });

        },
        getberand: function (page = 1) {

            var id = this.selectedmaincat;
            $('#largeloading').show();
            axios.post('/getberand?page=' + page, {
                idmaincat: id,
            }).then(response => {

                this.allberand = response.data;

                $('#largeloading').hide();


            });

        },

        getmodel: function (page = 1) {


            var id = this.selectedberand;
            $('#largeloading').show();
            axios.post('/getmodel?page=' + page, {
                idberand: id,
            }).then(response => {

                this.allmodel = response.data;

                $('#largeloading').hide();
            });

        },


        selectproblem: function (id) {


            $('.ulselectservice li').removeClass('active');
            $('.ulselectservice').find('li[dataid="' + id + '"]').addClass('active');
            this.selectedservice = id;

            $('#nextstep1 span').trigger('click');


        },

        selectberand: function (id) {

            $('.ulselectberand li').removeClass('active');
            $('.ulselectberand').find('li[dataid="' + id + '"]').addClass('active');

            this.selectedberand = id;
            this.getmodel();

            $('#nextstep2 span').trigger('click');

        },

        selectmodel: function (id) {

            $('.ulselectmodel li').removeClass('active');
            $('.ulselectmodel').find('li[dataid="' + id + '"]').addClass('active');

            this.selectedmodel = id;

            $('#nextstep3 span').trigger('click');
        },


        activeaddress: function (event) {


            var tag = event.target;
            $('.ulselectaddress li span').removeClass('active');
            $(tag).addClass('active');
            var dataid = $('.ulselectaddress li').find('span.active').attr('dataid');

            this.idchosedaddress = dataid;


            if (dataid == 0) {

                $('.insertnewaddres').show();


            } else {

                $('.insertnewaddres').hide();

            }


        },


        step1: function () {

            $('.insertrequestbody section').hide();
            $('.section1').show();

            if (this.selectedservice == '' || this.selectedmaincat == '') {

                swal('سرویس مورد نظر را انتخاب نمایید.', {
                    icon: "warning",
                });

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step1').show();

            } else {

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step2').show();

            }

        },
        step2: function () {

            if (this.selectedberand == '') {

                swal('برند مورد نظر را انتخاب نمایید.', {
                    icon: "warning",
                });

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step2').show();

            } else {

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step3').show();
            }

        },
        step3: function () {

            if (this.selectedmodel == '') {

                swal('مدل دستگاه  را انتخاب نمایید.', {
                    icon: "warning",
                });

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step3').show();

            } else {

                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step4').show();
            }

        },

        step4: function (whereinsertrequest) {


            if (whereinsertrequest == 'frompaneluser') {
                var ostan = $('#ostan').find('option:selected').val();
                var city = $('#shahr').find('option:selected').val();
                if (ostan == '' || ostan == undefined || city == '' || city == undefined) {
                    swal('استان و شهر را در بالای صفحه انتخاب نمایید', {
                        icon: "warning",
                    });

                    $('.section1_stepsdown >div').hide();
                    $('.section1_stepsdown_step4').show();

                }

                if (ostan !== '' && ostan !== undefined && city !== '' && city !== undefined) {

                    $('.ulstep li').removeClass('active');
                    $('.ulstep').find('li[dataid="' + 2 + '"]').addClass('active');
                    $('.insertrequestbody section').hide();
                    $('.section2').show();

                } else {

                    $('.section1_stepsdown >div').hide();
                    $('.section1_stepsdown_step4').show();
                }


            } else {


                $('.ulstep li').removeClass('active');
                $('.ulstep').find('li[dataid="' + 2 + '"]').addClass('active');
                $('.insertrequestbody section').hide();
                $('.section2').show();

            }


        },

        step5: function (whereinsertrequest) {


            this.detailsuser();

            var chosedaddres = this.idchosedaddress
            if (chosedaddres == 0) {

                $('.text-error').html('');
                var check = 'ok';

                var ostan = $('#ostan').find('option:selected').val();
                var city = $('#shahr').find('option:selected').val();

                var codeposti = this.codeposti;
                var address = this.fulladdress;
                var plak = this.plak;

                if (codeposti == '' || codeposti == null) {
                    $('#licodeposti  .text-error').html('  کد پستی را وارد نمایید.');
                    check = 'no';
                }

                if (address == '' || address == null) {
                    $('#liaddress  .text-error').html(' آدرس را وارد نمایید.');
                    check = 'no';
                }

                if (plak == '' || plak == null) {
                    $('#liplak  .text-error').html(' شماره پلاک را وارد نمایید.');
                    check = 'no';
                }


                if (whereinsertrequest == 'fromindex') {

                    if (ostan == '' || ostan == undefined) {
                        $('#liostan  .text-error').html('  استان  را انتخاب نمایید.');
                        check = 'no';
                    }
                    if (city == '' || city == undefined) {
                        $('#licity  .text-error').html('  شهر  را انتخاب نمایید.');
                        check = 'no';
                    }


                }


                if (check == 'ok') {


                    if (whereinsertrequest == 'frompaneluser') {

                        $('#largeloading').show();
                        axios.post('/panel/insertaddress', {
                            codeposti: codeposti,
                            address: address,
                            ostan: ostan,
                            city: city,
                            plak: plak,

                        }).then(response => {

                            var idaddress = response.data.idaddress;
                            var status = response.data.status;

                            if (status == 'ok') {

                                this.idchosedaddress = idaddress

                                this.codeposti = '';
                                this.fulladdress = '';
                                this.plak = '';

                                $('#largeloading').hide();


                            } else {

                                swal('لطفا مجددا وارد حساب کاربری خود شوید', {
                                    icon: "warning",
                                })


                                setTimeout(() => {

                                    window.location = '/login';
                                }, 2000);

                            }


                        })


                    }


                    $('.ulstep li').removeClass('active');
                    $('.ulstep').find('li[dataid="' + 3 + '"]').addClass('active');
                    $('.insertrequestbody section').hide();
                    $('.section3').show();


                }


            } else {


                $('.ulstep li').removeClass('active');
                $('.ulstep').find('li[dataid="' + 3 + '"]').addClass('active');
                $('.insertrequestbody section').hide();
                $('.section3').show();

            }


        },


        step6: function (whereinsertrequest) {


            $('.text-error').html('');
            var check = 'ok';

            var mobile = this.mobileuser;
            var name = this.nameuser;
            var family = this.familyuser;

            var khadamat = this.selectedmaincat;
            var service = this.selectedservice;
            var berand = this.selectedberand;
            var model = this.selectedmodel;
            var description = this.descriptionrequest;
            var idaddress = this.idchosedaddress;


            var codeposti = this.codeposti;
            var address = this.fulladdress;
            var plak = this.plak;
            var ostan = $('#ostan').find('option:selected').val();
            var city = $('#shahr').find('option:selected').val();


            if (mobile == '' || mobile == null) {
                $('.limobileuser  .text-error').html('شماره موبایل را وارد نمایید.');
                check = 'no';
            } else {

                if (mobile.length < 11) {
                    $('.limobileuser  .text-error').html('شماره موبایل باید 11 رقم باشد .');
                    check = 'no';
                }
            }

            if (name == '' || name == null) {
                $('.linameuser .text-error').html('نام خود را وارد نمایید.');
                check = 'no';
            }

            if (family == '' || family == null) {
                $('.lifamilyuser  .text-error').html('نام خانوادگی خود را وارد نمایید.');
                check = 'no';
            }


            if (check == 'ok') {


                if (whereinsertrequest == 'frompaneluser') {

                    $('#largeloading').show();
                    axios.post('/updateuser', {
                        mobile: mobile,
                        name: name,
                        family: family,

                    }).then(response => {

                        $('#largeloading').hide();
                        var error = response.data.alert;
                        if (error == 'ok') {

                            //insert request


                            if (khadamat == '' || service == '' || berand == '' || model == '' || description == '' || idaddress == '') {

                                swal('خطا در دریافت اطلاعات دریافتی ، لطفا مجددا درخواست خود را ثبت نمایید.', {
                                    icon: "warning",
                                });

                                setTimeout(() => {

                                    $('.ulstep li').removeClass('active');
                                    $('.ulstep').find('li[dataid="' + 1 + '"]').addClass('active');
                                    $('.insertrequestbody section').hide();
                                    $('.section1').show();
                                    $('.section1_stepsdown >div').hide();
                                    $('.section1_stepsdown_step1').show();

                                }, 3000);


                            } else {

                                $('#largeloading').show();
                                axios.post('/addrequestbyuser', {
                                    khadamat: khadamat,
                                    service: service,
                                    berand: berand,
                                    model: model,
                                    description: description,
                                    idaddress: idaddress,

                                }).then(response => {

                                    var trackingcode = response.data.trackingcode;
                                    $('.trackingcode').html('کد پیگیری درخواست ' + trackingcode + '');
                                    $('.ulstep li').removeClass('active');
                                    $('.ulstep').find('li[dataid="' + 4 + '"]').addClass('active');
                                    $('.insertrequestbody section').hide();
                                    $('.section4').show();
                                    $('#largeloading').hide();

                                });


                            }


                        } else {

                            swal(error, {
                                icon: "warning",
                            });


                        }


                    })


                } else {


                    if (mobile == '' || name == '' || family == '' || codeposti == '' || address == '' || plak == '' || ostan == undefined || city == undefined || khadamat == '' || service == '' || berand == '' || model == '') {

                        swal('خطا در دریافت اطلاعات دریافتی ، لطفا مجددا درخواست خود را ثبت نمایید.', {
                            icon: "warning",
                        });
                        setTimeout(() => {

                            $('.ulstep li').removeClass('active');
                            $('.ulstep').find('li[dataid="' + 1 + '"]').addClass('active');
                            $('.insertrequestbody section').hide();
                            $('.section1').show();
                            $('.section1_stepsdown >div').hide();
                            $('.section1_stepsdown_step1').show();

                        }, 3000);

                    } else {


                        $('#largeloading').show();
                        axios.post('/insertrequestbygust', {
                            mobile: mobile,
                            name: name,
                            family: family,
                            codeposti: codeposti,
                            address: address,
                            plak: plak,
                            ostan: ostan,
                            city: city,
                            khadamat: khadamat,
                            service: service,
                            berand: berand,
                            model: model,
                            description: description,

                        }).then(response => {

                            var status = response.data.status;
                            if (status == 'success') {
                                var trackingcode = response.data.trackingcode;
                                $('.trackingcode').html('کد پیگیری درخواست ' + trackingcode + '');
                                $('.ulstep li').removeClass('active');
                                $('.ulstep').find('li[dataid="' + 4 + '"]').addClass('active');
                                $('.insertrequestbody section').hide();
                                $('.section4').show();
                            } else {

                                swal(' ثبت درخواست برای کارشناس تعمیر امکان پذیر نمی باشد.', {
                                    icon: "warning",
                                });
                                setTimeout(() => {
                                    $('.ulstep li').removeClass('active');
                                    $('.ulstep').find('li[dataid="' + 1 + '"]').addClass('active');
                                    $('.insertrequestbody section').hide();
                                    $('.section1').show();
                                    $('.section1_stepsdown >div').hide();
                                    $('.section1_stepsdown_step1').show();
                                }, 3000);
                            }


                            $('#largeloading').hide();

                        });


                    }

                }


            }


        },


        nextstep: function (step, whereinsertrequest) {


            if (step == 1) {
                this.step1(whereinsertrequest);
            }

            if (step == 2) {


                this.step2(whereinsertrequest);

            }


            if (step == 3) {


                this.step3(whereinsertrequest);


            }
            if (step == 4) {


                this.step4(whereinsertrequest);

            }


            if (step == 5) {


                this.step5(whereinsertrequest);

            }
            if (step == 6) {


                this.step6(whereinsertrequest);


            }


        },


        chosedservicesforinsertmodal: function (idservice) {

            $('#largeloading').show();
            axios.post('/getmaincatwithidservice', {
                idservice: idservice,
            }).then(response => {

                this.selectedmaincat = response.data;
                this.selectedservice = idservice;

                $('.items').removeClass('active');
                $('.ulselectservice li').removeClass('active');
                $('.ulselectberand li').removeClass('active');
                $('.ulselectmodel li').removeClass('active');
                $('.section1_chosedmaincat_left').find('.items[dataid="' + response.data + '"]').addClass('active');


                this.selectedberand = '';
                this.selectedmodel = '';


                $('.section1_chosedmaincat').hide();
                $('.insertrequestbody section').hide();
                $('.section1').show();
                $('.section1_stepsdown >div').hide();
                $('.section1_stepsdown_step2').show();

                this.getservice();
                this.getberand();


                $('.ulselectservice').find('li[dataid="' + idservice + '"]').addClass('active');


                $('#openmodalinsertrequest').trigger('click');
                $('#largeloading').hide();

            });


        },

        //part insert request panel user


        detailsuser: function () {

            $('#largeloading').show();
            axios.post('/detailsuser', {}).then(response => {

                this.mobileuser = response.data.mobile;
                this.nameuser = response.data.name;
                this.familyuser = response.data.family;

                $('#largeloading').hide();

            });
        },


        getdetailsuser: function () {
            $('#largeloading').show();
            axios.post('/getdetailsuser', {}).then(response => {

                this.mobileuser = response.data.mobile;
                this.emailuser = response.data.email;
                this.nameuser = response.data.name;
                this.familyuser = response.data.family;
                this.nationalcodeuser = response.data.nationalcode;
                this.namecompanyuser = response.data.namecompany;
                this.phoneuser = response.data.phone;
                this.codepostiuser = response.data.codeposti;
                this.addressuser = response.data.address;


                var ostan = response.data.ostan;
                $('#liostanedit select').find('option[value="' + ostan + '"]').attr('selected', 'selected');
                $('#liostanedit .filter-option-inner-inner').html(ostan);

                var dataval = $('#liostanedit select').find('option[value="' + ostan + '"]').attr('data-val');
                $('#liostanedit select').selectpicker('refresh');

                this.cityeditrequest(dataval);

                var city = response.data.city;
                $('#licityedit select').find('option[value="' + city + '"]').attr('selected', 'selected');
                $('#licityedit  .filter-option-inner-inner').html(city);
                $('#licityedit select').selectpicker('refresh');

                $('#largeloading').hide();

            })
        },

        cityeditrequest: function (id) {
            var arr = new Array();
            if (id == 41) {

                arr = ['آذر شهر', 'اسكو', 'اهر', 'بستان آباد', 'بناب', 'بندر شرفخانه', 'تبريز', 'تسوج', 'جلفا', 'سراب', 'شبستر', 'صوفیان', 'عجبشير', 'قره آغاج', 'كليبر', 'كندوان', 'مراغه', 'مرند', 'ملكان', 'ميانه', 'ورزقان', 'هاديشهر', 'هريس', 'هشترود', 'ممقان'];

            } //if
            if (id == 44) {

                arr = ['اروميه', 'اشنويه', 'بازرگان', 'بوكان', 'پيرانشهر', 'تكاب', 'چالدران', 'خوي', 'سر دشت', 'سلماس', 'سيه چشمه', 'شاهين دژ', 'ماكو', 'مهاباد', 'مياندوآب', 'نقده'];

            } //if
            if (id == 45) {

                arr = ['اردبيل', 'بيله سوار', 'پارس آباد', 'خلخال', 'سرعين', 'گیوی(کوثر)', 'گرمي', 'مشگين شهر', 'نمين', 'نير'];

            } //if
            if (id == 31) {

                arr = ['آران و بيدگل', 'اردستان', 'اصفهان', 'باغ بهادران', 'تيران', 'چادگان', 'خميني شهر', 'خوانسار', 'دولت آباد', 'دهاقان', 'زرين شهر', 'زیبا شهر', 'سميرم', 'سپاهان شهر', 'شاهين شهر', 'شهرضا', 'فريدن', 'فريدون شهر', 'فلاورجان', 'فولاد شهر', 'قهدریجان', 'كاشان', 'گلدشت', 'گلپايگان', 'مباركه', 'ملک شهر', 'نايين', 'نجف آباد', 'نطنز', 'هرند'];

            } //if
            if (id == 26) {
                arr = ['آسارا', 'اشتهارد', 'چهار باغ', 'صفادشت', 'طالقان', 'کرج', 'کوهسار', 'نظر آباد', 'هشتگرد'];
            } //if
            if (id == 84) {
                arr = ['آبدانان', 'ايلام', 'ايوان', 'دره شهر', 'دهلران', 'سرابله', 'مهران'];
            } //if
            if (id == 77) {

                arr = ['اهرم', 'برازجان', 'بپخش', 'بوشهر', 'تنگستان', 'جم', 'خارك', 'خورموج', 'دشتستان', 'دشتي', 'دلوار', 'دير', 'ديلم', 'عسلویه', 'كنگان', 'گناوه'];

            } //if
            if (id == 21) {

                arr = ['اسلام شهر', 'اندیشه', 'بومهن', 'پاكدشت', 'تجريش', 'تهران', 'چهاردانگه', 'دماوند', 'رباط كريم', 'رودهن', 'ري', 'شريف آباد', 'شهريار', 'فشم', 'فيروزكوه', 'قدس', 'قرچك', 'كن', 'كهريزك', 'گلستان', 'لواسان', 'ملارد', 'ورامين'];

            } //if
            if (id == 38) {

                arr = ['ردل', 'بروجن', 'چلگرد', 'سامان', 'شهركرد', 'فارسان', 'فرخ شهر', 'لردگان', 'هفشجان'];

            } //if
            if (id == 56) {

                arr = ['بشرویه', 'بيرجند', 'خضری', 'سرایان', 'سربيشه', 'فردوس', 'قائن', 'هبندان'];

            } //if
            if (id == 51) {

                arr = ['بردسكن', 'بجستان', 'تايباد', 'تربت جام', 'تربت حيدريه', 'جغتای', 'جوین', 'چناران', 'خواف', 'خلیل آباد', 'درگز', 'رشتخوار', 'سبزوار', 'سرخس', 'طوس', 'طرقبه', 'فريمان', 'قوچان', 'كاشمر', 'كلات', 'گناباد', 'مشهد', 'نيشابور'];

            } //if
            if (id == 58) {

                arr = ['آشخانه', 'اسفراين', 'بجنورد', 'جاجرم', 'شيروان', 'فاروج'];


            } //if
            if (id == 61) {

                arr = ['آبادان', 'اميديه', 'انديمشك', 'اهواز', 'ايذه', 'گتوند', 'باغ ملك', 'بندرامام ', 'بندر ماهشهر', 'بهبهان', 'خرمشهر', 'دزفول', 'رامهرمز', 'رامشیر', 'سوسنگرد', 'شادگان', 'شوشتر', 'شوش', 'لالي', 'مسجد سليمان', 'هنديجان', 'هويزه'];


            } //if
            if (id == 24) {

                arr = ['آب بر', 'ابهر', 'ايجرود', 'خرمدره', 'زرين آباد', 'زنجان', 'قيدار', 'ماهنشان'];

            } //if
            if (id == 23) {

                arr = ['ايوانكي', 'بسطام', 'دامغان', 'سمنان', 'سرخه', 'شاهرود', 'شهمیرزاد', 'گرمسار', 'مهدیشهر'];

            } //if
            if (id == 54) {

                arr = ['ايرانشهر', 'چابهار', 'خاش', 'راسك', 'زابل', 'زاهدان', 'سراوان', 'سرباز', 'فنوج', 'کنارک', 'ميرجاوه', 'نيكشهر'];

            } //if
            if (id == 71) {

                arr = ['آباده', 'اردكان', 'ارسنجان', 'استهبان', 'اقليد', 'ایزد خواست', 'بوانات', 'پاسارگاد', 'جهرم', 'حاجي آباد', 'خرم بید', 'خنج', 'خشت', 'داراب', 'شيراز', 'فراشبند', 'فسا', 'فيروز آباد', 'قایمیه', 'قيرو کارزین', 'كازرون', 'گراش', 'لار', 'لامرد', 'مرودشت', 'مصیری(رستم)', 'مهر', 'نورآباد', 'نیریز'];

            } //if
            if (id == 28) {

                arr = ['آبيك', 'شهرک البرز', 'بوئين زهرا', 'تاكستان', 'قزوين', 'محمود آباد نمونه'];

            } //if
            if (id == 25) {

                arr = ['قم'];

            } //if
            if (id == 87) {

                arr = ['بانه', 'بيجار', 'ديواندره', 'دهگلان', 'سقز', 'سنندج', 'قروه', 'كامياران', 'مريوان'];

            } //if
            if (id == 34) {

                arr = ['شهر بابك', 'بافت', 'بردسير', 'بم', 'جيرفت', 'سرچشمه', 'راور', 'رفسنجان', 'زرند', 'سيرجان', 'كرمان', 'كهنوج'];

            } //if
            if (id == 83) {

                arr = ['اسلام آباد غرب', 'پاوه', 'ثلاث باباجانی', 'جوانرود', 'خسروی', 'سر پل ذهاب', 'سنقر', 'صحنه', 'قصر شيرين', 'كرمانشاه', 'كنگاور', 'گيلان غرب', 'هرسين'];

            } //if
            if (id == 74) {

                arr = ['دنا', 'دوگنبدان', 'دهدشت', 'سي سخت', 'گچساران', 'لیکک', 'ياسوج'];

            } //if
            if (id == 17) {

                arr = ['آزاد شهر', 'آق قلا', 'بندر گز', 'تركمن', 'جلین', 'راميان', 'علي آباد كتول', 'كردكوي', 'كلاله', 'گالیکش', 'گرگان', 'گنبد كاووس', 'مراوه تپه', 'مينو دشت'];

            } //if
            if (id == 13) {

                arr = ['ستارا', 'آستانه اشرفيه', 'املش', 'بندرانزلي', 'تالش', 'خمام', 'رودبار', 'رود سر', 'رستم آباد', 'رشت', 'رضوان شهر', 'سياهكل', 'شفت', 'صومعه سرا', 'فومن', 'كلاچاي', 'لاهيجان', 'لنگرود', 'لوشان', 'ماسال', 'ماسوله', 'منجيل'];

            } //if
            if (id == 66) {

                arr = ['ازنا', 'الشتر', 'اليگودرز', 'بروجرد', 'پلدختر', 'خرم آباد', 'دورود', 'سراب دوره', 'سپید دشت', 'شول آباد', 'كوهدشت', 'نور آباد'];

            } //if
            if (id == 15) {

                arr = ['مل', 'بلده', 'بهشهر', 'بابل', 'بابلسر', 'پل سفيد', 'تنكابن', 'جويبار', 'چالوس', 'رامسر', 'ساري', 'سلمانشهر', 'سواد كوه', 'فريدون كنار', 'کلاردشت', 'قائم شهر', 'گلوگاه', 'محمود آباد', 'مرزن آباد', 'نكا', 'نور', 'نوشهر'];

            } //if
            if (id == 86) {

                arr = ['آشتيان', 'راك', 'تفرش', 'خمين', 'خنداب', 'دليجان', 'زرندیه', 'ساوه', 'شازند', 'کمیجان', 'محلات'];

            } //if
            if (id == 76) {

                arr = ['ابوموسي', 'انگهران', 'بندر جاسك', 'بندر خمیر', 'بندرعباس', 'بندر لنگه', 'بستك', 'پارسیان', 'تنب بزرگ', 'حاجي آباد', 'دهبارز', 'قشم', 'كيش', 'ميناب'];

            } //if
            if (id == 81) {

                arr = ['سدآباد', 'بهار', 'تويسركان', 'رزن', 'كبودر اهنگ ', 'ملاير', 'نهاوند', 'همدان'];

            } //if
            if (id == 35) {

                arr = ['ابركوه', 'اردكان', 'اشكذر', 'بافق', 'تفت', 'طبس', 'مهريز', 'ميبد', 'رات', 'يزد'];

            } //if
            $('.shahr').find('select option').remove();
            var k = 0;
            if (arr.length > 0) {
                for (k = 0; k < arr.length; k++) {
                    $('.shahr').find('select').append($('<option>', {
                        text: arr[k],
                        value: arr[k]
                    }));
                } //for
            } //if

            $('#shahr').selectpicker('refresh');
        },

        updatedetailsuser: function (type) {

            $('.text-error').html('');
            var check = 'ok';

            var page = $('#page').val();

            var mobile = this.mobileuser;
            var email = this.emailuser;

            var name = this.nameuser;
            var family = this.familyuser;
            var nationalcode = this.nationalcodeuser;

            var namecompany = this.namecompanyuser;

            var phone = this.phoneuser;


            var codeposti = this.codepostiuser;
            var address = this.addressuser;
            var ostan = $('#liostanedit select').find('option:selected').val();
            var city = $('#licityedit select').find('option:selected').val();


            if (type == 'informationkarbari') {

                if (mobile == '' || mobile == null) {
                    $('#limobileedit  .text-error').html('شماره موبایل را وارد نمایید.');
                    check = 'no';
                } else {

                    if (mobile.length < 11) {
                        $('#limobileedit  .text-error').html('شماره موبایل باید 11 رقم باشد .');
                        check = 'no';
                    }
                }

                if (email == '' || email == null) {
                    $('#liemailedit  .text-error').html('ایمیل خود را وارد نمایید.');
                    check = 'no';
                }
            }

            if (type == 'informationpersonal') {

                if (name == '' || name == null) {
                    $('#lienameedit  .text-error').html('نام خود را وارد نمایید.');
                    check = 'no';
                }

                if (family == '' || family == null) {
                    $('#liefamilyedit  .text-error').html('نام خانوادگی خود را وارد نمایید.');
                    check = 'no';
                }

                if (nationalcode == '' || nationalcode == null) {
                    $('#linationalcodeedit  .text-error').html('کد ملی خود را وارد نمایید.');
                    check = 'no';
                }

                if (page == 'accountuser') {
                    //panel karshenasan

                    if (namecompany == '' || namecompany == null) {
                        $('#linamecompanyedit  .text-error').html('نام شرکت  را وارد نمایید.');
                        check = 'no';
                    }


                    if (codeposti == '' || codeposti == null) {
                        $('#licodepostiedit  .text-error').html('  کد پستی را وارد نمایید.');
                        check = 'no';
                    }


                    if (address == '' || address == null) {
                        $('#liaddressedit  .text-error').html(' آدرس را وارد نمایید.');
                        check = 'no';
                    }


                    if (ostan == '' || ostan == undefined) {
                        $('#liostanedit  .text-error').html(' استان را انتخاب نمایید.');
                        check = 'no';
                    }

                    if (city == '' || city == undefined) {
                        $('#licityedit  .text-error').html(' شهر را انتخاب نمایید.');
                        check = 'no';
                    }


                }


                if (phone == '' || phone == null) {
                    $('#liphoneedit  .text-error').html('شماره تلفن ثابت را وارد نمایید.');
                    check = 'no';
                }


            }


            if (check == 'ok') {

                $('#largeloading').show();
                axios.post('/updatedetailsuser', {
                    type: type,
                    mobile: mobile,
                    email: email,
                    name: name,
                    family: family,
                    nationalcode: nationalcode,
                    namecompany: namecompany,
                    phone: phone,
                    codeposti: codeposti,
                    address: address,
                    ostan: ostan,
                    city: city,
                    page: page,


                }).then(response => {


                    if (response.data.length > 0) {

                        $.each(response.data, function (index, title) {
                            swal(title, {
                                icon: "warning",
                            });
                        })


                    } else {

                        swal('تغییرات با موفقیت اعمال شد.', {
                            icon: "success",
                        });


                        this.getdetailsuser();
                        $('.text-error').html('');

                    }


                    $('#largeloading').hide();
                })
            }

        },


        insertaddress: function () {
            $('.text-error').html('');
            var check = 'ok';

            var ostan = $('#liostanaddress select').find('option:selected').val();
            var city = $('#licityaddress select').find('option:selected').val();
            var codeposti = this.codeposti;
            var address = this.fulladdress;
            var plak = this.plak;

            if (codeposti == '' || codeposti == null) {
                $('#licodeposti  .text-error').html('  کد پستی را وارد نمایید.');
                check = 'no';
            }

            if (address == '' || address == null) {
                $('#liaddress  .text-error').html(' آدرس را وارد نمایید.');
                check = 'no';
            }

            if (plak == '' || plak == null) {
                $('#liplak  .text-error').html(' شماره پلاک را وارد نمایید.');
                check = 'no';
            }

            if (ostan == '' || ostan == undefined) {
                $('#liostanedit  .text-error').html(' استان را انتخاب نمایید.');
                check = 'no';
            }

            if (city == '' || city == undefined) {
                $('#licityedit  .text-error').html(' شهر را انتخاب نمایید.');
                check = 'no';
            }


            if (check == 'ok') {
                $('#largeloading').show();
                axios.post('/panel/insertaddress', {
                    codeposti: codeposti,
                    address: address,
                    ostan: ostan,
                    city: city,
                    plak: plak,

                }).then(response => {

                    swal('آدرس با موفقیت ثبت شد.', {
                        icon: "success",
                    });

                    this.codeposti = '';
                    this.fulladdress = '';
                    this.plak = '';

                    this.getaddress();

                    $('#largeloading').hide();
                })
            }

        },

        getaddress: function () {
            $('#largeloading').show();
            axios.post('/panel/getaddress', {}).then(response => {
                this.alladdress = response.data;
                $('#largeloading').hide();
            })
        },
        removeaddress: function (id) {
            swal({
                title: " اطمینان به حذف آدرس دارید؟",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#largeloading').show();
                    axios.post('/panel/removeaddress', {
                        id: id
                    }).then(response => {
                        this.getaddress();
                        $('#largeloading').hide();
                    })

                }
            });


        },


        getallfacturepaneluser: function (page = 1) {
            $('#largeloading').show();
            axios.post('/getallfacturepaneluser?page=' + page, {}).then(response => {
                this.allfacturepaneluser = response.data;
                $('#largeloading').hide();
            })
        },


        getdetailsorder: function (idorder) {

            $('.blockdiscountcode').hide();
            this.discountcode = '';


            $('#largeloading').show();
            axios.post('/getdetailsorder', {
                idorder: idorder,
            }).then(response => {
                this.detailsorder = response.data;

                $.each(this.detailsorder, function (index, title) {
                    var pricefacture = title.pricefacture;
                    var price = Number(pricefacture).toLocaleString();
                    $('.priceend').html('مبلغ نهایی ' + price + ' تومان');
                    $('.blockdiscountcode').hide();
                });


                $('#largeloading').hide();

                var typefacture = response.data[0]['typefacture']['id'];
                if (typefacture == 2 || typefacture == 4) {
                    $('.selectortypepay').find('option[value=0]').remove();
                    $('.selectortypepay').selectpicker('refresh');
                }


            })
        },

        getrizfactures: function (idorder) {
            $('#largeloading').show();
            axios.post('/getrizfactures', {
                idorder: idorder,
            }).then(response => {
                this.rizfactures = response.data;
                $('#largeloading').hide();
            })
        },

        checkdiscountcode: function () {

            $('#largeloading').show();
            var discountcode = this.discountcode;
            axios.post('/checkdiscountcode', {
                discountcode: discountcode,
            }).then(response => {
                var error = response.data.error;
                var success = response.data.success;
                if (success == '') {

                    swal(error, {
                        icon: "warning",
                    });

                    $.each(this.detailsorder, function (index, title) {
                        var pricefacture = title.pricefacture;
                        var price = Number(pricefacture).toLocaleString();
                        $('.priceend').html('مبلغ نهایی ' + price + ' تومان');
                        $('.blockdiscountcode').hide();
                    });

                    this.discountcode = '';


                } else {
                    swal(success, {
                        icon: "success",
                    });

                    var array = response.data.array;

                    var percent = array['percent'];
                    var typediscountcode = array['typediscountcode_id'];
                    var pricediscount = array['price'];

                    $.each(this.detailsorder, function (index, title) {
                        var pricefacture = title.pricefacture;
                        if (typediscountcode == 1) {
                            var takhfif = pricefacture * percent / 100;
                        } else {
                            var takhfif = pricediscount;
                        }

                        $('.blockdiscountcode').show();
                        $('.blockdiscountcode').html('اعمال شد(' + Number(takhfif).toLocaleString() + ' تومان)');
                        var price = Number(pricefacture - takhfif).toLocaleString();
                        $('.priceend').html('مبلغ نهایی ' + price + ' تومان');
                        $('.inputdiscountcode').val(discountcode);

                    });

                }


                $('#largeloading').hide();
            })
        },

        changepassworduser: function () {
            $('#largeloading').show();
            var password = this.newpasswordforchangepassword;
            var passwordconfirmation = this.repeatpasswordforchangepassword;

            axios.post('/changepassworduser', {
                password: password,
                password_confirmation: passwordconfirmation,
            }).then(response => {
                var Success = response.data.Success;
                if (Success !== '') {
                    $('#allertsuccesschangepassword').show();
                    $('#allertsuccesschangepassword').html(Success);
                    $('#cheangepassword_password .text-formerror').hide();

                    this.newpasswordforchangepassword = '';
                    this.repeatpasswordforchangepassword = '';

                } else {
                    var Error = response.data.Error.password[0];
                    $('#allertsuccesschangepassword').hide();
                    $('#cheangepassword_password .text-formerror').html(Error);
                    $('#cheangepassword_password .text-formerror').show();
                }

                $('#largeloading').hide();
            })

        },

        checkpricechargebagmoney: function () {

            $('.text-error').html('');
            var check = 'ok';

            var pricebagmoney = $('.pricebagmoney').val();
            if (pricebagmoney == '' || pricebagmoney == null || pricebagmoney < 4999) {
                $('.lipricebagmoney .text-error').html('مبلغ افزایش اعتبار را وارد نمایید.');
                check = 'no';
            }

            if (check == 'ok') {

                $('#formincreasecredit').submit();

            }


        },

        getreportbagmoney: function (page = 1) {
            $('#largeloading').show();
            axios.post('/getreportbagmoney?page=' + page, {}).then(response => {
                this.allreportbagmoney = response.data;
                $('#largeloading').hide();
            })
        },
        insertrequestmoney: function () {

            var check = 'ok';
            var numbershaba = this.numbershaba;
            var requestmoney = this.requestmoney;

            if (numbershaba == '') {
                check = 'no';
                swal('شماره شبا خود را وارد نمایید.', {
                    icon: "warning",
                });

            } else {

                if (numbershaba.length !== 24) {
                    check = 'no';
                    swal('شماره شبا باید 24 رقم باشد.', {
                        icon: "warning",
                    });
                } else {

                    if (requestmoney == '') {
                        check = 'no';
                        swal('مبلغ درخواستی را وارد نمایید.', {
                            icon: "warning",
                        });
                        this.requestmoney = '';
                    } else {
                        if (requestmoney < 5000) {
                            check = 'no';
                            swal('حداقل مبلغ درخواستی باید بیشتر از 5000 تومان باشد.', {
                                icon: "warning",
                            });
                            this.requestmoney = '';

                        }

                    }
                }
            }

            if (check == 'ok') {

                swal({
                    title: "لطفا به این موارد توجه نمایید",
                    icon: "warning",
                    text: '1-مسولیت صحت شماره شبا بر عهده خود شما می باشد. 2- با ثبت درخواست امکان لغو  یا ویرایش  برای شما وجود نخواهد داشت در صورتی که تمایل به لغو درخواست دارید با پشتیبانی تماس بگیرید.',
                    dangerMode: true,
                    buttons: {
                        cancel: {
                            text: "لغو",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "موافقم",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    },

                }).then((willDelete) => {
                    if (willDelete) {

                        $('#largeloading').show();
                        axios.post('/insertrequestmoney', {
                            numbershaba: numbershaba,
                            requestmoney: requestmoney
                        }).then(response => {

                            var status = response.data.status;
                            var message = response.data.message;
                            if (status == 'error') {
                                swal(message, {
                                    icon: "warning",
                                });
                            } else {
                                swal(message, {
                                    icon: "success",
                                });
                                this.numbershaba = '';
                                this.requestmoney = '';
                                var bagmoney = response.data.bagmoney;
                                $('.bagmoney').html('' + Number(bagmoney).toLocaleString() + ' تومان');
                                this.getreportbagmoney();
                                this.getrequestmoney();
                            }
                            $('#largeloading').hide();
                        })


                    }
                });


            }


        },
        getrequestmoney: function (page = 1) {
            $('#largeloading').show();
            axios.post('/getrequestmoney?page=' + page, {}).then(response => {
                this.allrequestmoney = response.data;
                $('#largeloading').hide();
            })


        },
        showmessagecancelrequestmoney: function (idrequest) {
            $('#largeloading').show();
            axios.post('/showmessagecancelrequestmoney', {
                idrequest: idrequest,
            }).then(response => {
                $('#largeloading').hide();
                var messagecancelrequest = response.data.messagecancelrequest;
                if (messagecancelrequest == null) {
                    messagecancelrequest = 'با پشتیبانی تماس حاصل نمایید.'
                }
                swal(messagecancelrequest, {
                    icon: "warning",
                });

            })

        },


        //panel user -->start
        getrequestcustomer: function (page = 1) {
            $('#largeloading').show();
            axios.post('/panel/getrequestcustomer?page=' + page, {}).then(response => {
                this.allrequestcustomer = response.data;
                this.allrequestcustomernewdata = response.data.data['newdata'];
                $('#largeloading').hide();
            })
        },

        getreportsrequestcustomer: function (page = 1) {
            var idrequest = this.idrequest;
            $('#largeloading').show();
            axios.post('/panel/getreportsrequestcustomer?page=' + page, {
                idrequest: idrequest
            }).then(response => {


                this.allreportrequest = response.data;
                $('#largeloading').hide();
            })
        },


        //panel user -->end

        getrequesttkarshenasan: function (page = 1) {
            $('#largeloading').show();
            axios.post('/panelkarshenasan/getrequesttkarshenasan?page=' + page, {}).then(response => {
                this.allrequesttkarshenasan = response.data;
                this.getamarrequest();
                $('#largeloading').hide();


            })
        },


        getamarrequest: function () {
            $('#largeloading').show();
            axios.post('/panelkarshenasan/getamarrequest', {}).then(response => {

                var numberrequestkol = response.data.numberrequestkol;
                var numberrequestdarhalanjam = response.data.numberrequestdarhalanjam;
                var numberrequestanjamshode = response.data.numberrequestanjamshode;


                $('.numberrequestkol h4').html(numberrequestkol);
                $('.numberrequestdarhalanjam h4').html(numberrequestdarhalanjam);
                $('.numberrequestanjamshode h4').html(numberrequestanjamshode);

                $('#largeloading').hide();


            })
        },


        confirmrequestbykarshenas: function (idrequest) {

            swal({
                title: "مطمئن به تایید درخواست هستید؟",
                text: "با تایید درخواست شما موظف هستید  مشکل درخواست را به طور کامل رفع نمایید.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#largeloading').show();
                    axios.post('/panelkarshenasan/confirmrequestbykarshenas', {
                        'idrequest': idrequest,
                    }).then(response => {
                        this.getrequesttkarshenasan();
                        $('#largeloading').hide();
                    })

                }
            });

        },

        getdetailsrequest: function (idrequest) {

            if (this.withwindow < 630) {
                $('#modelmorerequest .table-responsive').hide();
            } else {
                $('#modelmorerequest .table-responsive').show();
            }

            this.idrequest = idrequest;

            $('#largeloading').show();
            axios.post('/getdetailsrequest', {
                'idrequest': idrequest,
            }).then(response => {
                this.detailsrequest = response.data;

                var page = $('#page').val();
                if (page == 'managerequests') {
                    this.getreportsrequest();
                }

                if (page == 'managerequests-panel') {
                    this.getreportsrequestcustomer();
                }


                $('#largeloading').hide();
            })

        },

        insertreportrequest: function () {

            $('.text-error').html('');

            var check = 'ok';
            var textreport = this.textreportrequest;
            var idrequest = this.idrequest;

            if (textreport == '' || textreport == null) {
                $('#divinsertreportrequest  .text-error').html('متن گزارش را وارد نمایید.');
                check = 'no';
            }

            if (check == 'ok') {
                $('#largeloading').show();
                axios.post('/panelkarshenasan/insertreportrequest', {
                    report: textreport,
                    idrequest: idrequest,
                }).then(response => {
                    swal("گزارش با موفقیت ثبت شد.", {
                        icon: "success",
                    });

                    this.getdetailsrequest(idrequest);
                    this.textreportrequest = '';
                    this.getreportsrequest();
                    $('#largeloading').hide();

                });

            }

        },


        getreportsrequest: function (page = 1) {
            var idrequest = this.idrequest;
            $('#largeloading').show();
            axios.post('/panelkarshenasan/getreportsrequest?page=' + page, {
                idrequest: idrequest
            }).then(response => {
                this.allreportrequest = response.data;
                $('#largeloading').hide();
            })
        },

        deletereportrequest: function (idrequest) {

            $('#largeloading').show();
            axios.post('/panelkarshenasan/deletereportrequest', {
                idrequest: idrequest
            }).then(response => {

                swal("گزارش با موفقیت حذف شد.", {
                    icon: "success",
                });
                this.getdetailsrequest(this.idrequest);
                this.getreportsrequest();
                $('#largeloading').hide();

            })
        },

        addfeilderizfacture: function () {

            $('.text-error').html('');
            var check = 'ok';
            var price = this.priceaddrizfacture;
            var babat = this.babataddrizfacture;

            if (price == '' || price == null) {
                $('#liaddrizfacture_price .text-error').html('مبلغ را وارد نمایید.');
                check = 'no';
            }

            if (babat == '' || babat == null) {
                $('#liaddrizfacture_babat .text-error').html('فیلد نباید خالی باشد.');
                check = 'no';
            }

            if (check == 'ok') {
                var li = '<li class="col-lg-12 col-md-12 col-sm-12 col-12"><i  onclick="closefieldrizfacture(this)" class="tx-red far fa-window-close"></i><div> <input type="hidden" name="babat[]" value="' + babat + '"><p class="tx-primery">' + babat + '</p> </div> <div> <input class="inputprice" type="hidden" name="price[]" value="' + price + '"><p class="tx-red">' + Number(price).toLocaleString() + ' تومان</p></div></li>';
                $('.ulstrizfactore').append(li);

                this.priceaddrizfacture = '';
                this.babataddrizfacture = '';

                var allli = $('.ulstrizfactore li');
                var pricecol = 0;
                $.each(allli, function (index, title) {
                    var price = $(title).find('.inputprice').val();
                    price = Number(price);
                    pricecol = pricecol + price;
                });

                $('.pricekolrizfacture').html('' + Number(pricecol).toLocaleString() + ' تومان');
            }
        },


        sodorfactureforrequest: function () {

            var check = 'ok';

            var allli = $('.ulstrizfactore li');

            if (allli.length == 0) {

                swal('لطفا هزینه ها و ریز فاکتور را ثبت نمایید.', {
                    icon: "warning",
                });
                check = 'no';
            }

            if (check == 'ok') {

                swal({
                    title: "مطمئن به ثبت فاکتور برای این درخواست هستید؟",
                    text: "با ثبت فاکتور امکان ویرایش و حذف آن وجود نخواهد داشت.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#largeloading').show();
                        $('#formsodorfactureforrequest').submit();
                    }
                });


            }


        },


        getfacturerequest: function () {

            var idrequest = this.idrequest;
            $('#largeloading').show();
            axios.post('/panelkarshenasan/getfacturerequest', {
                idrequest: idrequest
            }).then(response => {
                this.facturerequest = response.data;
                $('#largeloading').hide();
            })

        },


        showtypesidebarpanels: function () {
            var width = $(document).width();
            if (width >= 610) {

                $('.logoheaderpanel').css('height', '25px');
                $('.logoheaderpanel').css('width', '145px');

                $('.iconmenue').attr('typemenue', 'largeside');
                $('.iconmenue').removeClass('smallside');
                $('.partavatar').show();
                $('.sidebar-body .limenulevel1 > p').show();
                $('.sidebar-header').css('height', '190px');
                $('.sidebarpanel').css('width', '200px');
                $('.div-content').css('padding-right', '200px');


                $('.paddingnone').css('padding-right', '15px');
                $('.paddingnone').css('padding-left', '15px');
                $('section').css('padding-left', '15px');
                $('section').css('padding-right', '15px');

            } else {


                $('.paddingnone').css('padding-right', '0');
                $('.paddingnone').css('padding-left', '0');
                $('section').css('padding-left', '5px');
                $('section').css('padding-right', '5px');


                $('.logoheaderpanel').css('height', '17px');
                $('.logoheaderpanel').css('width', '90px');

                $('.iconmenue').attr('typemenue', 'smallside');
                $('.iconmenue').removeClass('largeside');

                $('.partavatar').hide();
                $('.sidebar-body .limenulevel1 > p').hide();
                $('.sidebar-header').css('height', '60px');
                $('.sidebarpanel').css('width', '55px');
                $('.div-content').css('padding-right', '55px');

            }
        },
        saveformrequest: function () {

            $('#largeloading').show();

            axios.post('/index/saveformrequest', {
                name: this.namecustomer,
                family: this.familycustomer,
                phonenumber: this.phonenumbercustomer,
                address: this.addresscustomer,
                description: this.descriptioncustomer

            }).then(response => {
                var error = response.data.error;
                var success = response.data.success;

                if (error == '') {

                    $('#modelsendrequest .modal-body > p').remove();
                    var er = '<p style="text-align: right;font-family: yekan;margin-top: 5px" class="alert alert-success">' + success + '</p>';
                    $('#modelsendrequest .modal-body').append(er);

                } else {
                    $('#modelsendrequest .modal-body  > p').remove();
                    er = '<p style="text-align: right;font-family: yekan;margin-top: 5px" class="alert alert-danger">' + error + '</p>';
                    $('#modelsendrequest .modal-body').append(er);


                }

                $('#largeloading').hide();

            })


        },
        saveemail: function () {


            $('#largeloading').show();
            axios.post('/index/saveemail', {
                email: this.email,
            }).then(response => {
                var error = response.data.error;

                var success = response.data.success;


                console.log(response.data);

                if (error == '') {


                    swal('ثبت نام شما با موفقیت در خبرنامه انجام شد.', {
                        icon: "success",
                    });


                } else {


                    swal(error, {
                        icon: "warning",
                    });

                }

                $('#largeloading').hide();

            })


        },


        //recovery password

        checkmobile: function () {
            var check = 'yes';
            var mobile = this.mobilerecpass;
            if (mobile == '') {
                check = 'no';
                swal('موبایل خود را وارد نمایید.', {
                    icon: "warning",
                });
            }

            if (check == 'yes') {
                //  $('#largeloading').show();
                axios.post('/checkmobile', {
                    mobile: mobile
                }).then(response => {

                    var status = response.data.status;
                    var error = response.data.error;
                    var success = response.data.success;

                    if (status == 'success') {
                        $('.step1').remove();
                        var mainseconds = 150;
                        var x = setInterval(function () {
                            var seconds = mainseconds - 1;

                            document.getElementById("secoundcounter").innerHTML = seconds;
                            if (mainseconds == 1) {
                                clearInterval(x);
                                $('#getagainverifiedcode').show();

                            } else {

                                mainseconds = mainseconds - 1
                            }

                        }, 1000);


                        $('.step2').show();
                        /* $('#register .main')*/
                        // Verifiedcode


                        swal(success, {
                            icon: "success",
                        });


                    } else {
                        swal(error, {
                            icon: "warning",
                        });
                    }

                    $('#largeloading').hide();
                })

            }


        },
        getagainverifiedcode: function () {

            var check = 'yes';
            var mobile = this.mobilerecpass;
            if (mobile == '') {
                check = 'no';
                swal('شماره موبایل یافت نشد.', {
                    icon: "warning",
                });

                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);

            }

            if (check == 'yes') {

                $('#largeloading').show();
                axios.post('/getagainverifiedcode', {
                    mobile: mobile
                }).then(response => {
                    var status = response.data.status;
                    var error = response.data.error;
                    var success = response.data.success;


                    if (status == 'success') {
                        $('#getagainverifiedcode').hide();
                        var mainseconds = 150;
                        var x = setInterval(function () {
                            var seconds = mainseconds - 1;
                            document.getElementById("secoundcounter").innerHTML = seconds;
                            if (mainseconds == 1) {
                                clearInterval(x);
                                $('#getagainverifiedcode').show();
                            } else {
                                mainseconds = mainseconds - 1
                            }

                        }, 1000);


                        swal(success, {
                            icon: "success",
                        });

                    } else {
                        swal(error, {
                            icon: "warning",
                        });
                    }

                    $('#largeloading').hide();
                })

            }

        },
        checkverifiedcode: function () {

            var check = 'yes';
            var mobile = this.mobilerecpass;
            var verifiedcode = this.Verifiedcode;
            if (mobile == '') {
                check = 'no';
                swal(' شماره موبایل  یافت نشد.', {
                    icon: "warning",
                });

                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);

            }

            if (verifiedcode == '') {
                check = 'no';
                swal('کد اعتبارسنجی را وارد نمایید.', {
                    icon: "warning",
                });
            }

            if (check == 'yes') {

                $('#largeloading').show();
                axios.post('/checkverifiedcode', {
                    mobile: mobile,
                    verifiedcode: verifiedcode,
                }).then(response => {
                    var status = response.data.status;
                    var error = response.data.error;

                    if (status == 'success') {

                        $('.step1').remove();
                        $('.step2').remove();
                        $('.step3').show();

                    } else {

                        swal(error, {
                            icon: "warning",
                        });

                    }


                    $('#largeloading').hide();
                })

            }


        },
        changepasswordrecovery: function () {

            var check = 'yes';
            var mobile = this.mobilerecpass;
            var verifiedcode = this.Verifiedcode;
            var password = this.passwordrecovery;
            var repeatpassword = this.repeatpasswordrecovery;

            if (mobile == '') {
                check = 'no';
                swal(' شماره موبایل یافت نشد.', {
                    icon: "warning",
                });

                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);

            }

            if (verifiedcode == '') {
                check = 'no';
                swal('کد اعتبارسنجی  یافت  نشد.', {
                    icon: "warning",
                });

                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);

            }


            if (password !== '' && repeatpassword !== '') {

                if (password.length < 6) {
                    check = 'no';
                    swal(' پسورد باید بیشتر از 6 کارکتر باشد.', {
                        icon: "warning",
                    });

                } else {
                    if (password !== repeatpassword) {
                        check = 'no';
                        swal('پسورد با هم مطابقت ندارد.', {
                            icon: "warning",
                        });
                    }

                }


            } else {

                check = 'no';
                swal('پسور جدید را وارد نمایید.', {
                    icon: "warning",
                });

            }


            if (check == 'yes') {

                $('#largeloading').show();
                axios.post('/changepasswordrecovery', {
                    mobile: mobile,
                    verifiedcode: verifiedcode,
                    password: password,
                }).then(response => {

                    var status = response.data.status;
                    var error = response.data.error;
                    var success = response.data.success;
                    if (status == 'success') {

                        swal(success, {
                            icon: "success",
                        });

                        setTimeout(function () {
                            window.location.href = '/login';
                        }, 3000);


                    } else {

                        swal(error, {
                            icon: "warning",
                        });


                        setTimeout(function () {
                            window.location.href = '/';
                        }, 3000);

                    }


                    $('#largeloading').hide();
                })

            }


        },


        //calculation system

        pricecalculatemaincatchosed: function () {

            $('#largeloading').show();
            //  $('#selectorpricecalculatemodeldevice option').remove();


            $('#selectorpricecalculateservice option').remove();
            $('#selectorpricecalculateberand option').remove();


            var idmaincat = $('#selectorpricecalculatemaincat').find('option:selected').val();
            axios.post('/pricecalculatemaincatchosed', {
                idmaincat: idmaincat
            }).then(response => {
                var services = response.data.pricecalculateservices;
                var berands = response.data.pricecalculatebernaddevices;
                $('#largeloading').hide();


                $.each(services, function (index, title) {
                    var option = '<option value="' + title.id + '">' + title.title + '</option>';
                    $('#selectorpricecalculateservice').append(option);

                });


                $.each(berands, function (index, title) {
                    var option = '<option value="' + title.id + '">' + title.title + '</option>';
                    $('#selectorpricecalculateberand').append(option);
                });


                if (services.length > 0) {
                    $('.selectorpricecalculateservice').selectpicker('refresh');
                } else {

                    $('.selectorpricecalculateservice').selectpicker('render');
                }

                if (berands.length > 0) {
                    $('.selectorpricecalculateberand').selectpicker('refresh');
                } else {

                    $('.selectorpricecalculateberand').selectpicker('render');
                }

                this.getpricecalculatemodeldevice();


            })

        },
        getpricecalculatemodeldevice: function () {

            $('#largeloading').show();

            var idberand = $('#selectorpricecalculateberand').find('option:selected').val();
            axios.post('/getpricecalculatemodeldevice', {
                idberand: idberand
            }).then(response => {

                var modeldevice = response.data;
                $('#largeloading').hide();
                $('#selectorpricecalculatemodeldevice option').remove();
                $.each(modeldevice, function (index, title) {
                    var option = '<option value="' + title.id + '">' + title.title + '</option>';
                    $('#selectorpricecalculatemodeldevice').append(option);
                });

                if (modeldevice.length > 0) {
                    $('.selectorpricecalculatemodeldevice').selectpicker('refresh');
                } else {
                    $('.selectorpricecalculatemodeldevice').selectpicker('render');
                }


            })

        },

        cecalculatecost: function () {

            $('.texttravelexpenses').html(0);
            $('.texttotalpricecomponnent').html('0');
            $('.textpricekol').html('');
            $('.textdastmozdtamir').html(0);
            $('.alertnotfounddevice').hide();


            var check = 'ok';
            var idmaincat = $('#selectorpricecalculatemaincat').find('option:selected').val();
            var idberand = $('#selectorpricecalculateberand').find('option:selected').val();
            var idservice = $('#selectorpricecalculateservice').find('option:selected').val();
            var idmodeldevice = $('#selectorpricecalculatemodeldevice').find('option:selected').val();

            if (idmaincat == '' || idmaincat == undefined) {
                $('#selectorpricecalculatemaincat').parent('div').find('.btn').css('border', '1px solid red');
                check = 'no';
            } else {
                $('#selectorpricecalculatemaincat').parent('div').find('.btn').css('border', '1px solid #eee');

            }
            if (idberand == '' || idberand == undefined) {
                $('#selectorpricecalculateberand').parent('div').find('.btn').css('border', '1px solid red');
                check = 'no';
            } else {
                $('#selectorpricecalculateberand').parent('div').find('.btn').css('border', '1px solid #eee');
            }

            if (idservice == '' || idservice == undefined) {
                $('#selectorpricecalculateservice').parent('div').find('.btn').css('border', '1px solid red');
                check = 'no';
            } else {
                $('#selectorpricecalculateservice').parent('div').find('.btn').css('border', '1px solid #eee');
            }

            if (idmodeldevice == '' || idmodeldevice == undefined) {
                $('#selectorpricecalculatemodeldevice').parent('div').find('.btn').css('border', '1px solid red');
                check = 'no';
            } else {
                $('#selectorpricecalculatemodeldevice').parent('div').find('.btn').css('border', '1px solid #eee');
            }


            if (check == 'ok') {

                $('#largeloading').show();
                axios.post('/cecalculatecost', {
                    idmaincat: idmaincat,
                    idservice: idservice,
                    idberand: idberand,
                    idmodeldevice: idmodeldevice,
                }).then(response => {

                    var pricekol = response.data.pricekol;
                    var totalpricecomponnent = response.data.totalpricecomponnent;
                    var travelexpenses = response.data.travelexpenses;
                    var dastmozdtamir = response.data.dastmozdtamir;
                    var status = response.data.status;

                    if (travelexpenses == 0) {

                        $('.texttravelexpenses').html('رایگان');
                    } else {
                        $('.texttravelexpenses').html('' + Number(travelexpenses).toLocaleString() + ' تومان');
                    }

                    if (totalpricecomponnent == 0) {
                        $('.texttotalpricecomponnent').html('بدون احتساب قیمت قطعه');
                    } else {
                        $('.texttotalpricecomponnent').html('' + Number(totalpricecomponnent).toLocaleString() + ' تومان');
                    }


                    $('.textpricekol').html('' + Number(pricekol).toLocaleString() + ' تومان');
                    $('.textdastmozdtamir').html('' + Number(dastmozdtamir).toLocaleString() + ' تومان');


                    if (status == 'ok') {

                        $('.alertnotfounddevice').hide();

                    } else {

                        $('.alertnotfounddevice').show();

                    }


                    $('#largeloading').hide();
                })


            } else {

                swal('لطفا تمام موارد را تکمیل نمایید.', {
                    icon: "warning",
                });

            }


        },


    },
    watch: {


        getsearchkhadamat: function (value) {


            if (value == '') {

                $('.contentsearch').hide();
            } else {
                $('.contentsearch').show();
            }

            axios.post('/getsearchkhadamat', {
                search: value
            }).then(response => {

                this.searchkhadamat = response.data;


            })

        },


        namecustomer: function (value) {
            axios.post('/index/validdetailsformsaverequest', {
                name: value
            }).then(response => {
                var errorname = response.data.name;

                if (errorname == undefined) {
                    $('#namecustomer p').remove();
                } else {
                    var error = ' <p style="color: #ff3535 !important;float: right;margin-top: 3px;font-size: 10pt;margin-bottom: 4px;direction: rtl;" class="">' + errorname[0] + '</p>';
                    $('#namecustomer p').remove();
                    $('#namecustomer').append(error);
                }

            })


        },
        familycustomer: function (value) {

            axios.post('/index/validdetailsformsaverequest', {
                family: value
            }).then(response => {
                var errorfamily = response.data.family;
                console.log(errorfamily);
                if (errorfamily == undefined) {
                    $('#familycustomer p').remove();
                } else {
                    var error = ' <p style="color: #ff3535 !important;float: right;margin-top: 3px;font-size: 10pt;margin-bottom: 4px;direction: rtl;" class="">' + errorfamily[0] + '</p>';
                    $('#familycustomer p').remove();
                    $('#familycustomer').append(error);
                }

            })


        },
        phonenumbercustomer: function (value) {
            axios.post('/index/validdetailsformsaverequest', {
                phonenumber: value
            }).then(response => {
                var errorphone = response.data.phonenumber;
                if (errorphone == undefined) {
                    $('#phonenumbercustomer p').remove();

                } else {

                    var error = ' <p style="color: #ff3535 !important;float: right;margin-top: 3px;font-size: 10pt;margin-bottom: 4px;direction: rtl;" class="">' + errorphone[0] + '</p>';
                    $('#phonenumbercustomer p').remove();
                    $('#phonenumbercustomer').append(error);
                }

            })


        },
        addresscustomer: function (value) {
            axios.post('/index/validdetailsformsaverequest', {
                address: value
            }).then(response => {
                var erroraddress = response.data.address;
                if (erroraddress == undefined) {
                    $('#addresscustomer p').remove();
                } else {
                    var error = ' <p style="color: #ff3535 !important;float: right;margin-top: 3px;font-size: 10pt;margin-bottom: 4px;direction: rtl;" class="">' + erroraddress[0] + '</p>';
                    $('#addresscustomer p').remove();
                    $('#addresscustomer').append(error);
                }

            })


        },
        descriptioncustomer: function (value) {
            axios.post('/index/validdetailsformsaverequest', {
                description: value
            }).then(response => {
                var errordescription = response.data.description;
                if (errordescription == undefined) {
                    $('#descriptioncustomer p').remove();
                } else {
                    var error = ' <p style="color: #ff3535 !important;float: right;margin-top: 3px;font-size: 10pt;margin-bottom: 4px;direction: rtl;" class="">' + errordescription[0] + '</p>';
                    $('#descriptioncustomer p').remove();
                    $('#descriptioncustomer').append(error);
                }

            })


        },

        searchservice: function (value) {

            var id = this.selectedmaincat;
            if (id !== '') {
                axios.post('/getservice', {
                    idmaincat: id,
                    search: value,
                }).then(response => {
                    this.allservice = response.data;

                });
            }


        },


        searchberand: function (value) {

            var id = this.selectedmaincat;
            if (id !== '') {
                axios.post('/getberand', {
                    idmaincat: id,
                    search: value,
                }).then(response => {
                    this.allberand = response.data;

                });
            }


        },

        searchmodel: function (value) {

            var id = this.selectedberand;


            if (id !== '') {
                axios.post('/getmodel', {
                    idberand: id,
                    search: value,
                }).then(response => {
                    this.allmodel = response.data;

                });
            }


        },


        rate: function (value) {
            $('.inputrate').val(value);

        },

    }


});


window.onresize = function (event) {

    var width = $(window).width();
    this.withwindow = width;
    if (width < 630) {
        $('.flat').show();
        $('.table-responsive').hide();
    } else {
        $('.flat').hide();
        $('.table-responsive').show();
    }

    if (width >= 600) {
        $('.logoheaderpanel').css('height', '25px');
        $('.logoheaderpanel').css('width', '145px');
        $('.iconmenue').attr('typemenue', 'largeside');
        $('.iconmenue').removeClass('smallside');
        $('.partavatar').show();
        $('.sidebar-body .limenulevel1 > p').show();
        $('.sidebar-header').css('height', '190px');
        $('.sidebarpanel').css('width', '200px');
        $('.div-content').css('padding-right', '200px');


        $('.paddingnone').css('padding-right', '15px');
        $('.paddingnone').css('padding-left', '15px');
        $('section').css('padding-left', '15px');
        $('section').css('padding-right', '15px');

    } else {

        $('.logoheaderpanel').css('height', '17px');
        $('.logoheaderpanel').css('width', '90px');

        $('.iconmenue').attr('typemenue', 'smallside');
        $('.iconmenue').removeClass('largeside');

        $('.partavatar').hide();
        $('.sidebar-body .limenulevel1 > p').hide();
        $('.sidebar-header').css('height', '60px');
        $('.sidebarpanel').css('width', '55px');
        $('.div-content').css('padding-right', '55px');


        $('.paddingnone').css('padding-right', '0');
        $('.paddingnone').css('padding-left', '0');
        $('section').css('padding-left', '5px');
        $('section').css('padding-right', '5px');

    }
};


var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
(function () {
    var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/5f60cece4704467e89ef2782/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})();


$(document).ready(function () {
    $('#largeloading').hide();
    var withscreen = $(window).width();
    if (withscreen > 630) {
        $('.flat').hide();
        $('.table-responsive').show();
    } else {
        $('.flat').show();
        $('.table-responsive').hide();
    }

    var widthwindow = $(window).width();
    if (widthwindow <= 585) {
        $('.selectpicker').selectpicker('mobile');
        setTimeout(function () {

            $('body iframe').eq(0).css({
                "height": "35px",
                "min-height": "35px",
                "max-height": "35px",
                "width": "35px",
                "min-width": "35px",
                "max-width": "35px",
                "bottom": "20px",
                "right": "15px",
            });

            $('body iframe ').eq(0).contents().find('#tawkchat-minified-box.round').css({
                "height": "35px",
                "width": "35px",
                "left": "0",
                "position": "absolute"
            });
            $('body iframe ').eq(0).contents().find('.round #tawkchat-minified-wrapper').css({
                "height": "35px",
                "width": "35px",
                "bottom": "0",
            });

            $('body iframe ').eq(0).contents().find('#tawkchat-status-text-container').css({
                "height": "35px",
                "width": "35px",
                "left": "0",
                "position": "absolute"
            });
            $('body iframe ').eq(0).contents().find('#tawkchat-status-text-container svg').css({
                "height": "20px",
                "width": "20px",
                "margin-top": "8px",
            });

            $('body iframe ').eq(0).contents().find('#short-message,#maximizeChat,#minimizeChatMinifiedBtn').css({
                "font-size": "20px",
                "line-height": "40px",

            });

            $('body iframe ').eq(0).contents().find('.round #tawkchat-status-agent-container #agent-profile-image').css({
                "height": "35px",
                "width": "35px",
                "background-size": "35px",
            });


            $('body iframe ').eq(2).css({
                "height": "18px",
                "min-height": "18px",
                "max-height": "18px",
                "width": "18px",
                "min-width": "18px",
                "max-width": "18px",
                "bottom": "20px",
                "right": "40px"
            });
            $('body iframe ').eq(2).contents().find('#tawkchat-chat-indicator-text').css({
                "font-size": "11px",
                "line-height": "18px",
            });

        }, 3000);

    } else {

        $('body iframe ').eq(0).contents().find('#greetingsText').css({
            "font-family": "yekan",
        });

        $('body iframe ').eq(0).contents().find('.messageWrapper .message').css({
            "font-family": "yekan",
        });
    }


    $(document).scroll(function () {
        if (widthwindow <= 585) {

            setTimeout(function () {
                $('body iframe').eq(0).css({
                    "height": "35px",
                    "min-height": "35px",
                    "max-height": "35px",
                    "width": "35px",
                    "min-width": "35px",
                    "max-width": "35px",
                    "bottom": "20px",
                    "right": "15px",
                });

            }, 110)


        }


    });


});
