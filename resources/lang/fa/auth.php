<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /* 'failed' => 'نام کاربری با پسورد مطابقت ندارد.',
     'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',*/

    'failed' => 'نام کاربری و یا رمز عبور اشتباه می باشد',
    'throttle' => 'شما تلاش های زیادی برای ورود به سیستم کرده اید . لطفا :seconds صبر نمایید و دوباره تلاش کنید.',

];
