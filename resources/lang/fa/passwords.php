<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    /*  'password' => 'Passwords must be at least six characters and match the confirmation.',
      'reset' => 'Your password has been reset!',
      'sent' => 'We have e-mailed your password reset link!',
      'token' => 'This password reset token is invalid.',
      'user' => "We can't find a user with that e-mail address.",*/


    "password" => "رمز عبور باید حداقل شش کاراکتر و مطابقت داشته باشد.",
    "user" => "کاربری با این ایمیل آدرس یافت نشد.",
    "token" => "مشخصه ی بازنشاندن رمز عبور اشتباه است.",
    "sent" => "یاد آور رمز عبور ارسال شد.",
    "reset"    => "رمز عبور بازنشانی شد!",

];
