@include('header')




    <section class="service-intro" style="margin-bottom: 15px">
        <div class="white rounded card">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div style="float: right" class="col-md-4">
                    {{--<img class="imagepageservice" style="width: 100%;height: 100%;" src="{{\App\Setting::first()->root}}img/khadamat/{{$khedmat->img}}"/>--}}
                    <img class="imagepageservice"  src="{{asset('/index/images/services/defualt.jpg')}}"/>
                </div>
                <div style="float: right" class="col-md-8 text-right nowrap">
                    <div class="service-content  mr-4 ml-4">
                        <h3 style="margin-top: 10px;text-align: center;font-size: 13pt" class="h3-responsive">
                            {{$khedmat->titlepage}}
                        </h3>

                        <p class="service-description mt-3 text-justify">
                            {{$khedmat->summarytext}}
                        </p>
                        <hr>
                        <button class=" btn  btn-cyan  request-btn mr-5 ">
                            <i class="fas fa-phone"></i>
                            مشاوره رایگان
                        </button>
                    <!--
                <button v-on:click="selectmaincat({{$khedmat->id}})" data-toggle="modal"  data-target="#modalinsertrequest" class=" btn  btn-cyan  request-btn mr-5 ">
                  ثبت سفارش
                </button>
                -->
                    </div>
                </div>

                {{--<div class="border-right">--}}
                {{--<div style="margin-bottom:20px" class="service-image  card mask cyan rounded mt-5 mr-4 ml-4">--}}
                {{--</div>--}}
                {{--</div>--}}



            </div>
        </div>
    </section>



    <section style="background-color: white !important;"  class="mt-1">

        <div class="container">
            <div class="arrow-sec text-left lg-device-display-none ">
                <div class="arr-item ">
                    <i class="fas fa-angle-double-left arrow-left fa-3x grey-text" style="font-size: 15pt;"
                       aria-hidden="true"></i>
                </div>
            </div>

            <div style="width: 100%;margin-top: 30px;display:inline-block;">
                <div  class="owl-carousel">
                    @foreach($khedmat->problems as $row)
                        <div style="width: 100%" class="hoverable   mb-5 rounded ">
                            <!-- Card -->
                            <div class="card ">
                                <!-- Card image -->
                                <div class="view">
                                    <img style="height:150px" class="card-img-top " src="{{\App\Setting::first()->root}}img/service/{{$row->img}}" alt="Card image cap">
                                    <div class="mask flex-center waves-effect waves-light aqua-cyan-light">
                                        <p class="white-text overlay"></p>
                                    </div>

                                </div>


                                <!-- Card content -->
                                <div style="height: 95px" class="card-body">
                                    <!-- Title -->
                                    <h4 style="font-size: 11pt;" class="card-title text-center">{{$row->title}}</h4>
                                <!--    <h4 style="font-size: 13pt;color:red" class="card-title text-center">{{number_format($row->tarefe)}} تومان</h4> -->
                                    <!-- Text -->
                                </div>

                                <!-- Card footer -->
                                <div  class="rounded-bottom  text-center">
                                    <!-- Button -->
                                    <a v-on:click="chosedservicesforinsertmodal({{$row->id}})" style="color:#fff !important" class="btn btn-action  mdb-color darken-4 rounded  red">
                                        بسپارش به ما
                                    </a>

                                </div>

                            </div>
                            <!-- Card -->
                        </div>
                    @endforeach
                </div>
            </div>


        </div>
    </section>
    <section style="margin-top: 10px;margin-bottom: 10px" class="service-info-section">
        <div >
            <div>
                <div class="white card rounded">
                    <div>
                        <div class="service-info mr-4 mt-4 ml-4 text-center">
                            <h5 class="h4-Responsive">
                                توضیحات مربوط به سرویس
                            </h5>
                            <p class="text-justify pt-2">
                                {{$khedmat->moretext }}
                            </p>

                        </div>

                    </div>
                </div>

            </div>

        </div>

    </section>

  @include('footer')

