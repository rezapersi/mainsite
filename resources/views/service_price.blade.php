@include('header')

<section class="sectioncalculationsystem">
    <div class="container white rounded z-depth-1">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">


                <!--page title -->
                <h4 style="font-size: 15pt" class="h4-responsive text-center pt-5"> سیستم محاسبه هزینه خدمات دیجی یار </h4>

                <div>
                    <table class="table text-center">
                        <thead>
                        <tr>
                            <th scope="col"> نوع دستگاه</th>
                            <th scope="col">برند دستگاه</th>
                            <th scope="col">مدل دستگاه</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select id="selectorpricecalculatemaincat" v-on:change="pricecalculatemaincatchosed"
                                        class="selectpicker" data-width="100%" data-title="انتخاب">
                                    @foreach($pricecalculatemaincat as $row)
                                        <option value="{{$row->id}}">{{$row->title}}</option>
                                    @endforeach
                                </select>

                            </td>


                            <td>
                                <select v-on:change="getpricecalculatemodeldevice" id="selectorpricecalculateberand"
                                        class="selectpicker selectorpricecalculateberand" data-width="100%">


                                </select>

                            </td>


                            <td>

                                <select id="selectorpricecalculatemodeldevice"
                                        class="selectpicker selectorpricecalculatemodeldevice" data-width="100%"
                                        data-live-search="true">


                                </select>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-center">
                    <h4 class="h4-responsive">
                        عنوان سرویس
                    </h4>

                    <select id="selectorpricecalculateservice" class="selectpicker selectorpricecalculateservice"
                            data-width="100%">

                    </select>


                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center pt-2">

                            <button v-on:click="cecalculatecost" type="button" class="btn btn-success btn-rounded ">
                                محاسبه قیمت
                            </button>
                        </div>


                        <ul class="ulshowclaculationcost">
                            <li>
                                <h4>

                                    هزینه ایاب و ذهاب:
                                </h4>
                                <p class="texttravelexpenses">0</p>

                            </li>

                            <li>
                                <h4>

                                    دستمزد تعمیر :
                                </h4>
                                <p class="textdastmozdtamir">0</p>

                            </li>


                            <li>
                                <h4>

                                     قیمت قطعه :
                                </h4>
                                <p class="texttotalpricecomponnent">0</p>

                            </li>


                        </ul>




                        <h4 class="h4-responsive text-center red-text">
                            قیمت نهایی

                 <p style="color: green;font-size: 12pt;margin-top: 10px;" class="textpricekol"></p>

                        </h4>


                        <p style="display: none" class="alertnotfounddevice alert alert-danger">برای اطلاع از هزینه تعمیر دستگاه خود درخواست خود را ثبت کنید. </p>

                        <div class="py-3 text-center">
                            <button v-on:click="defaultinsertrequest" data-toggle="modal" data-target="#modalinsertrequest" type="button" class="btn btn-primary   ">
                                ثبت سفارش
                            </button>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>


</section>

@include('footer')
