<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<title>اسم سایت</title>--}}

    @include('source.source')
    <link href="{{asset('/css/loginpanel.css')}}" rel="stylesheet">


</head>
<style>
  
    .secoundcounter {
        width: 50px;
        height: 50px;
        text-align: center;
        color: #3b7d16;
        border-radius: 100px;
        position: relative;
        right: 0;
        left: 0;
        margin: auto;
        display: block;
        background-color: #f4f4f4;
        font-size: 14pt;
        line-height: 50px;

    }

    .getagainverifiedcode {
        float: right;
        width: 100%;
        color: rgb(255, 78, 78);
        font-size: 11pt;
        cursor: pointer;
        text-align: center;
        margin-top: 10px;
        display: none;

    }

</style>
<body>

{{--@include('source.headerfix')--}}

<div id="largeloading" class="loading">

    <div>

        <h4>لطفا چند
            لحظه صبر نمایید ...</h4>
        <span>
            <img src="{{asset('/loading/three-dots.svg')}}">
            </span>
    </div>

</div>


<div style="float: right" class="container-fluid">

    {{--@include('source.headerfix')--}}
    <div id="app">
        <div id="register">
            <div class="main">



           {{--     <div class="imgheaderformlogin col-lg-6 col-md-8 col-sm-10 col-12">
                    <a href="/">
                        <img src="{{asset('/images/logo.png')}}">
                    </a>
                </div>
--}}


                <div style="display: block" class="step1">

                    <div style="margin-top: 60px" class="formsend contactus">
                        <div style="min-height: 180px" class="col-lg-4 col-md-5 col-sm-7 col-12">


                            <div style="width: 100%;min-height: 70px;"  class="imgheaderformlogin">
                                <a href="/">
                                    <img src="{{asset('/images/logo.png')}}">
                                </a>
                            </div>


                            <div class="divusername col-lg-12 col-md-12">
                                <label>شماره موبایل</label>
                                <input v-model="mobilerecpass" style="font-family: Tahoma;height: 50px;" name="key"
                                       class="form-control  ">
                            </div>
                            <div style="min-height: 50px" class="col-lg-12">
                                <button v-on:click="checkmobile"
                                        style="width: 100%;height: 45px;text-align: center;border-radius: 3px;border: none;"
                                        type="submit" class="btn aqua-gradient">دریافت کد
                                </button>
                            </div>

                        </div>


                    </div>

                </div>
                <div style="display:none" class="step2">

                    <div style="margin-top: 60px" class="formsend contactus">

                        <div style="min-height: 180px" class="col-lg-4 col-md-6 col-sm-8 col-12">


                            <div style="width: 100%;min-height: 70px;"  class="imgheaderformlogin">
                                <a href="/">
                                    <img src="{{asset('/images/logo.png')}}">
                                </a>
                            </div>


                            <div class="divmobile col-lg-12 col-md-12">
                                <label> کد تایید را وارد نمایید</label>
                                <input v-model="Verifiedcode" placeholder="- - - - -"
                                       style="font-family: Tahoma;height: 50px;text-align: center;font-size: 17pt !important;"
                                       name="key" class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="5">

                            </div>

                            <div class=" col-lg-12 col-md-12">

                                <span id="secoundcounter" class="secoundcounter">150</span>
                                <p id="getagainverifiedcode" style="text-align: center"
                                   v-on:click="getagainverifiedcode" class="getagainverifiedcode">دریافت مجددا کد
                                    تایید</p>
                            </div>

                            <div style="min-height: 50px" class="col-lg-12">
                                <button v-on:click="checkverifiedcode"
                                        style="width: 100%;height: 45px;text-align: center;border-radius: 3px;border: none;"
                                        type="submit" class="btn aqua-gradient"> تایید کد
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <div style="display:none" class="step3">

                    <div style="margin-top: 60px" class="formsend contactus">

                        <div style="min-height: 180px" class="col-lg-4 col-md-6 col-sm-8 col-12">


                            <div style="width: 100%;min-height: 70px;"  class="imgheaderformlogin">
                                <a href="/">
                                    <img src="{{asset('/images/logo.png')}}">
                                </a>
                            </div>



                            <div class=" col-lg-12 col-md-12">
                                <label>رمز عبور جدید</label>
                                <input type="password" v-model="passwordrecovery" style="font-family: Tahoma;height: 50px;text-align: center;font-size: 17pt !important;"  class="form-control">
                            </div>

                            <div class=" col-lg-12 col-md-12">
                                <label>تکرار رمز عبور جدید</label>
                                <input type="password" style="font-family: Tahoma;height: 50px;text-align: center;font-size: 17pt !important;" v-model="repeatpasswordrecovery" class="form-control  ">
                            </div>


                            <div style="min-height: 50px" class="col-lg-12">
                                <button v-on:click="changepasswordrecovery" style="width: 100%;height: 45px;text-align: center;border-radius: 3px;border: none;" type="submit" class="btn aqua-gradient"> تغییر رمز عبور
                                </button>
                            </div>


                        </div>


                    </div>


                </div>


                <a href="/login">
                            <p style="text-align: center"
                               class="font-small gotolink  col-lg-12 col-md-12 col-sm-12 col-12"> قبلا ثبت نام کرده ام</p>
                        </a>

            </div>
        </div>
    </div>
</div>


<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/mdb.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.js')}}"></script>
{{--@include('source.footerfix')--}}
