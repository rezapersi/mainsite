<!--<div style="background-color:#fff;width:100%;height:200px">-->
<!--     <select id="ostan" name="ostan" onchange="selectostan(this);"   class="selectpicker">-->

<!--            <option>1</option>-->
<!--            <option>2</option>-->

<!--       </select>-->
<!--</div>-->


<!-- ###################################################################
                    Footer section mobile version
############################################################################ -->


<footer class="page-footer font-small cyan darken-4  pt-5 lg-device-display-none" id="footer">


    <!-- Footer Elements -->
    <div class="container ">


        <!-- Grid row-->
        <div class="row">

            <!--Grid column-->
            <div class="col-md-6 mb-4 text-center ">

                <!-- social icon -->
                <h2 class="h2-responsive">
                    درباره ما:
                </h2>
                <p class="text-justify">
                    ما در دیجی یار معتقدیم که دانش و تخصص بدون تجربه و تعهد کارآمد نیست. از این رو متعهدیم، با به
                    کار گیری دانش و تجربه شبکه متخصصین خود مشکلات نرم افزاری و سخت افزاری شما را در سریع ترین زمان
                    ممکن حل کنیم و پاسخگوی درخواست های شما، به
                    صورت
                    حضوری و غیر حضوری باشیم.

                </p>


                <!-- Form -->

            </div>
            <!--Grid column-->
            <!--Grid column-->
            <div class="col-md-6 mb-4 text-center">
                <h5 class="h5-responsive">
                    عضویت در خبرنامه:
                </h5>
                <h6 class="h6-responsive text-right">
                    با عضویت در خبر نامه دیجی یار از تخفیف های ما با خبر شوید.
                </h6>
                <form class="input-group">
                    <input v-model="email" type="text" class="form-control form-control-sm"
                           placeholder="آدرس ایمیل خود را وارد کنید..." aria-label="Your email"
                           aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button v-on:click="saveemail" class="btn btn-sm btn-outline-white my-0" type="button">
                            عضویت
                        </button>
                    </div>
                </form>
                <hr>
                <h5 class="h5-responsive">
                    تماس با ما:
                </h5>
                <p>

                </p>
                <p>
                    پشتیبانی: 09010181932 </p>
                <div class="social-icon mt-2">
                    <!--Instagram-->
                    <a href="https://www.instagram.com/digiyar.co/">
                        <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <i class="fab fa-telegram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    <i class="fas fa-map-marker-alt fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>

                </div>

            </div>

            <!--Grid column-->

        </div>
        <hr>
        <!--Grid row-->
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="row">
            <div class="col-md-12 text-center grey-text ">
                <img src="{{asset('/index/images/logo/grayscale.png')}}" alt="" WIDTH=160 HEIGHT=30 align="middle"
                     class="mb-2">
                <p class="p-Responsive pt-1 ">
                    کلیه حقوق این وبسایت متعلق به گروه دیجی یار است.
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid">


        <div class="smooth-scroll text-center ">
            <a class="btn-floating btn-lg btn-elegant z-depth-0" href="#header"><i class="fas fa-arrow-up"></i></a>
        </div>
    </div>

</footer>

<!-- Footer -->


<!--#######################################################################
            Footer section desktop
  ######################################################################## -->


<footer style="float: right;width: 100%" class="page-footer font-small cyan darken-4  pt-5 sm-device-display-none " id="footer">

    <!-- Footer Elements -->
    <div class="container ">


        <!--Grid row-->
        <div class="row">

            <!--Grid column-->
            <div class="col-md-5 mb-4 text-right ml-5">

                <!-- social icon -->
                <h5 class="h5-responsive ">
                    درباره ما:
                </h5>
                <p class="text-justify ml-5">
                    ما در دیجی یار معتقدیم که دانش و تخصص بدون تجربه و تعهد کارآمد نیست. از این رو متعهدیم، با به
                    کار
                    گیری دانش و تجربه شبکه متخصصین خود مشکلات نرم افزاری و سخت افزاری شما را در سریع ترین زمان ممکن
                    حل
                    کنیم و پاسخگوی درخواست های شما، به
                    صورت
                    حضوری و غیر حضوری باشیم.

                </p>


                <!-- Form -->

            </div>
            <!--Grid column-->
            <!--Grid column-->


            <div class="col-md-5 mb-4 text-right">
                <h5 class="h5-responsive">
                    عضویت در خبرنامه:
                </h5>
                <h6 class="h6-responsive text-right">
                    با عضویت در خبر نامه دیجی یار از تخفیف های ما با خبر شوید.
                </h6>
                <form class="input-group">
                    <input v-model="email" type="text" class="form-control form-control-sm"
                           placeholder="آدرس ایمیل خود را وارد کنید..." aria-label="Your email"
                           aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-outline-white my-0" type="button" v-on:click="saveemail">عضویت
                        </button>
                    </div>
                </form>
                <hr>
                <h5 class="h5-responsive">
                    تماس با ما:
                </h5>
                <p>
                </p>
                <p>
                    {{--پشتیبانی 1: 1312 2842 021--}}
                    {{--</br>--}}
                    پشتیبانی: 09010181932
                </p>
                <div class="social-icon mt-2">
                    <!--Instagram-->
                    <a href="https://www.instagram.com/digiyar.co/">
                        <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <i class="fab fa-telegram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    <i class="fas fa-map-marker-alt fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>

                </div>

            </div>


            <!--Grid column-->

        </div>
        <hr>
        <!--Grid row-->
        <!-- Footer Elements -->
        <!-- Copyright -->

        <div class="row container-fluid">
            <div class="col-md-10 text-center grey-text">
                <img src="{{asset('index/images/logo/grayscale.png')}}" alt="" WIDTH=160 HEIGHT=30 align="middle"
                     class="mb-2">
                <p class="p-Responsive pt-1 ">
                    کلیه حقوق این وبسایت متعلق به گروه دیجی یار است.
                </p>
            </div>
            <div class="">


                <div class="smooth-scroll text-left  ">
                    <a class="btn-floating btn-lg btn-elegant z-depth-0" href="#header"><i
                                class="fas fa-arrow-up"></i></a>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- Footer -->


<!-- Large modal -->
<form class="modal multi-step animated fadeIn" id="qiuck-req-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row mt-4">
                    <h4 class="modal-title step-1 h4-responsive" data-step="1">
                        انتخاب سرویس
                    </h4>
                    <h4 class="modal-title step-2 h4-responsive " data-step="2">
                        اطلاعات تماس
                    </h4>
                    <h4 class="modal-title step-3 h4-responsive " data-step="3">
                        ثبت نهایی
                    </h4>
                </div>
                <div class="container">
                    <div class="m-progress">
                        <div class="m-progress-bar-wrapper">
                            <div class="m-progress-bar">
                            </div>
                        </div>
                        <div class="m-progress-stats">
                <span class="m-progress-current">
                </span>
                            /
                            <span class="m-progress-total">
                </span>
                        </div>
                        <div class="m-progress-complete">
                            Completed
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body step-1 text-right" data-step="1">

                <p>
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                    چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
                    نیاز، و کاربردهای متنوع با هدف بهبود
                    کرد،
                    در
                    این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و
                    زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا
                    مورد استفاده قرار گیرد.
                </p>

            </div>
            <div class="modal-body step-2 text-right" data-step="2">
                <p>
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                    چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
                    نیاز، و کاربردهای متنوع با هدف بهبود
                    کرد،
                    در
                    این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و
                    زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا
                    مورد استفاده قرار گیرد.
                </p>
            </div>
            <div class="modal-body step-3 text-right" data-step="3">
                <p>
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                    چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
                    نیاز، و کاربردهای متنوع با هدف بهبود
                    کرد،
                    در
                    این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و
                    زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا
                    مورد استفاده قرار گیرد.
                </p>
                <button class=" btn  btn-cyan  request-btn mr-5 text-center disabled">
                    ثبت درخواست
                </button>


            </div>


            <div class="modal-footer">
                <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button><-->
                <button type="button" class="btn btn-primary step step-2" data-step="2"
                        onclick="sendEvent('#demo-modal-3', 1)">مرحله قبل
                </button>
                <button type="button" class="btn btn-primary step step-1" data-step="1"
                        onclick="sendEvent('#demo-modal-3', 2)">مرحله بعد
                </button>
                <button type="button" class="btn btn-primary step step-3" data-step="3"
                        onclick="sendEvent('#demo-modal-3', 2)">مرحله قبل
                </button>
                <button type="button" class="btn btn-primary step step-2" data-step="2"
                        onclick="sendEvent('#demo-modal-3', 3)"> مرحله بعد
                </button>
            </div>
        </div>
    </div>
</form>

</div>


</body>
</html>


<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/mdb.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/jquery.mCustomScrollbar.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/jquery.mousewheel.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/city.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/jquery-asPieProgress.js')}}"></script>

<!-- Your custom scripts (optional) -->
{{--<script type="text/javascript" src="{{asset('/index/js/custom_js/multi-step-modal.js')}}"></script>--}}
<script src="{{asset('/slidertouch/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('/slidertouch/dist/jquery.mousewheel.min.js')}}"></script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5f60cece4704467e89ef2782/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->

<script>

    sendEvent = function (sel, step) {
        var sel_event = new CustomEvent('next.m.' + step, {
            detail: {
                step: step
            }
        });
        window.dispatchEvent(sel_event);
    };


    $(document).ready(() => {
        var widthwindow = $(window).width();
        if (widthwindow <= 585) {
            $('.selectpicker').selectpicker('mobile');
            setTimeout(function () {

                $('body iframe').eq(0).css({
                    "height": "25px",
                    "min-height": "25px",
                    "max-height": "25px",
                    "width": "25px",
                    "min-width": "25px",
                    "max-width": "25px",
                    "bottom": "16px",
                    "right": "20px",
                });

                $('body iframe ').eq(0).contents().find('#tawkchat-minified-box.round').css({
                    "height": "25px",
                    "width": "25px",
                    "left": "0",
                    "position": "absolute"
                });
                $('body iframe ').eq(0).contents().find('.round #tawkchat-minified-wrapper').css({
                    "height": "25px",
                    "width": "25px",
                    "bottom": "0",
                });

                $('body iframe ').eq(0).contents().find('#tawkchat-status-text-container').css({
                    "height": "25px",
                    "width": "25px",
                    "left": "0",
                    "position": "absolute"
                });
                $('body iframe ').eq(0).contents().find('#tawkchat-status-text-container svg').css({
                    "height": "17px",
                    "width": "17px",
                    "margin-top": "4px",
                });

                $('body iframe ').eq(0).contents().find('#short-message,#maximizeChat,#minimizeChatMinifiedBtn').css({
                    "font-size": "14px",
                    "line-height": "40px",

                });

                $('body iframe ').eq(0).contents().find('.round #tawkchat-status-agent-container #agent-profile-image').css({
                    "height": "25px",
                    "width": "25px",
                    "background-size": "25px",
                });

                $('body iframe ').eq(0).contents().find('.theme-background-color').css({
                    "background-color": "#757575",
                });

                $('body iframe ').eq(0).contents().find('.round #tawkchat-minified-wrapper').css({
                    "border-radius": "0",
                });


                $('body iframe ').eq(2).css({
                    "height": "18px",
                    "min-height": "18px",
                    "max-height": "18px",
                    "width": "18px",
                    "min-width": "18px",
                    "max-width": "18px",
                    "bottom": "20px",
                    "right": "40px"
                });

                $('body iframe ').eq(2).contents().find('#tawkchat-chat-indicator-text').css({
                    "font-size": "11px",
                    "line-height": "18px",
                });


            }, 3000);

        } else {
            $('body iframe ').eq(0).contents().find('#greetingsText').css({
                "font-family": "yekan",
            });

            $('body iframe ').eq(0).contents().find('.messageWrapper .message').css({
                "font-family": "yekan",
            });
        }

        $(document).scroll(function () {
            if (widthwindow <= 585) {
                setTimeout(function () {
                    $('body iframe').eq(0).css({
                        "height": "25px",
                        "min-height": "25px",
                        "max-height": "25px",
                        "width": "25px",
                        "min-width": "25px",
                        "max-width": "25px",
                        "bottom": "16px",
                        "right": "20px",
                    });
                }, 110)


            }


        });


        $('#ostan').selectpicker('refresh');
        $(document.body).on('click', '.card[data-clickable=true]', (e) => {
            var href = $(e.currentTarget).data('href');
            window.location = href;
        });
    });


    var owl = $('.owl-carousel');
    owl.owlCarousel({
        rtl: true,
        loop: false,
        nav: true,
        navText: ["<img src='{{asset('/images/panels/right-arrow.png')}}'>", "<img src='{{asset('/images/panels/left-arrow.png')}}'>"],
        margin: 10,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 2
            },
            680: {
                items: 3
            },
            960: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });

</script>

