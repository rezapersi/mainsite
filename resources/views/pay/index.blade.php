<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>پرداخت</title>
    @include('source.source')
</head>

<style>
    body {
        background-color: white;
    }



    .h4 {
        font-family: yekan;
        line-height: 50px;
        color: #666;
        text-align: right;
        padding-right: 12px;
        margin-bottom: 0;
        position: relative;
    }

    .ulselectpay {
        width: 100%;
        height: auto;
        float: right;
        margin-top: 10px;

    }

    .ulselectpay li {
        width: 100%;
        height: 45px;
        float: right;
        margin-top: 10px;
        border: 1px solid #eee;
        border-radius: 3px;
        cursor: pointer;

    }

    .ulselectpay li p {
        float: right;
        text-align: right;
        padding: 10px;
        color: #6e6b6b;

    }

    .ulselectpay li.active p {

        color: green;

    }

    .ulselectpay li span {
        width: 25px;
        height: 25px;
        float: left;
        border: 1px solid #ccc;
        border-radius: 100px;
        margin-left: 10px;
        margin-top: 10px;
        position: relative;
    }

    .ulselectpay li span.active {

        background-color: green;
        border: none;
    }

    .ulselectpay li span.active::before {
        content: "";
        width: 7px;
        height: 7px;
        border-radius: 100px;
        background-color: white;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        margin: auto;

    }

    .ulselectpay li.active {
        border: 1px solid green;
    }

    .logo {
        width: 130px;
        height: 30px;
        float: left;
        border-radius: 3px;
        position: absolute;
        top:5px;
        left: 2px;
    }

    .divpart {

        width: 100%;
        float: right;
        min-height: 40px;
        border-bottom: 1px solid #eee;
        margin-bottom: 5px;
    }

    .divpart li {
        width: auto;
        margin-right: 10px;
        margin-left: 10px;
        float: right;
        min-height: 40px;
        line-height: 35px;
        margin-top: 3px;
    }

    .divpart li label {
        float: right;
        color: #3c3b3b;
        text-align: right;
    }

    .divpart li p {
        float: right;
        text-align: right;
        color: #6c6c6c;
        margin-right: 4px;

    }

</style>

<body>


<div id="app">
    <div class="container-fluid">

        <div class="container" style="width: 100%;min-height: 500px;margin-top: 60px">


            @if(\Session::exists('errorpay'))
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">خطا !</h4>
                    <p>  {{\Session::get('errorpay')}}</p>
                </div>
            @else

                @if($detailsbuy!=='')

                    @if($detailsbuy->statuspay_id==2)

                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">خطا !</h4>
                            <p>پرداخت برای این درخواست قبلا انجام شده است.</p>
                        </div>


                    @else
                        <form method="post" action="{{route('sendrequestpay')}}">
                            @csrf
                            <div style="margin-bottom: 15px" class="buyvip col-lg-12 col-md-12 co-sm-12 co-12">

                                <!-- Card -->
                                <div style="" class="card card-cascade wider reverse">

                                    <!-- Card content -->
                                    <div style="padding: 0px;box-shadow: 0 0 0 0 !important;border: 1px solid #eee"
                                         class="card-body card-body-cascade text-center">


                                        <h4 style="font-family: yekan;font-size: 17pt;color:#d75f5f ;" class="card-title h4">
                                            <strong style="border-bottom: 1px dashed #d75f5f">صورت حساب</strong>
                                            <img class="logo" src="{{asset('/images/logo.png')}}">
                                        </h4>



                                        <div class="divpart">

                                            <ul>
                                                <li class="">
                                                    <label>تاریخ فاکتور:</label>
                                                    <p>{{$detailsbuy->requestt->datefactor}}</p>
                                                </li>

                                                <li class="">
                                                    <label>شماره فاکتور:</label>
                                                    <p>{{$detailsbuy->id}}</p>
                                                </li>
                                                <li class="">
                                                    <label> کد پیگیری درخواست:</label>
                                                    <p style="font-family: tahoma">{{$detailsbuy->requestt->trackingcode}}</p>
                                                </li>

                                            </ul>

                                        </div>

                                        <div class="divpart">
                                            <ul>
                                                <li class="">
                                                    <label> نام نام خانوادگی:</label>
                                                    <p>{{$detailsbuy->requestt->namecustomers}} {{$detailsbuy->requestt->familycustomers}}</p>

                                                </li>
                                                <!--
                                                <li class="">
                                                    <label>شماره تماس:</label>
                                                    <p>{{$detailsbuy->requestt->phonenumber}}</p>
                                                </li>
                                                -->
                                            </ul>

                                        </div>


                                        <div class="divpart">
                                            <ul>
                                                <li style="width: 100%;margin-bottom: 0;">
                                                    <label style="width: 100%"> عنوان درخواست:</label>
                                                    <p style="float:right;width: 100%">{{$detailsbuy->titlerequest}}</p>
                                                </li>


                                                <li class="">
                                                    <label> مبلغ تعرفه :</label>
                                                    <p>{{number_format($detailsbuy->pricepay)}} تومان</p>
                                                </li>
                                                <li class="">
                                                    <label>  هزینه ایاب و ذهاب :</label>
                                                    @if($detailsbuy->costcommuting==0)

                                                        <p style="color: red">رایگان</p>
                                                        @else
                                                        <p>{{number_format($detailsbuy->costcommuting)}} تومان</p>
                                                        @endif

                                                </li>
                                                <li class="">
                                                    <label>  هزینه درخواست :</label>

                                                    @if($detailsbuy->costrequest==0)
                                                        <p style="color: red">رایگان</p>
                                                        @else
                                                        <p>{{number_format($detailsbuy->costrequest)}} تومان</p>
                                                        @endif


                                                </li>

                                                <?php
                                                    $pricekol=$detailsbuy->pricepay+$detailsbuy->costcommuting+$detailsbuy->costrequest
                                                 ?>
                                                <li class="">
                                                    <label> جمع کل :</label>
                                                    <p style="color: green">{{number_format($pricekol)}} تومان</p>
                                                </li>

                                            </ul>

                                        </div>


                                       {{-- <div>

                                            <ul class="ulselectpay">

                                                <li data-id="1" class="active">
                                                   <span class="active selectpay">
                                                   </span>
                                                    <p>درگاه پرداخت زرین پال</p>
                                                </li>


                                                <li data-id="2">
                                                   <span class="selectpay">
                                                   </span>
                                                    <p>درگاه پرداخت آی دی پی</p>
                                                </li>

                                            </ul>

                                        </div>--}}

                                        <div class="acseptrules">
                                            <div style="display: block;min-height: 50px;float: right;width: 100%;margin-top: 5px" class="pay">
                                                <input type="hidden" name="pinsearch" value="{{$detailsbuy->pinsearch}}">
                                                <input type="hidden" class="typebank" name="typebank" value="1"> {{--default zarinpal--}}
                                                <div style="width: 100%" class="">
                                                    <button style="float: right;width: 100%;text-align: center;border-radius: 3px;border: none;font-size: 14pt" class="btn btn-success"> پرداخت
                                                    </button>
                                                </div>

                                            </div>


                                        </div>

                                    </div>
                                    <!-- Card -->


                                </div>


                            </div>
                        </form>
                    @endif



                @else
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">خطا !</h4>
                        <p>درخواست مورد نظر یافت نشد.</p>

                    </div>
                @endif



            @endif


        </div>

    </div>
</div>
@include('source.footerfix')

<script>
    $('.ulselectpay li').click(function () {
        $('.ulselectpay li').removeClass('active');
        $('.ulselectpay li span').removeClass('active');
        $(this).addClass('active');
        $('span', this).addClass('active');
        var type = $(this).attr('data-id');
        $('.typebank').val(type);
    })


</script>