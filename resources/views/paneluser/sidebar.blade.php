<div class="sidebar-header">
    <span onclick="openclosemenue()" class="iconmenue iconpanel"></span>
    <div class="partavatar">
        <img src="{{asset('/images/panels/user/useravatar.png')}}">
        <h4>{{Auth::user()->name}} {{Auth::user()->family}}</h4>
        <p style="color: #01ed3c !important;font-size: 10pt !important;font-weight: bold;">موجودی  {{number_format(\Auth::user()->bagmoney)}}</p>
    </div>

</div>
<div class="sidebar-body">

    <ul class="ullevel1">

        <a href="/panel/insertrequest">
            <li class="limenulevel1 @if($namepage=='insertrequest') {{'active'}} @endif">
                <span class="iconinsertrequest iconpanel @if($namepage=='insertrequest') {{'active'}} @endif"></span>
                <p>ثبت درخواست</p>
            </li>
        </a>
        <a href="/panel/dashboard">
            <li class="limenulevel1 @if($namepage=='dashboarduser') {{'active'}} @endif">
                <span class="icondashboard iconpanel @if($namepage=='dashboarduser') {{'active'}} @endif"></span>
                <p>داشبورد</p>
            </li>
        </a>


        <a href="/panel/accountuser">
            <li class="limenulevel1 @if($namepage=='accountuser-panel') {{'active'}} @endif">
                <span class="iconaccountuser iconpanel @if($namepage=='accountuser-panel') {{'active'}} @endif"></span>
                <p>حساب کاربری</p>
            </li>
        </a>


        <a href="/panel/facture">
            <li class="limenulevel1 @if($namepage=='facture') {{'active'}} @endif">
                <span class="iconbill iconpanel @if($namepage=='facture') {{'active'}} @endif"></span>
                <p>صورت حساب</p>
            </li>
        </a>


        <a href="/panel/managerequests">
            <li class="limenulevel1 @if($namepage=='managerequests-panel') {{'active'}} @endif">
                <span class="iconmanagerequests iconpanel @if($namepage=='managerequests-panel') {{'active'}} @endif"></span>
                <p> درخواست ها</p>
            </li>
        </a>

        <a href="/panel/bagmoney">
           
            <li class="limenulevel1 @if($namepage=='bagmoney') {{'active'}} @endif">
                <span class="iconmoneymanagement iconpanel @if($namepage=='bagmoney') {{'active'}} @endif"></span>
                <p>کیف پول و درخواست تسویه</p>
            </li>
        </a>


        <!-- <a href="/panel/support"> -->
            {{--<li class="limenulevel1 @if($namepage=='support') {{'active'}} @endif">--}}
                {{--<span class="iconsupport iconpanel @if($namepage=='support') {{'active'}} @endif"></span>--}}
                {{--<p>پشتیبانی</p>--}}
            {{--</li>--}}
        <!-- </a> -->

    </ul>


</div>


