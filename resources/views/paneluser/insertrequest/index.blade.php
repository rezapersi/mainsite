@extends('paneluser.master')
@section('content')

    <section class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12"
             style="position: relative;right: 0;left: 0;margin: auto;float: none">


        <div class="divinsertrequeststeps">

            <div class="insertrequeststepslarge col-xl-2 col-lg-3 col-md-12">
                <div>
                    <ul class="ulstep">
                        <li dataid="1" class="active">جزئیات سفارش</li>
                        <li dataid="2">آدرس پستی</li>
                        <li dataid="3">مشخصات فردی</li>
                        <li dataid="4">ثبت نهایی</li>
                    </ul>

                </div>


            </div>


            <div class="insertrequestbody col-xl-10 col-lg-9 col-md-12">
                <section style="display: block" class="section1">

                    <div class="section1_chosedostan col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <h4 style="width: 100%"> انتخاب استان :</h4>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <select id="ostan" name="ostan" onchange="selectostan(this);"
                                    class="selectpicker"
                                    data-width="100%">
                                <option data-val="41" value="آذربايجان شرقي">آذربايجان شرقي</option>
                                <option data-val="44" value="آذربايجان غربي">آذربايجان غربي</option>
                                <option data-val="45" value="اردبيل">اردبيل</option>
                                <option data-val="31" value="اصفهان">اصفهان</option>
                                <option data-val="26" value="البرز">البرز</option>
                                <option data-val="84" value="ايلام">ايلام</option>
                                <option data-val="77" value="بوشهر">بوشهر</option>
                                <option data-val="21" value="تهران">تهران</option>
                                <option data-val="38" value="چهار محال بختياري">چهار محال بختياري</option>
                                <option data-val="56" value="خراسان جنوبي">خراسان جنوبي</option>
                                <option data-val="51" value="خراسان رضوي">خراسان رضوي</option>
                                <option data-val="58" value="خراسان شمالي">خراسان شمالي</option>
                                <option data-val="61" value="خوزستان">خوزستان</option>
                                <option data-val="24" value="زنجان">زنجان</option>
                                <option data-val="23" value="سمنان">سمنان</option>
                                <option data-val="54" value="سيستان بلوچستان">سيستان بلوچستان</option>
                                <option data-val="71" value="فارس">فارس</option>
                                <option data-val="28" value="قزوين">قزوين</option>
                                <option data-val="25" value="قم">قم</option>
                                <option data-val="87" value="کردستان">کردستان</option>
                                <option data-val="34" value="کرمان">کرمان</option>
                                <option data-val="83" value="کرمانشاه">کرمانشاه</option>
                                <option data-val="74" value="کهکيلويه وبوير احمد">کهکيلويه وبوير احمد</option>
                                <option data-val="17" value="گلستان">گلستان</option>
                                <option data-val="13" value="گيلان">گيلان</option>
                                <option data-val="66" value="لرستان">لرستان</option>
                                <option data-val="15" value="مازندران">مازندران</option>
                                <option data-val="86" value="مرکزي">مرکزي</option>
                                <option data-val="76" value="هرمزگان">هرمزگان</option>
                                <option data-val="81" value="همدان">همدان</option>
                                <option data-val="35" value="يزد">يزد</option>
                            </select>


                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
        <span class="shahr">
            <select id="shahr" name="city" data-live-search="true"
                    class="selectpicker" data-width="100%"></select>
            </span>
                        </div>


                    </div>
                    <div class="section1_chosedmaincat">
                        <div class="section1_chosedmaincat_right col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">

                            <h4 style="font-size: 18pt">انتخاب سرویس</h4>

                            <img src="{{asset('/images/panels/servicesicon.png')}}">
                        </div>

                        <div class="section1_chosedmaincat_left col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">

                            <div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="scrolling">

                                <div class="owl-carousel">

                                    @foreach($maincat as  $row)
                                        <div dataid="{{$row->id}}" v-on:click="selectmaincat({{$row->id}})"
                                             class="items">
                                            <img src="{{\App\Setting::first()->root}}img/khadamat/{{$row->img}}">
                                            <p>{{$row->title}}</p>
                                        </div>
                                    @endforeach

                                </div>


                            </div>


                        </div>

                    </div>
                    <div class="section1_stepsdown">


                        {{-- start --}}
                        <div style="display: block" class="section1_stepsdown_step1">
                            <h4>سرویس مورد نظر خود را انتخاب کنید</h4>

                            <div class="search">
                                <input v-model="searchservice" class="form-control" placeholder="جستجوی سرویس">
                            </div>

                            <ul class="ulselectservice">

                                <li v-for="get in allservice.data" :dataid="get.id" v-on:click="selectproblem(get.id)"
                                    class="col-lg-3 col-md-4 col-sm-6 col-6">
                                    <div class="waves-effect waves-light">
                                        <img src="{{asset('/images/panels/addicon.png')}}">

                                        <p class="tx-green">@{{get.title}} </p>
                                    </div>
                                <li>


                            </ul>


                            {{-- paginate --}}

                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                <div class="divpaginate">
                                    <pagination :data="allservice" v-on:pagination-change-page="getservice">
                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                    </pagination>
                                </div>
                            </div>


                            <div id="nextstep1" style="opacity:0" class="nextitem">
<span v-on:click="nextstep(1,'frompaneluser')" class="bg-primery waves-effect waves-light">

    مرحله بعد
    <i class="fas fa-chevron-left"></i>

</span>
                            </div>
                        </div>


                        {{-- end --}}


                        <div class="section1_stepsdown_step2">

                            <h4>برند دستگاه</h4>


                            <div class="search">
                                <input v-model="searchberand" class="form-control" placeholder="جستجوی برند">
                            </div>


                            <ul class="ulselectberand">

                                <li v-for="get in allberand.data" :dataid="get.id" v-on:click="selectberand(get.id)"
                                    class="col-lg-3 col-md-4 col-sm-6 col-6">
                                    <div class="waves-effect waves-light">
                                        <img src="{{asset('/images/panels/addicon.png')}}">
                                        <p class="tx-green">@{{get.title}}</p>
                                    </div>
                                <li>

                            </ul>

                            {{-- paginate --}}

                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                <div class="divpaginate">
                                    <pagination :data="allberand" v-on:pagination-change-page="getberand">
                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                    </pagination>
                                </div>
                            </div>


                            <div id="nextstep2" style="opacity:0" class="nextitem">
            <span v-on:click="nextstep(2,'frompaneluser')" class="bg-primery waves-effect waves-light">

                مرحله بعد
                <i class="fas fa-chevron-left"></i>
            
            </span>
                            </div>

                        </div>


                        <div class="section1_stepsdown_step3">

                            <h4>مدل دستگاه</h4>

                            <div class="search">
                                <input v-model="searchmodel" class="form-control" placeholder="جستجوی مدل">
                            </div>


                            <ul class="ulselectmodel">

                                <li v-for="get in allmodel.data" :dataid="get.id" v-on:click="selectmodel(get.id)"
                                    class="col-lg-3 col-md-4 col-sm-6 col-6">
                                    <div class="waves-effect waves-light">
                                        <img src="{{asset('/images/panels/addicon.png')}}">
                                        <p class="tx-green">@{{get.title}}</p>
                                    </div>


                            </ul>


                            {{-- paginate --}}

                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                <div class="divpaginate">
                                    <pagination :data="allmodel" v-on:pagination-change-page="getmodel">
                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                    </pagination>
                                </div>
                            </div>


                            <div id="nextstep3" style="opacity:0" class="nextitem">
            <span v-on:click="nextstep(3,'frompaneluser')" class="bg-primery waves-effect waves-light">

                مرحله بعد
                <i class="fas fa-chevron-left"></i>
            
            </span>
                            </div>


                        </div>


                        <div class="section1_stepsdown_step4">

                            <h4>ثبت توضیحات</h4>

                            <p class="tx-red">به منظور ارائه خدمات بهتر از سوی کارشناسان ما لطفا توضیحاتی که لازم است
                                بدانیم را در ذیل وارد نمایید.</p>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <textarea v-model="descriptionrequest"
                                          placeholder="مثال:دستگاه خاموش است و روشن نمی شود."
                                          class="col-lg-12 col-md-12 form-control"></textarea>
                            </div>


                            <div class="nextitem">
        <span v-on:click="nextstep(4,'frompaneluser')" class="bg-primery waves-effect waves-light">

            مرحله بعد
            <i class="fas fa-chevron-left"></i>
        
        </span>
                            </div>


                        </div>


                    </div>
                </section>
                {{-- end section 1 --}}


                {{-- section 2 --}}

                {{-- address posti --}}
                <section style="display: none" class="section2">
                    <div class="chosedaddress">
                        <h4>انتخاب آدرس</h4>
                        <ul class="ulselectaddress">
                            <li>
                                <span dataid='0' v-on:click="activeaddress" class="active"></span>
                                <p>آدرس جدید</p>
                            </li>
                            @foreach($addresuser as $row)
                                <li>
                                    <span dataid='{{$row->id}}' v-on:click="activeaddress"></span>
                                    <p> استان {{$row->ostan}} شهر {{$row->city}} ({{$row->address}}) پلاک {{$row->plak}} / کد پستی{{$row->codeposti}}</p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="insertnewaddres">

                        <div style="box-shadow:none" class="part">

                            <div class="part-left col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">


                                <div class="part-body">
                                    <ul>


                                        <li style="margin-top: 0" id="liplak" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div style="min-height: 65px;">
                                                {{--<label>پلاک</label>--}}
                                                <input placeholder="پلاک" type="number" min="0" v-model="plak" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>


                                        <li style="margin-top: 0" id="licodeposti" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div style="min-height: 65px;">
                                                {{--<label></label>--}}
                                                <input placeholder="کد پستی" v-model="codeposti" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>


                                        <li style="margin-top: 0" id="liaddress" class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                            <div style="min-height: 65px;">
                                                {{--<label> آدرس کامل </label>--}}
                                                <input placeholder=" آدرس کامل" v-model="fulladdress" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>

                                    </ul>


                                </div>


                            </div>


                        </div>

                    

                    </div>

                   

          <div class="nextitem">
        <span v-on:click="nextstep(5,'frompaneluser')" class="bg-primery waves-effect waves-light">
            مرحله بعد
            <i class="fas fa-chevron-left"></i>
        </span>
         </div>

                </section>

                {{-- end section 2 --}}
                {{-- section 3 --}}

                <section  class="section3">
                    <div style="float: right;min-height: 300px;width: 100%;background-color: white">
                        <div style="box-shadow:none;" class="part">
                            <div class="part-left col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">


                                <div style="margin-top: 30px" class="part-body">
                                    <ul>

                                        <li  class="linameuser col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div>
                                                <label>نام</label>
                                                <input  v-model="nameuser" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>


                                        <li  class="lifamilyuser col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div>
                                                <label> نام خانوادگی</label>
                                                <input v-model="familyuser" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>


                                        <li  class="limobileuser col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div>
                                                <label> موبایل </label>
                                                <input v-model="mobileuser" class="form-control">
                                                <p class="text-error"></p>
                                            </div>
                                        </li>

                                    </ul>


                                </div>


                            </div>


                        </div>


          <div class="nextitem">
        <span v-on:click="nextstep(6,'frompaneluser')" class="bg-primery waves-effect waves-light">
         ثبت نهایی
         <i class="fas fa-chevron-left"></i>
        </span>
         </div>

                    </div>


                </section>

                {{-- end section 3 --}}



                {{--  section 4 --}}


                <section class="section4">

                    <div class="finalinsertrequest">

                        <p class="tx-green">درخواست شما با موفقیت ثبت شد</p>

                        <h4> کارشناسان ما به زودی با شما تماس خواهند گرفت</h4>
                        <h4 class="trackingcode"></h4>
                        <p>پشتیبانی : {{App\Setting::first()->phonecontact}}</p>
                    </div>

                </section>
                {{-- end section 4 --}}


            </div>

        </div>


    </section>



@endsection