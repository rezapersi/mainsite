<div class="headerpanel">

    <ul>

        <li>
            <div class="logoheaderpanel">
                <a href="/">
                <img src="{{asset('/images/panels/logo.png')}}">
                </a>
            </div>
        </li>


        <li style="float: left;margin-right: 10px;">
            <div class="divuser_headerpanel">
                <img src="{{asset('/images/panels/user/useravatar.png')}}">
                <i onclick="showtopbarmenue(event)" class="fas fa-angle-down"></i>

                <div class="topbarmenue">

                    <ul>

                        <a href="/panel/accountuser">
                            <li class="tx-gray">
                                پروفایل
                            </li>
                        </a>


                        <li data-toggle="modal" data-target="#modalchangepassword" class="tx-gray">
                            تغییر کلمه عبور
                        </li>

                        <li data-toggle="modal" data-target="#modalchargebagmoney" class="tx-gray">
                            افزایش اعتبار
                        </li>


                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();   document.getElementById('logout-form').submit();">
                            <li class="tx-gray">
                                خروج
                            </li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>


                        </a>

                    </ul>


                </div>


            </div>

        </li>


        <li style="float: left">
            <div style="position: relative;margin-right: 10px;margin-left: 5px" class="diviconheaderpanel">
                {{--<span>2</span>--}}
                <i style="background-position: -46px -85px" class="iconpanel"></i>
            </div>

        </li>
        <li style="float: left">
            <div style="position: relative" class="diviconheaderpanel">
                {{--<span>2</span>--}}
                <i style="background-position: -9px -81px" class="iconpanel"></i>

            </div>

        </li>

        {{--  <li style="float: left">

              <div class="diviconheaderpanel">
                  <span>2</span>
                  <i data-toggle="dropdown"  aria-haspopup="true" aria-expanded="true" class="dropdown-toggle far fa-bell"></i>

                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                  </div>


              </div>


          </li>--}}


    </ul>


</div>