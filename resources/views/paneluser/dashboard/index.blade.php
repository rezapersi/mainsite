@extends('paneluser.master')
@section('content')

    <section style="right: 0;left: 0;margin: auto;float: none;" class="col-xl-10 col-lg-810 col-md-12 col-sm-12 col-12">


        <div class="divsearchdashboard">
            <h4>چه کمکی از دست ما برمیاد؟</h4>
            <div class="col-xl-10 col-lg-10 col-md-11 col-sm-12 col-12">
                <input class="form-control" placeholder="جستجو بین خدمات">
                <i class="tx-primery fas fa-search"></i>
            </div>
        </div>


        <ul class="uldashboard">

            <li  class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                <div class="bg-primery">
                    <h4 class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">درخواست های فعال</h4>



                 
                    @if(sizeof($activerequest)>0)
                        <ul >
                    
                        @foreach($activerequest as $row)
                
                        <li>
                            <p> {{$row->maincat->title}} / {{$row->bernaddevice->title}} /{{$row->modeldevice->title}}</p>
                           <a href="/panel/managerequests">
                           <span >جزئیات بیشتر> </span>
                           </a>
                          
                        </li>

                        @endforeach
                      
                    </ul>


                    @else

                        <div>
                        <p class="tx-red" style="width: 100%;">درحال حاضر درخواست فعالی وجود ندارد</p>

                    </div>

                    @endif
                  

               
                    <span> 
                        <a style="width:100%;height:100%" href="/panel/insertrequest">
                        ثبت درخواست جدید

                        </a> 
                        
                        </span>
                   

                  

                </div>
            </li>

            <li class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                <div class="bg-yellow">
                    <h4 class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">کیف پول</h4>

                    <div>
                        <p>موجودی</p>
                        <span>{{number_format(\Auth::user()->bagmoney)}} تومان</span>
                    </div>

                    <span data-toggle="modal" data-target="#modalchargebagmoney">افزایش موجودی</span>

                </div>
            </li>


        </ul>

    </section>




@endsection