@extends('paneluser.master')
@section('content')

    <section class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        @if(\Session::exists('erroruploadimg'))
            <p class="alert alert-danger">{{\Session::get('erroruploadimg')}}</p>
        @endif

        @if(\Session::exists('successuploadimg'))
            <p class="alert alert-success">{{\Session::get('successuploadimg')}}</p>
        @endif


        <div class="part">


            <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                <i class="fas fa-ellipsis-h"></i>
            </div>
            <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                <div class="part-header">
                    <h4>اطلاعات کاربری</h4>
                </div>

                <div class="part-body">
                    <ul>

                        <li id="liemailedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>آدرس ایمیل</label>
                                <input v-model="emailuser" style="font-family: Tahoma;text-align: left"
                                       placeholder="test@yahoo.com" class="form-control">

                                <p class="text-error"></p>
                            </div>

                        </li>

                        <li id="limobileedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> شماره همراه</label>
                                <input type="number" min="0" v-model="mobileuser" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>


                    </ul>


                    <div class="divbuttom">

                        <button v-on:click="updatedetailsuser('informationkarbari')" class="buttommaked">ثبت
                            تغییرات
                        </button>

                    </div>


                </div>


            </div>
        </div>
        <div class="part">

            <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                <i class="fas fa-ellipsis-h"></i>
            </div>

            <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                <div class="part-header">
                    <h4>اطلاعات شخصی </h4>
                </div>

                <div class="part-body">
                    <ul>

                        <li id="lienameedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> نام</label>
                                <input v-model="nameuser" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>

                        <li id="liefamilyedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> نام خانوادگی</label>
                                <input v-model="familyuser" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>

                        <li id="linationalcodeedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> کد ملی</label>
                                <input v-model="nationalcodeuser" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>


                        <li id="liphoneedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>شماره ثابت</label>
                                <input type="number" min="0" v-model="phoneuser" class="form-control">
                                <p class="text-error"></p>
                            </div>
                        </li>

                    </ul>


                    <div class="divbuttom">

                        <button v-on:click="updatedetailsuser('informationpersonal')" class="buttommaked">ثبت تغییرات
                        </button>

                    </div>


                </div>


            </div>


        </div>

        <div class="part">

            <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                <i class="fas fa-ellipsis-h"></i>
            </div>

            <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                <div class="part-header">
                    <h4>افزودن آدرس</h4>
                </div>

                <div class="part-body">
                    <ul>

                        <li id="liostanaddress" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>استان</label>
                                <select id="ostan" name="ostan" onchange="selectostan(this);"
                                        class="selectpicker"
                                        data-width="100%">
                                    <option data-val="41" value="آذربايجان شرقي">آذربايجان شرقي</option>
                                    <option data-val="44" value="آذربايجان غربي">آذربايجان غربي</option>
                                    <option data-val="45" value="اردبيل">اردبيل</option>
                                    <option data-val="31" value="اصفهان">اصفهان</option>
                                    <option data-val="26" value="البرز">البرز</option>
                                    <option data-val="84" value="ايلام">ايلام</option>
                                    <option data-val="77" value="بوشهر">بوشهر</option>
                                    <option data-val="21" value="تهران">تهران</option>
                                    <option data-val="38" value="چهار محال بختياري">چهار محال بختياري</option>
                                    <option data-val="56" value="خراسان جنوبي">خراسان جنوبي</option>
                                    <option data-val="51" value="خراسان رضوي">خراسان رضوي</option>
                                    <option data-val="58" value="خراسان شمالي">خراسان شمالي</option>
                                    <option data-val="61" value="خوزستان">خوزستان</option>
                                    <option data-val="24" value="زنجان">زنجان</option>
                                    <option data-val="23" value="سمنان">سمنان</option>
                                    <option data-val="54" value="سيستان بلوچستان">سيستان بلوچستان</option>
                                    <option data-val="71" value="فارس">فارس</option>
                                    <option data-val="28" value="قزوين">قزوين</option>
                                    <option data-val="25" value="قم">قم</option>
                                    <option data-val="87" value="کردستان">کردستان</option>
                                    <option data-val="34" value="کرمان">کرمان</option>
                                    <option data-val="83" value="کرمانشاه">کرمانشاه</option>
                                    <option data-val="74" value="کهکيلويه وبوير احمد">کهکيلويه وبوير احمد</option>
                                    <option data-val="17" value="گلستان">گلستان</option>
                                    <option data-val="13" value="گيلان">گيلان</option>
                                    <option data-val="66" value="لرستان">لرستان</option>
                                    <option data-val="15" value="مازندران">مازندران</option>
                                    <option data-val="86" value="مرکزي">مرکزي</option>
                                    <option data-val="76" value="هرمزگان">هرمزگان</option>
                                    <option data-val="81" value="همدان">همدان</option>
                                    <option data-val="35" value="يزد">يزد</option>
                                </select>

                                <p class="text-error"></p>
                            </div>
                        </li>


                        <li id="licityaddress" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>شهر</label>
                                <span class="shahr">
                            <select id="shahr" name="city" data-live-search="true"
                                    class="selectpicker" data-width="100%"></select>
                            </span>

                                <p class="text-error"></p>

                            </div>
                        </li>


                        <li id="liplak" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>پلاک</label>
                                <input type="number" min="0" v-model="plak" class="form-control">
                                <p class="text-error"></p>
                            </div>
                        </li>


                        <li id="licodeposti" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> کد پستی</label>
                                <input v-model="codeposti" class="form-control">
                                <p class="text-error"></p>
                            </div>
                        </li>


                        <li id="liaddress" class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <div>
                                <label> آدرس کامل </label>
                                <input v-model="fulladdress" class="form-control">
                                <p class="text-error"></p>
                            </div>
                        </li>

                    </ul>


                    <div class="divbuttom">

                        <button v-on:click="insertaddress" class="buttommaked">افزودن آدرس
                        </button>

                    </div>


                </div>


            </div>


        </div>


        <div style="float: right;margin-top: 30px;display: block"
             class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>

                    <th scope="col">استان وشهر</th>
                    <th scope="col"> کد پستی</th>
                    <th scope="col">پلاک</th>
                    <th scope="col">آدرس کامل</th>
                    <th scope="col">حذف</th>


                </tr>
                </thead>
                <tbody>

                <tr v-for="get in alladdress">
                    <td>@{{get.ostan +'-'+ get.city}}</td>
                    <td>@{{get.codeposti}}</td>
                    <td>@{{get.plak}}</td>
                    <td>@{{get.address}}</td>
                    <td>
                        <i v-on:click="removeaddress(get.id)" class="tx-red fas fa-trash-alt"></i>
                    </td>
                </tr>

                </tbody>
            </table>

        </div>
        <div v-if="withwindow<630" v-for="get in alladdress"
             class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">


                <li>
                    <div class="flatright">
                        استان و شهر
                    </div>
                    <div class="tx-primery flatleft">
                        @{{get.ostan +'-'+ get.city}}
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        کد پستی
                    </div>
                    <div class="tx-primery flatleft">
                        @{{get.codeposti}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        پلاک
                    </div>
                    <div class="tx-primery flatleft">
                        @{{get.plak}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        آدرس
                    </div>

                    <div class="tx-primery flatleft">
                        @{{get.address}}
                    </div>

                </li>
                <li>
                    <div class="flatright">
                        حذف آدرس
                    </div>

                    <div class="tx-green flatleft">
                        <i v-on:click="removeaddress(get.id)" class="tx-red fas fa-trash-alt"></i>
                    </div>

                </li>


            </ul>
        </div>


    </section>








@endsection