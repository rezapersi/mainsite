@extends('paneluser.master')
@section('content')

    <section style="position: relative;right: 0;left: 0;margin: auto;float: none;display: flow-root;"
             class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
        <div style="float: right;margin-top: 30px"
             class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>
                    <th scope="col">شناسه درخواست</th>
                    <th scope="col">تاریخ ثبت</th>
                    <th scope="col">نام و مدل دستگاه</th>
                    <th scope="col">توضیحات</th>
                    <th scope="col">کارشناس تعمیر</th>
                    <th scope="col">هزینه درخواست</th>
                    <th scope="col">وضعیت</th>
                    <th scope="col">گزارش</th>
                </tr>

                </thead>
                <tbody>

                <tr v-for="get in allrequestcustomernewdata">

                    <td style="color: orangered;font-family: Tahoma">@{{get.trackingcode}}</td>
                    <td style="direction: ltr">@{{get.date}}</td>
                    <td>@{{get.maincat.title}} @{{get.bernaddevice.title}}  @{{get.modeldevice.title}}</td>
                    <td style="cursor: pointer" v-on:click="getdetailsrequest(get.id)" data-toggle="modal"
                        data-target="#modelshowproblem" class="maincolor">
                        مشاهده
                    </td>


                    <td>@{{get.namekarshenas}}</td>


                    <td v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3">---</td>
                    <td v-if="get.statusrequest_id==4 || get.statusrequest_id==5">@{{Number(get.pricefacture).toLocaleString()}}
                        تومان
                    </td>

                    <td v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3"
                        class="tx-red">@{{get.statusrequest.title}}</td>
                    <td v-if="get.statusrequest_id==4" class="tx-red">@{{get.statusrequest.title}}</td>
                    <td v-if="get.statusrequest_id==5" class="tx-green">@{{get.statusrequest.title}}</td>


                    <td style="cursor: pointer" v-on:click="getdetailsrequest(get.id);getreportsrequestcustomer()"
                        data-toggle="modal" data-target="#modelreportrequest" class="maincolor">
                        مشاهده
                    </td>

                </tr>
                </tbody>
            </table>

        </div>
        <div v-if="withwindow<630" v-for="get in allrequestcustomernewdata"
             class="flat smallscreen col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                <li>
                    <div class="flatright">
                        شناسه درخواست
                    </div>
                    <div style="color: orangered;font-family: Tahoma" class="flatleft">
                        @{{get.trackingcode}}
                    </div>

                </li>

                <li>
                    <div class="flatright">
                        تاریخ ثبت
                    </div>
                    <div style="direction: ltr" class="flatleft">
                        @{{get.date}}
                    </div>

                </li>


                <li>
                    <div class="flatright">
                        نام و مدل دستگاه
                    </div>
                    <div class="flatleft">
                        @{{get.maincat.title}} @{{get.bernaddevice.title}}  @{{get.modeldevice.title}}
                    </div>
                </li>

                <li>
                    <div class="flatright">
                         توضیحات
                    </div>
                    <div class="flatleft">
                        <i v-on:click="getdetailsrequest(get.id)" data-toggle="modal" data-target="#modelshowproblem"
                           class=" fas fa-chevron-left"></i>
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        کارشناس تعمیر
                    </div>
                    <div class="flatleft">
                        @{{get.namekarshenas}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        هزینه درخواست
                    </div>


                    <div v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3"
                         class="flatleft">
                        ---
                    </div>

                    <div v-if="get.statusrequest_id==4 || get.statusrequest_id==5" class="flatleft">
                        @{{Number(get.pricefacture).toLocaleString()}} تومان
                    </div>

                </li>


                <li>
                    <div class="flatright">
                        وضعیت
                    </div>

                    <div v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3"
                         class="tx-red flatleft">
                        @{{get.statusrequest.title}}
                    </div>
                    <div v-if="get.statusrequest_id==4" class="tx-red flatleft">
                        @{{get.statusrequest.title}}
                    </div>

                    <div v-if="get.statusrequest_id==5" class="tx-green flatleft">
                        @{{get.statusrequest.title}}
                    </div>

                </li>


                <li>
                    <div class="flatright">
                        گزارش
                    </div>

                    <div class="flatleft">
                        <i v-on:click="getdetailsrequest(get.id);getreportsrequestcustomer()" data-toggle="modal"
                           data-target="#modelreportrequest" class="fas fa-chevron-left"></i>
                    </div>


                </li>


            </ul>

        </div>

        <pagination :data="allrequestcustomer" v-on:pagination-change-page="getrequestcustomer">
            <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
            <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
        </pagination>

    </section>


    <div class="modal fade" id="modelshowproblem" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog modal-xl modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 v-for="get in detailsrequest" style="text-align: right;font-family: Tahoma;font-size: 14pt;"
                        class="tx-primery modal-title w-100" id="myModalLabel">@{{get.trackingcode}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div v-for="get in detailsrequest"
                     style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body">


                    <div style="background-color:#fbfbfb;border-radius: 5px;min-height: 50px;margin-bottom: 10px;"
                         class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <p class="tx-primery" style="padding: 10px;margin-top: 5px">@{{get.description}}</p>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <div style="height: 100%" class="modal fade" id="modelreportrequest" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog modal-xl modal-lg" role="document">
            <div style="height: 100%;background-color: white" class="modal-content">

                <div class="modal-header">
                    <h4 v-for="get in detailsrequest" style="text-align: right;font-family: Tahoma;font-size: 14pt;"
                        class="tx-primery modal-title w-100" id="myModalLabel">@{{get.trackingcode}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div v-for="get in detailsrequest"
                     style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body ">

                    <div v-if="withwindow>630" class="table-responsive text-nowrap">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">تاریخ ثبت</th>
                                <th scope="col">گزارش</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="report in allreportrequest.data">
                                <td style="direction: ltr">@{{report.date}}</td>
                                <td>@{{report.report}}</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div v-if="withwindow<630" v-for="report in allreportrequest.data"
                         class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                            <li>
                                <div class="flatright">
                                    تاریخ ثبت
                                </div>
                                <div style="color: orangered;direction: ltr" class="flatleft">
                                    @{{report.date}}
                                </div>

                            </li>

                            <li>
                                <div class="flatright">
                                    گزارش
                                </div>
                                <div class="flatleft">
                                    @{{report.report}}
                                </div>
                            </li>


                        </ul>

                    </div>
                    <pagination :data="allreportrequest" v-on:pagination-change-page="getreportsrequestcustomer">
                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                    </pagination>


                </div>


            </div>
        </div>
    </div>


@endsection
