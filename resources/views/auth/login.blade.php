<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ورود</title>

    @include('source.source')
    <link href="{{asset('/css/loginpanel.css')}}" rel="stylesheet">
</head>

<body>

{{--@include('source.headerfix')--}}

<div style="float: right" class="container-fluid">
    {{--@include('source.headerfix')--}}

    <div id="register">
        <div class="main">

         {{--   <div class="imgheaderformlogin col-xl-3 col-lg-3 col-md-4 col-sm-7 col-12">
                <a href="/">
                    <img src="{{asset('/images/logo.png')}}">
                </a>
            </div>--}}


            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div style="margin-top: 60px" class="formsend contactus">

                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-7 col-12">


                        <div style="width: 100%;min-height: 70px;" class="imgheaderformlogin">
                            <a href="/">
                                <img src="{{asset('/images/logo.png')}}">
                            </a>
                        </div>



                        <div class=" col-lg-12 col-md-12">
                            <label>شماره موبایل </label>
                            <input style="font-family: Tahoma" name="mobile" value="{{ old('mobile') }}" class="form-control  ">

                            @if ($errors->has('mobile'))
                                <p>{{ $errors->first('mobile') }} </p>
                            @endif

                        </div>


                        <div  class=" col-lg-12 col-md-12">
                            <label> رمز عبور </label>
                            <input  type="password" name="password" class="form-control  ">
                            @if ($errors->has('password'))
                                <p>{{ $errors->first('password') }} </p>
                            @endif
                        </div>



                        <div style="min-height: 45px;" class="col-lg-12">
                            <button style="width: 100%;height: 45px;text-align: center;border-radius: 3px;border: none;"
                                    type="submit" class="btn aqua-gradient">ورود
                            </button>
                        </div>

                        <a href="/passwordrecovery">
                            <p style="text-align: center;margin-bottom: 0"  class="font-small gotolink  col-lg-12 col-md-12 col-sm-12 col-12">بازیابی رمز عبور</p>
                        </a>

                        <a  href="/register">
                            <p style="text-align: center;margin-bottom: 0;font-weight: bold;color: #01c2ed !important;" style="text-align: center"
                               class="font-small gotolink  col-lg-12 col-md-12 col-sm-12 col-12">هنوز ثبت نام نکرده ام</p>
                        </a>


                    </div>


                </div>
            </form>


        </div>
    </div>


</div>


<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/mdb.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.js')}}"></script>

{{--@include('source.footerfix')--}}
