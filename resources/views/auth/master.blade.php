<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <title>ورود به پنل  {{\App\Setting::first()->namesait}} </title>--}}
    @include('source.source')

</head>

<style>
    body {
        background: linear-gradient(#8219b3, #da80ff);
        height: 900px;
        position: relative;
        z-index: 1;
        background-position: bottom center;
        background-size: cover;
    }

</style>


<body>

{{--@include('source.headerfix')--}}

<div class="container-fluid">
    @yield('content')
</div>

<div class="browsernotsupport">
    <img src="{{asset('/img/logo/mehr_red.png')}}">
    <h4>نام سایت</h4>
    <p>لطفا از مرورگر موزیلا یا کروم استفاده نمایید</p>

</div>

{{--
@include('source.footerfix')--}}
