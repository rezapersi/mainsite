<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>جزئیات پرداخت</title>
    @include('source.source')
    <link href="{{asset('/css/detailspay.css')}}" rel="stylesheet">
</head>
<style>
    body {
        background-color: white;
    }

</style>
<body>

<div id="app"></div>
<div class="container-fluid">

    <div class="detailspay">

        @if(Session::exists('errorpay'))
            <div style="margin-top: 10px" class="alert alert-danger" role="alert">
                <h4 class="alert-heading">{{Session::get('errorpay')}}</h4>

            </div>

            {{--<img style="width: 100%;float: right" src="{{asset('/img/error.png')}}">--}}

        @endif

        @if(Session::exists('errorsuccess'))
            <div style="margin-top: 10px" class="alert alert-success" role="alert">
                <h4 class="alert-heading">{{Session::get('errorsuccess')}}</h4>

            </div>

        @endif


        @if($getdetailspay !=='')

            <div style="margin-top: 40px" class="col-sm-12  table-responsive">

                <div class="alert alert-secondary" role="alert">
                    جزئیات پرداخت
                </div>


                <table id="dtBasicExample" class="table table-striped  dataTable table-bordered" role="grid"
                       aria-describedby="dtBasicExample_info" style="width: 100%;background-color: white"
                       width="100%" cellspacing="0">

                    <thead>
                    <tr role="row">

                        <th class="th-sm sorting" tabindex="0" aria-controls="dtBasicExample" rowspan="1"
                            colspan="1"
                            style="width: 135px;" aria-label="Name

          : activate to sort column ascending">کد پیگیری

                        </th>
                        <th class="th-sm sorting_asc" tabindex="0" aria-controls="dtBasicExample" rowspan="1"
                            colspan="1"
                            style="width: 207px;" aria-label="Position

          : activate to sort column descending" aria-sort="ascending"> تاریخ پرداخت
                        </th>


                        <th class="th-sm sorting" tabindex="0" aria-controls="dtBasicExample" rowspan="1"
                            colspan="1"
                            style="width: 98px;" aria-label="Office

          : activate to sort column ascending"> مبلغ پرداختی


                        </th>
                        {{-- <th class="th-sm sorting" tabindex="0" aria-controls="dtBasicExample" rowspan="1"
                             colspan="1"
                             style="width: 98px;" aria-label="Office

       : activate to sort column ascending"> پرداخت از طریق


                         </th>--}}
                        <th class="th-sm sorting" tabindex="0" aria-controls="dtBasicExample" rowspan="1"
                            colspan="1"
                            style="width: 75px;" aria-label="Age

          : activate to sort column ascending"> وضعیت پرداخت

                        </th>


                    </tr>
                    </thead>
                    <tbody>

                    <tr role="row" class="odd">
                        <td style="color: red;font-family: Tahoma"
                            class="">{{$getdetailspay->numberorder_showcustomer}}</td>
                        <td style="color:orangered;direction: ltr" class="sorting_1">{{$getdetailspay->datepay}}</td>
                        <td style="color: green" class="">{{number_format($getdetailspay->pricepay)}} تومان</td>
                        <td style="color: green">{{$getdetailspay->statuspay->title}}</td>

                    </tr>

                    </tbody>

                </table>
            </div>

            <style>

            </style>

        @endif

        <div id="redirecttohome">
            <div id="counter">15</div>

            <?php
            $leveluser=Auth::user()->leveluser_id;
            if($leveluser==7){
                $url='/panel/facture';
            }elseif($leveluser==5){
                $url='/panelkarshenasan/facture';
            }else{
                $url='/index';
            }

            ?>

            <a style="text-align: center" id="buttomredirecttohome" href="{{$url}}" class="btn btn-success">
                بازگشت به پنل
            </a>
        </div>


    </div>
</div>


@include('source.footerfix')


