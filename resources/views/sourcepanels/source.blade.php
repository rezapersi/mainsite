<meta name="theme-color" content="#2c93bd">
<style>
@font-face {
    font-family: 'yekan';
   src:url({{ asset('/font/Yekan.ttf')}}) format('truetype'),
        url({{asset('/font/yekan.eot?#iefix')}}) format('embedded-opentype'),
        url({{asset('/font/Yekan.woff')}}) format('woff'); 
   
}

@font-face {
    font-family: 'irsans';
   src: url({{ asset('/font/irsans.ttf')}}) format('truetype');
    
}

.iconpanel {
    background: url({{ asset('/images/panels/iconspanel.png') }}) no-repeat;

}

</style>
<link href="{{asset('/css/app.css')}}" rel="stylesheet">
<link href="{{asset('/css/mdb.min.css')}}" rel="stylesheet">
<link href="{{asset('/css/compiled-4.6.0.css')}}" rel="stylesheet">
<link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet">
<link href="{{asset('/css/bootstrap-select.css')}}" rel="stylesheet">
<link  rel="stylesheet" type='text/css' href="{{asset('/css/stylepanels.css')}}">
<link href="{{asset('/css/jquery.mCustomScrollbar.css')}}" rel="stylesheet">
<link href="{{asset('/css/asPieProgress.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.6/animate.min.css" />

<!--taghvim shamsi-->
<link rel="stylesheet" type="text/css" media="all" href="{{asset('/JalaliJSCalendar-1.4/skins/aqua/theme.css')}}"
      title="Aqua"/>
<link rel="alternate stylesheet" type="text/css" media="all"
      href="{{asset('/JalaliJSCalendar-1.4/skins/calendar-blue2.css')}}" title="blue"/>

<style>
 .btn-primary:hover{

        background-color: #4285f4 !important;
    }

    .btn-secondary:hover{

        background-color: #a6c !important;
    }

    .btn-danger:hover{

        background-color: #ff3547 !important;
    }


    .btn-success:hover{

        background-color: #00c851  !important;
    }

    .btn-ins:hover{

        background-color: #2e5e86   !important;
    }

    .btn-tw:hover{

        background-color: #55acee    !important;
    }

    .btn-li:hover{

        background-color: #0082ca     !important;
    }


    .btn-grey:hover {
        background-color: #616161 !important;
    }</style>