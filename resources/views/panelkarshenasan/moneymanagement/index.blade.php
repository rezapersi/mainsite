@extends('panelkarshenasan.master')
@section('content')

    <section style="position: relative;right: 0;left: 0;margin: auto;float: none"
             class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">

        <ul class="ulbagmoney">
            <li class="paddingnone col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/creditcard.png')}}">

                    <input placeholder="شماره شبا خود را وارد نمایید">

                    <div class="bg-red bagmoneyborderdown">

                      <span class="tx-gray">
تسویه حساب
                      </span>


                    </div>


                </div>


            </li>

            <li class="paddingnone col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/Moneywallet.png')}}">
                    <h4 class="tx-gray">موجودی کیف پول</h4>

                   <p class="tx-green">250,000</p>

                    <div class="bg-primery bagmoneyborderdown">

                      <span style="border: 1px solid #01c2ed" class="tx-gray">
تسویه حساب
                      </span>


                    </div>


                </div>


            </li>


        </ul>


        <div style="float: right;margin-top: 30px;display: block"
             class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>
                    <th scope="col">ردیف</th>
                    <th scope="col"> تاریخ</th>
                    <th scope="col"> شناسه تراکنش</th>
                    <th scope="col">عنوان تراکنش</th>
                    <th scope="col">مبلغ</th>
                    <th scope="col">موجودی</th>
                    <th scope="col">جزئیات بیشتر</th>
                </tr>

                </thead>
                <tbody>
                <tr {{--v-for="get in allincomes.data"--}}>

                    <td style="color: orangered;font-family: Tahoma">1</td>
                    <td>1398/11/10 13:00:00</td>
                    <td style="font-family: Tahoma">DYR-123451</td>
                    <td>تسویه حساب</td>
                    <td class="tx-red">1,200,000</td>
                    <td class="tx-green">0,0</td>
                    <td class="maincolor">
                        بیشتر
                    </td>

                </tr>
                <tr {{--v-for="get in allincomes.data"--}}>

                    <td style="color: orangered;font-family: Tahoma">1</td>
                    <td style="direction: ltr">1398/11/10 13:00:00</td>
                    <td style="font-family: Tahoma">DYR-123451</td>
                    <td>افزایش اعتبار</td>
                    <td class="tx-red">1,200,000</td>
                    <td class="tx-green">50,000</td>
                    <td class="maincolor">
                        بیشتر
                    </td>

                </tr>
                <tr {{--v-for="get in allincomes.data"--}}>

                    <td style="color: orangered;font-family: Tahoma">1</td>
                    <td style="direction: ltr">1398/11/10 13:00:00</td>
                    <td style="font-family: Tahoma">DYR-123451</td>
                    <td> هزینه درخواست</td>
                    <td class="tx-red">1,200,000</td>
                    <td class="tx-green">50,000</td>
                    <td class="maincolor">
                        بیشتر
                    </td>

                </tr>

                </tbody>
            </table>

        </div>


        <div style="display: block"
             {{--v-if="withwindow<630" v-for="get in allincomes.data"--}} class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">


                <li>
                    <div class="flatright">
                        تاریخ
                    </div>
                    <div style="direction: ltr" class="flatleft">
                        1398/11/10 13:00:00
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        شناسه تراکنش
                    </div>
                    <div style="font-family: Tahoma" class="flatleft">
                        DYR-123451
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        عنوان تراکنش
                    </div>
                    <div  class="flatleft">
                        تسویه حساب
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        مبلغ
                    </div>
                    <div  class="tx-red flatleft">
                        1,200,000
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        موجودی
                    </div>
                    <div  class="tx-green flatleft">
                        0,0
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        جزئیات بیشتر
                    </div>
                    <div class="flatleft">
                        <i {{--v-on:click="getdetailsincome(get.id)"--}} data-toggle="modal"
                           data-target="#modaldescription"
                           class="maincolor fas fa-chevron-left"></i>
                    </div>
                </li>


            </ul>

        </div>


    </section>


@endsection