<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:locale" content="fa_IR"/>
    <meta property="og:type" content="website"/>
    <title>پنل کارشناسان</title>
    @include('sourcepanels.source')

</head>
<body>
<style>
    .showdatenow {

        width: auto;
        height: 60px;
        float: left;
        padding: 20px;
    }

    .showdatenow p {

        color: #195e47;
        direction: ltr;
    }

    .darkwindow {
        width: 100%;
        height: 100%;
        position: fixed;
        background-color: rgba(0, 0, 0, 0.3);
        display: none;
        z-index: 3;
    }
</style>
<div class="darkwindow"></div>
<div id="app">

    <div id="largeloading" class="loading">

        <div>

            <h4>لطفا چند
                لحظه صبر نمایید ...</h4>
            <span>
            <img src="{{asset('/loading/three-dots.svg')}}">
            </span>
        </div>

    </div>

    <div style="position: relative;float: right" class="container-fluid">
        <input type="hidden" id="page" value="{{$namepage}}">

        <div class="sidebarpanel">
            @include('panelkarshenasan.sidebar')
        </div>

        <div id="divcontent" class="div-content col-lg-12 col-md-12 col-sm-12 col-12">

            <div>
                @include('panelkarshenasan.headerpanel')
                <div class="div-content2" style="min-height: 500px">
                    @yield('content')

                    <div class="modal fade" id="modalchargebagmoney" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4  style="width: 100%;text-align: right;font-size: 13pt;color: #666" class="h4headremodal">افزایش اعتبار</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">

                                    <p class="alert alert-warning">حداقل مبلغ افزایش شارژ {{number_format(5000)}} تومان می باشد.</p>

                                    <form id="formincreasecredit" method="post" action="{{route('increasecredit')}}">
                                        @csrf
                                        <ul class="ulinsert">
                                            <li class="lipricebagmoney col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div>
                                                    <label>مبلغ(تومان) </label>
                                                    <input name="price" type="number" min="4999"  class="pricebagmoney form-control">
                                                    <p class="text-error"></p>

                                                </div>
                                            </li>

                                            <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div>

                                                    <button v-on:click.prevent="checkpricechargebagmoney"  style="width: 60px;float: right;margin-top: 45px;" type="submit"  class="buttommaked">
                                                        پرداخت
                                                    </button>
                                                </div>
                                            </li>

                                        </ul>


                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalchangepassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header text-center">
                                    <h4  class="tx-primery modal-title w-100 font-weight-bold">تغییر پسورد</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body mx-3">

                                    <div id="cheangepassword_password" class="md-form mb-5">
                                        <i style="left: -10px" class="fas fa-lock prefix grey-text"></i>
                                        <input v-model="newpasswordforchangepassword" name="password" style="font-family: Tahoma;text-align: left" type="text" id="defaultForm-email" class="form-control">
                                        <label  style="text-align: right" data-error="wrong" data-success="right" for="defaultForm-email"> پسورد جدید</label>

                                        <p style="display: none" class="text-formerror" ></p>
                                    </div>

                                    <div id="changepassword_confirmpassword" class="md-form mb-5">
                                        <i  style="left: -10px" class="fas fa-key prefix grey-text"></i>
                                        <input v-model="repeatpasswordforchangepassword" style="font-family: Tahoma;text-align: left" type="text" id="defaultForm-pass" class="form-control" name="password_confirmation">
                                        <label style="text-align: right" data-error="wrong" data-success="right" for="defaultForm-pass">تکرار پسورد </label>


                                    </div>


                                    <p style="display: none" id="allertsuccesschangepassword" class="alert alert-success"></p>


                                </div>

                                <div class="modal-footer d-flex justify-content-center">

                                    <button v-on:click="changepassworduser()" style="width: 100%;float: right;margin-top: 0;height: 40px" type="submit"  class="buttommaked">
                                        تغییر پسورد
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modelshowmadarek" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true">

                        <div class="modal-dialog modal-xl modal-lg" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4 style="text-align: right;color:#b43232;font-family: Tahoma;font-size: 14pt;" class="modal-title w-100" id="myModalLabel">مدارک</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body">

                                    <div style="margin-bottom: 30px;min-height: 250px" class="divinsertrequest">

                                        @if(\Auth::user()->statusconfirmmadarek==0)
                                          <p class="alert alert-danger">مدارک شما هنوز ارسال نشده است.</p>
                                       @endif
                                        @if(\Auth::user()->statusconfirmmadarek==1)
                                          <p class="alert alert-primery">مدارک ارسالی شما در انتظار تایید توسط تیم پشتیبانی می باشد.</p>
                                       @endif

                                      @if(\Auth::user()->statusconfirmmadarek==3)
                                          <p class="alert alert-danger">مدارک ارسالی شما مورد تایید قرار نگرفت لطفا مجددا مدارک را ارسال نمایید.</p>
                                       @endif

                                        @if(\Auth::user()->statusconfirmmadarek==2)


                                                <ul class="ulshowmadarek">
                                                    <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                        <div>
                                                            <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgnationalcode}}">
                                                            <h4 class="tx-gray">کارت ملی</h4>
                                                        </div>
                                                    </li>

                                                    <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                        <div>
                                                            <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgmaharat}}">
                                                            <h4 class="tx-gray">گواهی مهارت</h4>
                                                        </div>
                                                    </li>

                                                    <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                        <div>
                                                            <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgjavazkasb}}">
                                                            <h4 class="tx-gray">جواز کسب</h4>
                                                        </div>
                                                    </li>


                                                </ul>


                                            @endif



                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                </div>

            </div>

        </div>
    </div>

</div>


@include('sourcepanels.footerfix')

<script type="text/javascript">

    function openclosemenue() {
        var typemenue = $('.iconmenue').attr('typemenue');
        var width = $(document).width();
        if (typemenue == 'largeside') {

            $('.iconmenue').attr('typemenue', 'smallside');
            $('.partavatar').hide();
            $('.sidebar-body .limenulevel1 > p').hide();

            $('.sidebar-header').animate({height: '60px'});
            $('.sidebarpanel').animate({width: '65px'});
          //  $('.div-content').animate({'padding-right': '65px'});
            // $('.div-content').css('padding-right', '65px');

        } else {

            $('.iconmenue').attr('typemenue', 'largeside');
            $('.partavatar').show();
            $('.sidebar-body .limenulevel1 > p').show();
            $('.sidebar-header').animate({height: '180px'});
            $('.sidebarpanel').css('width', '200px');
           // $('.div-content').css('padding-right', '200px');

        }


        if (width >= 610) {

            if (typemenue == 'largeside') {

                  $('.div-content').animate({'padding-right': '65px'});

            } else {

                 $('.div-content').css('padding-right', '200px');

            }

        }else{

            if (typemenue == 'largeside') {
                $('.darkwindow').fadeOut(200);
                $('.div-content').animate({'padding-right': '65px'});
            } else {
                $('.darkwindow').fadeIn(300);
                $('.div-content').css('padding-right', '0');

            }



        }






    }


 /*   $(document).ready(function () {
        var width = $(document).width();
        if (width >= 600) {
            $('.logoheaderpanel').css('height', '25px');
            $('.logoheaderpanel').css('width', '145px');

            $('.iconmenue').attr('typemenue', 'largeside');
            $('.iconmenue').removeClass('smallside');
            $('.partavatar').show();
            $('.sidebar-body .limenulevel1 > p').show();
            $('.sidebar-header').css('height', '180px');
            $('.sidebarpanel').css('width', '200px');
            $('.div-content').css('padding-right', '200px');

        } else {

            $('.logoheaderpanel').css('height', '17px');
            $('.logoheaderpanel').css('width', '90px');

            $('.iconmenue').attr('typemenue', 'smallside');
            $('.iconmenue').removeClass('largeside');

            $('.partavatar').hide();
            $('.sidebar-body .limenulevel1 > p').hide();
            $('.sidebar-header').css('height', '62px');
            $('.sidebarpanel').css('width', '65px');
            $('.div-content').css('padding-right', '65px');

        }
    });*/


    /*function showname() {
     var file = document.getElementById('fileInput');
     var namefile = file.files.item(0).name;
     if (namefile) {
     $('.lableinputfileinsertticket').html(namefile);
     } else {
     $('.lableinputfileinsertticket').html('انتخاب فایل');
     }
     };
     function shownamefileanswer() {
     var file = document.getElementById('fileInputanswer');
     var namefile = file.files.item(0).name;
     if (namefile) {
     $('.lableinputfileinsertanswer').html(namefile);
     } else {
     $('.lableinputfileinsertanswer').html('انتخاب فایل');
     }
     };*/


    function uploadimglogo(tag, type) {
        var input = $(tag);
        var getsize = tag.files[0].size;


        if (type == 'add') {
            var rootclass = '.formaddlogo';
        }

        if (type == 'edit') {
            var rootclass = '.formeditlogo';
        }


        if (type == 'nationalcart') {
            var rootclass = '.formaddnationalcart';
        }

        if (type == 'maharat') {
            var rootclass = '.formaddmaharat';
        }

        if (type == 'javaz') {
            var rootclass = '.formaddjavaz';
        }


        //format tasvir


        var ext = $('' + rootclass + ' .fileuploadlogo').val().split('.').pop().toLowerCase();

        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {

            $('' + rootclass + ' .showerroruploadimg ').show();
            $('' + rootclass + ' .fileuploadlogo').val('');
            $('' + rootclass + ' .showerroruploadimg p').html('فرمت فایل  انتخابی قابل قبول نیست');

        } else {
            //size tasvir
            if (getsize > 530000) {

                $('' + rootclass + ' .showerroruploadimg ').show();
                $('' + rootclass + ' .fileuploadlogo').val('');
                $('' + rootclass + ' .showerroruploadimg p').html('سایز تصویر باید کمتر از 500 کیلوبایت باشد.');

            } else {

                $('' + rootclass + ' .showimgloading').fadeIn(400);
                //load tasvir
                if (typeof (FileReader) != "undefined") {
                    var image_holder = $('' + rootclass + ' .image-holderlogo');
                    image_holder.empty();
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image"
                        }).appendTo(image_holder);

                    };
                    image_holder.show();
                    reader.readAsDataURL($('' + rootclass + ' .fileuploadlogo')[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }

            }

        }


    }

    function removeimglogo(type) {

        if (type == 'add') {
            var rootclass = '.formaddlogo';
        }

        if (type == 'edit') {
            var rootclass = '.formeditlogo';
        }


        if (type == 'nationalcart') {
            var rootclass = '.formaddnationalcart';
        }

        if (type == 'maharat') {
            var rootclass = '.formaddmaharat';
        }

        if (type == 'javaz') {
            var rootclass = '.formaddjavaz';
        }


        $('' + rootclass + ' .image-holderlogo img').remove();
        $('' + rootclass + ' .showimgloading').fadeOut(300);
        $('' + rootclass + ' .fileuploadlogo').val('');
    }
    function hiderroruploadimglogo(type) {


        if (type == 'add') {
            var rootclass = '.formaddlogo';
        }

        if (type == 'edit') {
            var rootclass = '.formeditlogo';
        }


        if (type == 'nationalcart') {
            var rootclass = '.formaddnationalcart';
        }

        if (type == 'maharat') {
            var rootclass = '.formaddmaharat';
        }

        if (type == 'javaz') {
            var rootclass = '.formaddjavaz';
        }



        $('' + rootclass + ' .showerroruploadimg').fadeOut(300);
    }


    function closefieldrizfacture(tag) {
        var iconclose=$(tag);
        iconclose.parents('li').remove();
        var allli = $('.ulstrizfactore li');
        var pricecol = 0;
        $.each(allli, function (index, title) {
            var price = $(title).find('.inputprice').val();
            price=Number(price);
            pricecol = pricecol+price;
        });
        $('.pricekolrizfacture').html(''+Number(pricecol).toLocaleString()+' تومان');


    }




    function showtopbarmenue(e) {
        $('.topbarmenue').slideDown();
        e.stopPropagation()
    }


    $('.sidebarpanel').click(function (e) {
        e.stopPropagation();
    });


    $(document).click(function () {


        var typemenue = $('.iconmenue').attr('typemenue');
        var width = $(document).width();

        if (width < 610) {
            if (typemenue == 'largeside') {
                $('.iconmenue').attr('typemenue', 'smallside');
                $('.partavatar').hide();
                $('.sidebar-body .limenulevel1 > p').hide();
                $('.sidebar-header').animate({height: '60px'});
                $('.sidebarpanel').animate({width: '65px'});
                $('.darkwindow').fadeOut(200);
                $('.div-content').animate({'padding-right': '65px'});
            }
        }


        $('.topbarmenue').slideUp();






    })




</script>



