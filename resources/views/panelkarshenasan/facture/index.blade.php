@extends('panelkarshenasan.master')
@section('content')
    <section style="position: relative;right: 0;left: 0;margin: auto;float: none" class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">


        @if(Session::exists('errorpay'))
            <p style="margin-top: 10px;margin-bottom: 0" class="alert alert-danger">{{ Session::get('errorpay')}}</p>
        @endif

        @if(Session::exists('successpay'))
            <p style="margin-top: 10px;margin-bottom: 0" class="alert alert-success">{{ Session::get('successpay')}}</p>
        @endif



        <div style="float: right;margin-top: 30px;display: block" class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>

                    <th scope="col">شماره صورت حساب</th>
                    <th scope="col">تاریخ پرداخت</th>
                    <th scope="col">نوع صورت حساب</th>
                    <th scope="col">مبلغ</th>
                    <th scope="col">تخفیف</th>
                    <th scope="col">پرداختی</th>
                    <th scope="col">وضعیت پرداخت</th>
                    <th scope="col">پرداخت</th>

                </tr>
                </thead>
                <tbody>

                <tr v-for="get in allfacturepaneluser.data">

                    <td style="color: orangered;font-family: Tahoma">@{{get.numberorder_showcustomer}}</td>
                    <td style="direction: ltr">@{{get.datepay}}</td>
                    <td>@{{get.typefacture.title}}</td>
                    <td>@{{Number(get.pricefacture).toLocaleString()}} تومان

                        <button v-if="get.typefacture.id==1 || get.typefacture.id==3" v-on:click="getrizfactures(get.id)" data-toggle="modal" data-target="#modelrizfacture" style="width: 50px;font-size: 9pt;height: 25px;float: none;text-align: center;padding: 0;" class="buttommaked bg-red">
                            ریز فاکتور
                        </button>
                    </td>

                    <td> @{{Number(get.discount).toLocaleString()}} تومان</td>
                    <td>@{{Number(get.pricepay).toLocaleString()}} تومان</td>

                    <td>@{{get.statuspay.title}} </td>

                    <td class="tx-green" v-if="get.statuspay.id==2">
                        پرداخت شده
                    </td>
                    <td class="tx-green" v-if="get.statuspay.id==2 && get.typefacture_id ==4">
                        تسویه شده
                    </td>


                    <td v-if="get.statuspay.id==1 && get.typefacture_id !==4">
                        <button v-on:click="getdetailsorder(get.id)" data-toggle="modal" data-target="#modalpay" style="width: 87px;font-size: 9pt;height: 25px;float: none"
                                class="buttommaked bg-green">پرداخت
                        </button>
                    </td>

                    <td v-if=" get.statuspay.id==3 && get.typefacture_id !==4">
                        <button v-on:click="getdetailsorder(get.id)" data-toggle="modal" data-target="#modalpay" style="width: 87px;font-size: 9pt;height: 25px;float: none"
                                class="buttommaked bg-green">پرداخت
                        </button>
                    </td>



                    <td class="tx-red" v-if="get.statuspay.id !==2 && get.typefacture_id ==4">
                        در انتظار تسویه حساب
                    </td>


                </tr>
                </tbody>
            </table>

        </div>
        <div v-if="withwindow<630" v-for="get in allfacturepaneluser.data"
             class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                <li>

                    <div  style="color: orangered" class="flatright">
                        @{{get.numberorder_showcustomer}} | @{{get.statuspay.title}}
                    </div>


                    <div style="font-family: Tahoma" class="flatleft">
                        <i v-on:click="getrizfactures(get.id)" data-toggle="modal" data-target="#modelrizfacture" class=" fas fa-chevron-left"></i>
                    </div>

                </li>

                <li>
                    <div class="flatright">
                        تاریخ پرداخت
                    </div>
                    <div style="direction: ltr" class="flatleft">
                        @{{get.datepay}}
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        مبلغ
                    </div>
                    <div class="flatleft">
                        @{{Number(get.pricefacture).toLocaleString()}} تومان
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        نوع صورت حساب
                    </div>
                    <div class="flatleft">
                        @{{get.typefacture.title}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        تخفیف
                    </div>
                    <div class="flatleft">
                        @{{Number(get.discount).toLocaleString()}} تومان
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        پرداختی
                    </div>

                    <div class="tx-green flatleft">
                        @{{Number(get.pricepay).toLocaleString()}} تومان
                    </div>

                </li>


                <li v-if="get.statuspay.id==1  && get.typefacture_id !==4">
                    <div style="width: 100%" class="flatright">
                        <button v-on:click="getdetailsorder(get.id)" data-toggle="modal" data-target="#modalpay" style="width: 100%;font-size: 11pt;height: 35px;float: none;"
                                class="buttommaked bg-green">
                            پرداخت
                        </button>
                    </div>
                </li>


                                <li v-if=" get.statuspay.id==3 && get.typefacture_id !==4">
                    <div style="width: 100%" class="flatright">
                        <button v-on:click="getdetailsorder(get.id)" data-toggle="modal" data-target="#modalpay" style="width: 100%;font-size: 11pt;height: 35px;float: none;"
                                class="buttommaked bg-green">
                            پرداخت
                        </button>
                    </div>
                </li>



                <li v-if="get.statuspay.id !==2 && get.typefacture_id==4">
                    <div style="width: 100%" class="flatright">
                        <button  style="background-color: #f45;width: 100%;font-size: 11pt;height: 35px;float: none;" class="buttommaked bg-green">
                           در انتظار تسویه حساب
                        </button>
                    </div>

                </li>



            </ul>
        </div>

        <pagination :data="allfacturepaneluser" v-on:pagination-change-page="getallfacturepaneluser">
            <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
            <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
        </pagination>
    </section>


    <div style="height: 100%" class="modal fade" id="modelrizfacture" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div  class="modal-dialog modal-xl modal-lg" role="document">
            <div style="height: 100%;background-color: white" class="modal-content">

                <div class="modal-header">
                    <h4  style="text-align: right;font-family: Tahoma;font-size: 14pt;" class="tx-primery modal-title w-100" id="myModalLabel"> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div  style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body ">

                        <div class="factor_body">

                            <ul class="ulfieldfacture">

                                <li v-for="rizfacture in rizfactures">
                                    <div class="tx-gray col-lg-8 col-md-8 col-sm-8 col-12">@{{rizfacture.babat}}</div>
                                    <div class="tx-primery col-lg-4 col-md-4 col-sm-4 col-12">@{{Number(rizfacture.price).toLocaleString()}} تومان</div>
                                </li>


                            </ul>


                        </div>

                </div>


            </div>
        </div>
    </div>



    <!-- Modal pay -->
    <div class="modal fade" id="modalpay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 v-for="details in detailsorder" style="width: 100%;text-align: right;font-size: 13pt;color: #666" class="h4headremodal">پرداخت صورت حساب (@{{details.numberorder_showcustomer }})</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>

                <div class="modal-body">

                    <p class="alert alert-primary">موجودی کیف پول شما {{number_format(Auth::user()->bagmoney)}} تومان می باشد.</p>
                    <form method="post" action="{{route('payfacture')}}">
                        @csrf
                        <ul style="min-height:130px" class="ulinsert">

                            <li  class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div>
                                    <label>پرداخت از طریق</label>
                                    <select name="typepay"  class="selectortypepay selectpicker"
                                            data-width="100%">
                                        <option value="1">مستقیم (درگاه بانک)</option>
                                        <option value="0"> کیف پول</option>
                                    </select>
                                </div>
                            </li>

                            <li  v-for="details in detailsorder" v-if="details.typefacture.id==1" class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div style="position: relative">
                                    <label>کد تخفیف</label>
                                   <input style="font-family: tahoma" v-model="discountcode" class="form-control">
                                    <span v-on:click="checkdiscountcode" class="bg-red checkdiscountcode">بررسی</span>

                                    <span class="blockdiscountcode">

    </span>

                                </div>

                            </li>
                            <input  v-for="details in detailsorder" type="hidden"  name="idfacture" :value="details.id">
                            <input  class="inputdiscountcode" type="hidden"  name="discountcode">
                            <li class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div style="background-color: #b8fdb8">
                                    <h4 class="priceend"  v-for="details in detailsorder" style="text-align: center;font-size: 14pt;margin-top: 15px;">مبلغ نهایی @{{Number(details.pricefacture).toLocaleString()}} تومان</h4>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button v-on:click="showloading" type="submit" style="width: 100%;text-align: center;margin-top: 15px;margin-right: 0;float: right;border-radius: 3px"
                                                class="btn btn-success">
                                            پرداخت
                                        </button>
                                    </div>

                                </div>
                            </li>


                        </ul>
                    </form>

                </div>

            </div>
        </div>
    </div>



@endsection


