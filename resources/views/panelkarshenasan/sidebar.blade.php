<div class="sidebar-header">
    <span onclick="openclosemenue()" class="iconmenue iconpanel"></span>
    <div class="partavatar">
        <div class="ratekarshenas">
            <i class="fas fa-star"></i>
            <p>{{Auth::user()->rate}}</p>
        </div>

        <img src="{{asset('/images/panels/user/useravatar.png')}}">
        <h4>{{Auth::user()->name}} {{Auth::user()->family}}</h4>

        <?php
        use App\User;
        $user = User::where('id', Auth::id())->with('services')->first();
        ?>

        @foreach($user->services as $row)
            <p>کارشناس {{$row->title}}</p>
        @endforeach
    </div>

</div>

<div class="sidebar-body">

    <ul class="ullevel1">

        <!-- <a href="/panelkarshenasan/dashboard">
            <li class="limenulevel1 @if($namepage=='dashboardkarshenasan') {{'active'}} @endif">
                <span class="icondashboard iconpanel @if($namepage=='dashboardkarshenasan') {{'active'}} @endif"></span>
                <p>داشبورد</p>
            </li>
        </a> -->


        <a href="/panelkarshenasan/accountuser">
            <li class="limenulevel1 @if($namepage=='accountuser') {{'active'}} @endif">
                <span class="iconaccountuser iconpanel @if($namepage=='accountuser') {{'active'}} @endif"></span>
                <p>حساب کاربری</p>
            </li>
        </a>

        @if(Auth::user()->blockpanel==0)

        <a href="/panelkarshenasan/facture">
            <li class="limenulevel1 @if($namepage=='facture') {{'active'}} @endif">
                <span class="iconbill iconpanel @if($namepage=='facture') {{'active'}} @endif"></span>
                <p>صورت حساب</p>
            </li>
        </a>


        <a href="/panelkarshenasan/managerequests">
            <li class="limenulevel1 @if($namepage=='managerequests') {{'active'}} @endif">
                <span class="iconmanagerequests iconpanel @if($namepage=='managerequests') {{'active'}} @endif"></span>
                <p>مدیریت درخواست ها</p>
            </li>
        </a>




        <a href="/panelkarshenasan/bagmoney">
            <li class="limenulevel1 @if($namepage=='bagmoney') {{'active'}} @endif">
                <span class="iconmoneymanagement iconpanel @if($namepage=='bagmoney') {{'active'}} @endif"></span>
                <p>کیف پول و درخواست تسویه</p>
            </li>
        </a>



        {{-- <a href="/panelkarshenasan/partsbank"> --}}
            {{--<li class="limenulevel1 @if($namepage=='partsbank') {{'active'}} @endif">--}}
                {{--<span class="iconpartsbank iconpanel @if($namepage=='partsbank') {{'active'}} @endif"></span>--}}
                {{--<p>بانک قطعات</p>--}}
            {{--</li>--}}
        {{-- </a> --}}

        @endif
        <!-- <a href="/panelkarshenasan/support"> -->
            {{--<li class="limenulevel1 @if($namepage=='support') {{'active'}} @endif">--}}
                {{--<span class="iconsupport iconpanel @if($namepage=='support') {{'active'}} @endif"></span>--}}
                {{--<p>پشتیبانی</p>--}}
            {{--</li>--}}
        <!-- </a> -->

    </ul>


</div>


