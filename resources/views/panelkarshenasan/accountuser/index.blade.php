@extends('panelkarshenasan.master')
@section('content')

    <section class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        @if(\Session::exists('erroruploadimg'))
            <p class="alert alert-danger">{{\Session::get('erroruploadimg')}}</p>
        @endif

        @if(\Session::exists('successuploadimg'))
            <p class="alert alert-success">{{\Session::get('successuploadimg')}}</p>
        @endif


            <div class="part">

                <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                    <i class="fas fa-ellipsis-h"></i>
                </div>

                <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                    <div class="part-header">
                        <h4>اطلاعات شخصی و حقوقی</h4>
                    </div>

                    <div class="part-body">
                        <ul>

                            <li id="lienameedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0"> نام :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->name}}</p>
                                    {{--<input v-model="nameuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>

                            <li id="liefamilyedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0"> نام خانوادگی :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->family}}</p>

                                    {{--<input v-model="familyuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>

                            </li>

                            <li id="linationalcodeedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0"> کد ملی :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->nationalcode}}</p>
                                    {{--<input v-model="nationalcodeuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>


                            <li id="linamecompanyedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0">نام شرکت/محل کسب :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->namecompany}}</p>
                                    {{--<input v-model="namecompanyuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>

                            </li>


                            <li id="liphoneedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0">شماره ثابت :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->phone}}</p>

                                    {{--<input type="number" min="0" v-model="phoneuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>


                            <li id="liostanedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label  style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0">استان :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->ostan}}</p>


                                    {{--<select id="ostan" name="ostan" onchange="selectostan(this);"--}}
                                            {{--class="selectpicker"--}}
                                            {{--data-width="100%">--}}
                                        {{--<option data-val="41" value="آذربايجان شرقي">آذربايجان شرقي</option>--}}
                                        {{--<option data-val="44" value="آذربايجان غربي">آذربايجان غربي</option>--}}
                                        {{--<option data-val="45" value="اردبيل">اردبيل</option>--}}
                                        {{--<option data-val="31" value="اصفهان">اصفهان</option>--}}
                                        {{--<option data-val="26" value="البرز">البرز</option>--}}
                                        {{--<option data-val="84" value="ايلام">ايلام</option>--}}
                                        {{--<option data-val="77" value="بوشهر">بوشهر</option>--}}
                                        {{--<option data-val="21" value="تهران">تهران</option>--}}
                                        {{--<option data-val="38" value="چهار محال بختياري">چهار محال بختياري</option>--}}
                                        {{--<option data-val="56" value="خراسان جنوبي">خراسان جنوبي</option>--}}
                                        {{--<option data-val="51" value="خراسان رضوي">خراسان رضوي</option>--}}
                                        {{--<option data-val="58" value="خراسان شمالي">خراسان شمالي</option>--}}
                                        {{--<option data-val="61" value="خوزستان">خوزستان</option>--}}
                                        {{--<option data-val="24" value="زنجان">زنجان</option>--}}
                                        {{--<option data-val="23" value="سمنان">سمنان</option>--}}
                                        {{--<option data-val="54" value="سيستان بلوچستان">سيستان بلوچستان</option>--}}
                                        {{--<option data-val="71" value="فارس">فارس</option>--}}
                                        {{--<option data-val="28" value="قزوين">قزوين</option>--}}
                                        {{--<option data-val="25" value="قم">قم</option>--}}
                                        {{--<option data-val="87" value="کردستان">کردستان</option>--}}
                                        {{--<option data-val="34" value="کرمان">کرمان</option>--}}
                                        {{--<option data-val="83" value="کرمانشاه">کرمانشاه</option>--}}
                                        {{--<option data-val="74" value="کهکيلويه وبوير احمد">کهکيلويه وبوير احمد</option>--}}
                                        {{--<option data-val="17" value="گلستان">گلستان</option>--}}
                                        {{--<option data-val="13" value="گيلان">گيلان</option>--}}
                                        {{--<option data-val="66" value="لرستان">لرستان</option>--}}
                                        {{--<option data-val="15" value="مازندران">مازندران</option>--}}
                                        {{--<option data-val="86" value="مرکزي">مرکزي</option>--}}
                                        {{--<option data-val="76" value="هرمزگان">هرمزگان</option>--}}
                                        {{--<option data-val="81" value="همدان">همدان</option>--}}
                                        {{--<option data-val="35" value="يزد">يزد</option>--}}
                                    {{--</select>--}}

                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>


                            <li id="licityedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0">شهر :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->city}}</p>


                                    {{--<span class="shahr">--}}
                            {{--<select id="shahr" name="city" data-live-search="true"--}}
                                    {{--class="selectpicker" data-width="100%"></select>--}}
                            {{--</span>--}}

                                    {{--<p class="text-error"></p>--}}

                                </div>
                            </li>


                            <li id="licodepostiedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0"> کد پستی :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->codeposti}}</p>

                                    {{--<input v-model="codepostiuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>

                            <li  id="liaddressedit" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div style="background-color: #fbfbfb;border-radius: 3px;min-height: 35px;margin-bottom: 20px">
                                    <label style="width: auto;float: right;padding: 5px;margin-bottom: 0;padding-left: 0"> آدرس محل :</label>
                                    <p style="float:right;padding: 5px;margin-bottom: 0;font-size: 12pt;">{{$detailskarshenas->address}}</p>

                                    {{--<input v-model="addressuser" class="form-control">--}}
                                    {{--<p class="text-error"></p>--}}
                                </div>
                            </li>

                        </ul>


                        {{--<div class="divbuttom">--}}

                            {{--<button v-on:click="updatedetailsuser('informationpersonal')" class="buttommaked">ثبت تغییرات--}}
                            {{--</button>--}}

                        {{--</div>--}}


                    </div>


                </div>


            </div>



        <div class="part">

            <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                <i class="fas fa-ellipsis-h"></i>
            </div>

            <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                <div class="part-header">
                    <h4>اطلاعات کاربری</h4>
                </div>

                <div class="part-body">
                    <ul>

                        <li id="liemailedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label>آدرس ایمیل</label>
                                <input v-model="emailuser" style="font-family: Tahoma;text-align: left" placeholder="test@yahoo.com" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>

                        <li id="limobileedit" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                                <label> شماره همراه</label>
                                <input type="number" min="0" v-model="mobileuser" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>


                    </ul>


                    <div class="divbuttom">

                        <button v-on:click="updatedetailsuser('informationkarbari')" class="buttommaked">ثبت
                            تغییرات
                        </button>

                    </div>


                </div>


            </div>
        </div>


        <form action="/panelkarshenasan/updateservicekarshenas" method="post" enctype="multipart/form-data">
            @csrf
            <div class="part">
                <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                    <i class="fas fa-ellipsis-h"></i>
                </div>

                <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                    <div class="part-header">
                        <h4>مدیریت تخصص</h4>
                    </div>

                    <div class="part-body">
                        <ul>

                            <li id="lichosedkarshenas" class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                <div>
                                    <label>تخصص </label>

                                    <?php
                                    use App\User;
                                    $user = User::where('id', Auth::id())->with('services')->first();
                                    ?>

                                    <select id="selectorchosedkarshenas" name="servicekarshenas[]" class="selectpicker"
                                            data-width="100%" multiple>

                                        @foreach($service as $row)
                                            <option @foreach($user->services as $service) @if($service->id==$row->id) selected="selected"
                                                    @endif  @endforeach       value="{{$row->id}}">{{$row->title}}</option>
                                        @endforeach
                                    </select>


                                    <p class="text-error"></p>
                                </div>

                            </li>


                        </ul>


                        <div class="divbuttom">

                            <button type="submit" v-on:click="showloading" class="buttommaked">ثبت
                                تغییرات
                            </button>

                        </div>


                    </div>


                </div>


            </div>
        </form>




        <div class="part">


            <div class="part-right col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2">
                <i class="fas fa-ellipsis-h"></i>
            </div>


            <div class="part-left col-xl-11 col-lg-11 col-md-11 col-sm-10 col-10">

                <div class="part-header">
                    <h4>آپلود مدارک</h4>
                </div>


                <?php
                $statusconfirmmadarek = Auth::user()->statusconfirmmadarek;
                ?>



                @if($statusconfirmmadarek==3)
                    <p class="alert alert-danger">مدارک شما تایید نگردید.لطفا مجددا مدارک را ارسال نمایید.</p>
                @endif

                @if($statusconfirmmadarek==0 or $statusconfirmmadarek==3)
                    <form method="post" action="{{route('updatemadarekkarshenas')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="part-body">
                            <ul>
                                <li class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div>
                                        <div style="background-color: unset;padding: 0" class="formaddnationalcart">
                                            <ul style="width: 100%">
                                                <li style="width: 100%">
                                                    <div>
                                                        {{--  <label>تصویر(چک و ..)</label>--}}
                                                        <div style="margin-bottom: 10px;float: right;padding: 0"
                                                             class="col-lg-12">
                                                            <div class="waves-effect waves-light file-upload">
                                                                {{--لایه اول--}}
                                                                <div class="coll-lg-12 iconuploadercenter">


                                                                    <i class="fas fa-cloud-upload-alt fa-3x"></i>

                                                                    <p> کارت ملی</p>
                                                                </div>
                                                                {{-- لایه دوم نمایش تصویر--}}
                                                                <div class="showimgloading">
                                                                    <a onclick="removeimglogo('nationalcart')"
                                                                       style="height: 30px;font-size: 10pt;line-height: 13px;width: 60px;padding: 7px;"
                                                                       class="btn btn-sm btn-danger">حذف <i
                                                                                class="fas fa-trash ml-1"></i></a>

                                                                    <div style="width: 100%;height: 100%;float: right"
                                                                         class="image-holderlogo">

                                                                        {{--  @if($detailsseting['logo'] !=='')
                                                                              <img src="{{asset('/img/logo/'.$detailsseting['logo'])}}">
                                                                          @endif--}}
                                                                    </div>

                                                                </div>

                                                                <div class="showerroruploadimg">
                                                                    <p></p>
                                                                    <a style="height: 30px;margin-top: 5px;margin-left: 5px;line-height: 8px;"
                                                                       onclick="hiderroruploadimglogo('nationalcart')"
                                                                       class="btn btn-danger">متوجه
                                                                        شدم</a>
                                                                </div>

                                                                <input name="imgnationalcart"
                                                                       onchange="uploadimglogo(this,'nationalcart')"
                                                                       type="file" class="fileuploadlogo"/>


                                                            </div>


                                                        </div>


                                                    </div>
                                                </li>
                                            </ul>
                                            {{--<button type="submit"
                                                    style="float: right;position: absolute;left: 5px;bottom: 25px;z-index: 2;width: 80px;font-size: 9pt;height: 30px;"
                                                    class="buttommaked">آپلود تصویر
                                            </button>--}}

                                        </div>
                                    </div>
                                </li>
                                <li class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div>
                                        <div style="background-color: unset;padding: 0" class="formaddmaharat">

                                            <ul style="width: 100%">
                                                <li style="width: 100%">
                                                    <div>
                                                        {{--  <label>تصویر(چک و ..)</label>--}}
                                                        <div style="margin-bottom: 10px;float: right;padding: 0"
                                                             class="col-lg-12">
                                                            <div class="waves-effect waves-light file-upload">
                                                                {{--لایه اول--}}
                                                                <div class="coll-lg-12 iconuploadercenter">


                                                                    <i class="fas fa-cloud-upload-alt fa-3x"></i>

                                                                    <p> گواهی مهارت </p>
                                                                </div>
                                                                {{-- لایه دوم نمایش تصویر--}}
                                                                <div class="showimgloading">
                                                                    <a onclick="removeimglogo('maharat')"
                                                                       style="height: 30px;font-size: 10pt;line-height: 13px;width: 60px;padding: 7px;"
                                                                       class="btn btn-sm btn-danger">حذف <i
                                                                                class="fas fa-trash ml-1"></i></a>

                                                                    <div style="width: 100%;height: 100%;float: right"
                                                                         class="image-holderlogo">

                                                                        {{--  @if($detailsseting['logo'] !=='')
                                                                              <img src="{{asset('/img/logo/'.$detailsseting['logo'])}}">
                                                                          @endif--}}
                                                                    </div>

                                                                </div>

                                                                <div class="showerroruploadimg">
                                                                    <p></p>
                                                                    <a style="height: 30px;margin-top: 5px;margin-left: 5px;line-height: 8px;"
                                                                       onclick="hiderroruploadimglogo('maharat')"
                                                                       class="btn btn-danger">متوجه
                                                                        شدم</a>
                                                                </div>

                                                                <input name="imgmaharat"
                                                                       onchange="uploadimglogo(this,'maharat')"
                                                                       type="file"
                                                                       class="fileuploadlogo"/>


                                                            </div>


                                                        </div>


                                                    </div>
                                                </li>
                                            </ul>
                                            {{-- <button type="submit"
                                                     style="float: right;position: absolute;left: 5px;bottom: 25px;z-index: 2;width: 80px;font-size: 9pt;height: 30px;"
                                                     class="buttommaked">آپلود تصویر
                                             </button>--}}

                                        </div>
                                    </div>
                                </li>

                                <li class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div>
                                        <div style="background-color: unset;padding: 0" class="formaddjavaz">


                                            <ul style="width: 100%">
                                                <li style="width: 100%">
                                                    <div>
                                                        {{--  <label>تصویر(چک و ..)</label>--}}
                                                        <div style="margin-bottom: 10px;float: right;padding: 0"
                                                             class="col-lg-12">
                                                            <div class="waves-effect waves-light file-upload">
                                                                {{--لایه اول--}}
                                                                <div class="coll-lg-12 iconuploadercenter">


                                                                    <i class="fas fa-cloud-upload-alt fa-3x"></i>

                                                                    <p> جواز کسب </p>
                                                                </div>
                                                                {{-- لایه دوم نمایش تصویر--}}
                                                                <div class="showimgloading">
                                                                    <a onclick="removeimglogo('javaz')"
                                                                       style="height: 30px;font-size: 10pt;line-height: 13px;width: 60px;padding: 7px;"
                                                                       class="btn btn-sm btn-danger">حذف <i
                                                                                class="fas fa-trash ml-1"></i></a>

                                                                    <div style="width: 100%;height: 100%;float: right"
                                                                         class="image-holderlogo">

                                                                        {{--  @if($detailsseting['logo'] !=='')
                                                                              <img src="{{asset('/img/logo/'.$detailsseting['logo'])}}">
                                                                          @endif--}}
                                                                    </div>

                                                                </div>

                                                                <div class="showerroruploadimg">
                                                                    <p></p>
                                                                    <a style="height: 30px;margin-top: 5px;margin-left: 5px;line-height: 8px;"
                                                                       onclick="hiderroruploadimglogo('javaz')"
                                                                       class="btn btn-danger">متوجه
                                                                        شدم</a>
                                                                </div>

                                                                <input name="imgjavaz"
                                                                       onchange="uploadimglogo(this,'javaz')"
                                                                       type="file"
                                                                       class="fileuploadlogo"/>


                                                            </div>


                                                        </div>


                                                    </div>
                                                </li>
                                            </ul>
                                            {{--  <button type="submit"
                                                      style="float: right;position: absolute;left: 5px;bottom: 25px;z-index: 2;width: 80px;font-size: 9pt;height: 30px;"
                                                      class="buttommaked">آپلود تصویر
                                              </button>--}}
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <div class="divbuttom">

                                <button type="submit"
                                        {{--v-on:click="updatedetailsuser('informationpersonal')"--}} class="buttommaked">
                                    ثبت تغییرات
                                </button>

                            </div>


                        </div>
                    </form>
                @endif


                @if($statusconfirmmadarek==1)
                    <p class="alert alert-primary">مدارک شما ارسال گردیده است و پس از تایید توسط تیم پشتیبانی نمایش داده
                        خواهد شد.</p>
                @endif



                @if($statusconfirmmadarek==2)

                    <ul class="ulshowmadarek">
                        <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                              <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgnationalcode }}">
                                <h4 class="tx-gray">کارت ملی</h4>
                            </div>
                        </li>

                        <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                              <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgmaharat}}">
                                <h4 class="tx-gray">گواهی مهارت</h4>
                            </div>
                        </li>

                        <li class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div>
                              <img src="{{asset('/images/usersimg/madarek/')}}{{\Illuminate\Support\Facades\Auth::user()->imgjavazkasb}}">
                                <h4 class="tx-gray">جواز کسب</h4>
                            </div>
                        </li>


                    </ul>




                @endif


            </div>


        </div>

    </section>








@endsection