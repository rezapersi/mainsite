@extends('panelkarshenasan.master')
@section('content')

    <section style="position: relative;right: 0;left: 0;margin: auto;float: none" class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">

        <ul class="ulbagmoney">

            <li class="paddingnone col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/creditcard.png')}}">
                    <div style="position: relative">
                        <input v-model="numbershaba" type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="24" placeholder="شماره شبا خود را وارد نمایید">
                        <span class="cardshaba">IR</span>
                    </div>
                    <div >
                        <input  type="number" min="0" v-model="requestmoney"  placeholder="مبلغ درخواست (تومان)">
                    </div>
                    <div class="bg-red bagmoneyborderdown">


                        <div>

     <span v-on:click="insertrequestmoney" style="right: 1px;left: auto;top: 1px;height: 35px;" class="tx-gray">
ثبت درخواست
        </span>

  <span v-on:click="getrequestmoney" data-toggle="modal" data-target="#modelshowrequestmoney" style="left: 1px;right: auto;top: 1px;height: 35px;" class="tx-gray">
 درخواست های من

    </span>

                        </div>


                    </div>

                </div>
            </li>

            <li class="paddingnone col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/Moneywallet.png')}}">
                    <h4 class="tx-gray">موجودی کیف پول</h4>

                   <p class="bagmoney tx-green">{{number_format(\Auth::user()->bagmoney)}} تومان</p>

                    <div class="bg-primery bagmoneyborderdown">

                      <span data-toggle="modal" data-target="#modalchargebagmoney" style="border: 1px solid #01c2ed" class="tx-gray">
 افزایش اعتبار
                      </span>


                    </div>
                </div>
            </li>
        </ul>



        <div style="float: right;margin-top: 30px;display: block" class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>
                    <th scope="col"> تاریخ</th>
                    <th scope="col"> شناسه تراکنش</th>
                    <th scope="col">عنوان تراکنش</th>
                    <th scope="col">مبلغ</th>
                    <th scope="col">موجودی</th>
                </tr>

                </thead>
                <tbody>
                <tr v-for="get in allreportbagmoney.data">
                    <td style="direction: ltr">@{{get.date}}</td>
                    <td style="font-family: Tahoma">@{{get.factorcode}}</td>
                    <td>@{{get.title}}</td>
                    <td v-if="get.typereport_id==1" class="tx-green">@{{Number(get.price).toLocaleString()}} تومان</td>
                    <td v-if="get.typereport_id==2" class="tx-red">@{{Number(get.price).toLocaleString()}} تومان</td>
                    <td class="tx-green">@{{Number(get.newbagmoney).toLocaleString()}} تومان</td>
                </tr>
                </tbody>
            </table>

        </div>
        <div style="display: block" v-if="withwindow<630" v-for="get in allreportbagmoney.data" class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                <li>
                    <div class="flatright">
                        تاریخ
                    </div>
                    <div style="direction: ltr" class="flatleft">
                        @{{get.date}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        شناسه تراکنش
                    </div>
                    <div style="font-family: Tahoma" class="flatleft">
                        @{{get.factorcode}}
                    </div>
                </li>

                <li>
                    <div class="flatright">
                        عنوان تراکنش
                    </div>
                    <div  class="flatleft">
                        @{{get.title}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        مبلغ
                    </div>


                    <div  v-if="get.typereport_id==1"  class="tx-green flatleft">
                        @{{Number(get.price).toLocaleString() }} تومان
                    </div>
                    <div  v-if="get.typereport_id==2"  class="tx-red flatleft">
                        @{{Number(get.price).toLocaleString() }} تومان
                    </div>



                </li>

                <li>
                    <div class="flatright">
                        موجودی
                    </div>
                    <div  class="tx-green flatleft">
                        @{{Number(get.newbagmoney).toLocaleString() }} تومان
                    </div>
                </li>



            </ul>

        </div>

        <pagination :data="allreportbagmoney" v-on:pagination-change-page="getreportbagmoney">
            <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
            <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
        </pagination>


    </section>


    <div class="modal fade" id="modelshowrequestmoney" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 style="width: 100%;text-align: right;font-size: 13pt;color: #666"
                        class="h4headremodal">درخواست های من</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div style="float: right;margin-top: 30px;display: block" class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <table style="background-color: white" class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">شماره فاکتور</th>
                                <th scope="col"> تاریخ درخواست</th>
                                <th scope="col"> مبلغ درخواست</th>
                                <th scope="col">شماره شبا</th>
                                <th scope="col">وضعیت درخواست</th>


                            </tr>

                            </thead>
                            <tbody>
                            <tr v-for="get in allrequestmoney.data">
                                <td class="tx-red" style="font-family: Tahoma">@{{get.numberfacture}}</td>
                                <td style="direction: ltr">@{{get.daterequest}}</td>
                                <td class="tx-green">@{{Number(get.price).toLocaleString()}} تومان</td>
                                <td style="font-weight: bold">@{{get.numbershaba}}</td>

                                <td v-if="get.statusrequestmoney.id==1" class="tx-primery">@{{get.statusrequestmoney.title}}</td>
                                <td v-if="get.statusrequestmoney.id==2" class="tx-green" >@{{get.statusrequestmoney.title}}</td>
                                <td v-if="get.statusrequestmoney.id==3" class="tx-red">
                                    @{{get.statusrequestmoney.title}}
                                    <i v-on:click="showmessagecancelrequestmoney(get.id)" style="margin-right: 5px;font-size: 15pt;height: 20px;" class="tx-red far fa-question-circle"></i>
                                </td>


                            </tr>
                            </tbody>
                        </table>
                    </div>


                    <div style="display: block" v-if="withwindow<630" v-for="get in allrequestmoney.data"
                         class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">


                            <li>
                                <div class="flatright">
                                    شماره فاکتور
                                </div>
                                <div style="font-family: Tahoma" class="tx-red flatleft">
                                    @{{get.numberfacture}}
                                </div>
                            </li>



                            <li>
                                <div class="flatright">
                                    تاریخ درخواست
                                </div>
                                <div style="direction: ltr" class="flatleft">
                                    @{{get.daterequest}}
                                </div>
                            </li>


                            <li>
                                <div class="flatright">
                                    مبلغ درخواست
                                </div>

                                <div class="tx-red flatleft">
                                    @{{Number(get.price).toLocaleString() }} تومان
                                </div>

                            </li>


                            <li>
                                <div class="flatright">
                                    شماره شبا
                                </div>
                                <div style="font-weight: bold" class="flatleft">
                                    @{{get.numbershaba}}
                                </div>
                            </li>





                            <li>
                                <div class="flatright">
                                    وضعیت درخواست
                                </div>


                                <div v-if="get.statusrequestmoney.id==1"  class="flatleft tx-primery">
                                    @{{get.statusrequestmoney.title}}
                                </div>
                                <div v-if="get.statusrequestmoney.id==2"  class="flatleft tx-green">
                                    @{{get.statusrequestmoney.title}}
                                </div>
                                <div v-if="get.statusrequestmoney.id==3"  class="flatleft tx-red">
                                    @{{get.statusrequestmoney.title}}
                                    <i v-on:click="showmessagecancelrequestmoney(get.id)" style="margin-right: 5px;font-size: 15pt;height: 20px;" class="tx-red far fa-question-circle"></i>

                                </div>


                            </li>




                        </ul>

                    </div>


                    <pagination :data="allrequestmoney" v-on:pagination-change-page="getrequestmoney">
                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                    </pagination>


                </div>

            </div>
        </div>
    </div>






@endsection