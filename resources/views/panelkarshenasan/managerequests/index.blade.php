@extends('panelkarshenasan.master')
@section('content')

    <section style="position: relative;right: 0;left: 0;margin: auto;float: none"
             class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">

        @if(\Session::exists('errorinsertfacture'))
            <p style="margin-top: 10px" class="alert alert-danger">{{\Session::get('errorinsertfacture')}}</p>
        @endif

        @if(\Session::exists('successinsertfacture'))
            <p style="margin-top: 10px" class="alert alert-success">{{\Session::get('successinsertfacture')}}</p>
        @endif

        <ul class="ulstatusrequest">
            <li class="numberrequestkol paddingnone col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/allrequest.png')}}">
                    <h4>0</h4>
                    <p>کل درخواست ها</p>
                </div>
            </li>
            <li class="numberrequestdarhalanjam paddingnone col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div>
                    <img style="height: 53px" src="{{asset('/images/panels/active.png')}}">
                    <h4>0</h4>
                    <p>درخواست های در حال انجام</p>
                </div>
            </li>
            <li class="numberrequestanjamshode paddingnone col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div>
                    <img src="{{asset('/images/panels/conform.png')}}">
                    <h4></h4>
                    <p>درخواست های انجام شده</p>
                </div>
            </li>

        </ul>

        <div style="float: right;margin-top: 30px;display: block" class="table-responsive text-nowrap col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <table style="background-color: white" class="table ">
                <thead>
                <tr>
                    <th scope="col">شناسه درخواست</th>
                    <th scope="col">تاریخ ثبت</th>
                    <th scope="col">نام و مدل دستگاه</th>
                    <th scope="col"> توضیحات</th>
                    <th scope="col">هزینه درخواست</th>
                    <th scope="col">وضعیت</th>
                    <th scope="col">جزئیات بیشتر</th>
                </tr>

                </thead>
                <tbody>

                <tr v-for="get in allrequesttkarshenasan.data">

                    <td style="color: orangered;font-family: Tahoma">@{{get.trackingcode}}</td>
                    <td style="direction: ltr">@{{get.date}}</td>
                    <td>@{{get.maincat.title}} @{{get.bernaddevice.title}}  @{{get.modeldevice.title}}</td>

                    <td style="cursor: pointer" v-on:click="getdetailsrequest(get.id)" data-toggle="modal"
                        data-target="#modelshowproblem" class="maincolor">
                        مشاهده
                    </td>

                    <td v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3">---</td>
                    <td v-if="get.statusrequest_id==4 || get.statusrequest_id==5">@{{Number(get.pricefacture).toLocaleString()}} تومان</td>


                    <td v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3" class="tx-red">@{{get.statusrequest.title}}</td>

                    <td v-if="get.statusrequest_id==4" class="tx-red">@{{get.statusrequest.title}}</td>
                    <td v-if="get.statusrequest_id==5" class="tx-green">@{{get.statusrequest.title}}</td>




                    <td style="cursor: pointer" v-if="get.statusrequest_id==3 || get.statusrequest_id==4 || get.statusrequest_id==5"
                        class="maincolor" v-on:click="getdetailsrequest(get.id)" data-toggle="modal"
                        data-target="#modelmorerequest">
                        بیشتر
                    </td>

                    <td v-if="get.statusrequest_id==2">
                        <button v-on:click="confirmrequestbykarshenas(get.id)"
                                style="width: 87px;font-size: 9pt;height: 25px;" class="buttommaked">تایید درخواست
                        </button>
                    </td>


                </tr>
                </tbody>
            </table>

        </div>
        <div v-if="withwindow<630" v-for="get in allrequesttkarshenasan.data"
             class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                <li>
                    <div class="flatright">
                        شناسه درخواست
                    </div>
                    <div style="color: orangered;font-family: Tahoma" class="flatleft">
                        @{{get.trackingcode}}
                    </div>

                </li>
                <li>
                    <div class="flatright">
                        تاریخ ثبت
                    </div>
                    <div style="direction: ltr" class="flatleft">
                        @{{get.date}}
                    </div>

                </li>

                <li>
                    <div class="flatright">
                        نام و مدل دستگاه
                    </div>
                    <div class="flatleft">
                        @{{get.maincat.title}} @{{get.bernaddevice.title}}  @{{get.modeldevice.title}}
                    </div>
                </li>


                <li>
                    <div class="flatright">
                         توضیحات
                    </div>
                    <div class="flatleft">
                        <i v-on:click="getdetailsrequest(get.id)" data-toggle="modal" data-target="#modelshowproblem"
                           class=" fas fa-chevron-left"></i>
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        هزینه درخواست
                    </div>

                    <div v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3" class="flatleft">---</div>
                    <div v-if="get.statusrequest_id==4 || get.statusrequest_id==5" class="flatleft">
                        @{{Number(get.pricepay).toLocaleString()}} تومان
                    </div>
                </li>


                <li>
                    <div class="flatright">
                        وضعیت
                    </div>


                    <div v-if="get.statusrequest_id==1 || get.statusrequest_id==2 || get.statusrequest_id==3"
                         class="tx-red flatleft">
                        @{{get.statusrequest.title}}
                    </div>
                    <div v-if="get.statusrequest_id==4" class="tx-red flatleft">
                        @{{get.statusrequest.title}}
                    </div>
                    <div v-if="get.statusrequest_id==5" class="tx-green flatleft">
                        @{{get.statusrequest.title}}
                    </div>




                </li>


                <li>
                    <div class="flatright">
                        جزئیات بیشتر
                    </div>


                    <div v-if="get.statusrequest_id==3 || get.statusrequest_id==4 || get.statusrequest_id==5" class="flatleft">
                        <i v-on:click="getdetailsrequest(get.id);getreportsrequest()" data-toggle="modal"
                           data-target="#modelmorerequest" class="fas fa-chevron-left"></i>
                    </div>


                    <div v-if="get.statusrequest_id==2" class="flatleft">
                        <button v-on:click="confirmrequestbykarshenas(get.id)"
                                style="width: 87px;font-size: 9pt;height: 25px;" class="buttommaked">تایید درخواست
                        </button>
                    </div>


                </li>


            </ul>

        </div>

        <pagination :data="allrequesttkarshenasan" v-on:pagination-change-page="getrequesttkarshenasan">
            <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
            <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
        </pagination>


    </section>

    {{--modal moshkel--}}

    <div class="modal fade" id="modelshowproblem" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog modal-xl modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 v-for="get in detailsrequest" style="text-align: right;font-family: Tahoma;font-size: 14pt;"
                        class="tx-primery modal-title w-100" id="myModalLabel">@{{get.trackingcode}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div v-for="get in detailsrequest"
                     style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body">

                    <!-- <div style="background-color:#fbfbfb;border-radius: 5px;min-height: 50px;margin-bottom: 10px;"
                         class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <label style="float: right;margin-top: 10px">نوع مشکل:</label>
                        <p style="float: right;padding: 9px;color:#333;">@{{get.problem.title}}</p>
                    </div> -->

                    <div style="background-color:#fbfbfb;border-radius: 5px;min-height: 50px;margin-bottom: 10px;"
                         class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <p class="tx-primery" style="padding: 10px;margin-top: 5px">@{{get.description}}</p>
                    </div>

                </div>


            </div>
        </div>
    </div>


    <div style="height: 100%" class="modal fade" id="modelmorerequest" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div style="display: contents" class="modal-dialog modal-xl modal-lg" role="document">
            <div style="height: 100%;background-color: white" class="modal-content">

                <div class="modal-header">
                    <h4 v-for="get in detailsrequest" style="text-align: right;font-family: Tahoma;font-size: 14pt;"
                        class="tx-primery modal-title w-100" id="myModalLabel">@{{get.trackingcode}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div v-for="get in detailsrequest" style="font-size: 11pt;font-family: yekan;text-align: justify;color: #777;" class="modal-body ">

                    <div style="position: relative;right: 0;left: 0;margin: auto;float: none" class="morerequest col-lg-8 col-md-8 col-sm-11 col-12">

                        <h4 v-if="get.showdetailstokarshenas==1" style="font-size: 14pt">اطلاعات مشتری</h4>
                        <div v-if="get.showdetailstokarshenas==1" class="informationuser">

                            <ul>

                                <li class="col-lg-6 col-md-6 colsm-12 col-12">
                                    <div>
                                        <label class="tx-primery">نام کاربر:</label>
                                        <p>@{{get.user.name}} @{{get.user.family}}</p>
                                    </div>
                                </li>

                                <li class="col-lg-6 col-md-6 colsm-12 col-12">
                                    <div>
                                        <label class="tx-primery">شماره تماس:</label>
                                        <p>@{{get.user.mobile}}</p>
                                    </div>
                                </li>

                                <li class="col-lg-6 col-md-6 colsm-12 col-12">
                                    <div>
                                        <label class="tx-primery"> استان و شهر:</label>
                                        <p>@{{get.user.ostan}} - @{{get.user.city}}</p>
                                    </div>
                                </li>


                                <li class="col-lg-6 col-md-6 colsm-12 col-12">
                                    <div>
                                        <label class="tx-primery"> کد پستی:</label>
                                        <p>@{{get.user.codeposti}} </p>
                                    </div>
                                </li>

                                <li class="col-lg-12 col-md-12 colsm-12 col-12">
                                    <div class="tx-primery">
                                        <label> آدرس:</label>
                                        <p>@{{get.user.address}} </p>
                                    </div>
                                </li>

                            </ul>


                        </div>


                        <h4 v-if="get.showdetailstokarshenas==1" style="font-size: 14pt">گزارشات</h4>
                        <div v-if="get.statusrequest_id !==5 && get.statusrequest_id !==4" style="float: right;width: 100%;background-color:#fbfbfb;border-radius: 3px;margin-bottom: 25px;" class="divinsertreport">

                            <div id="divinsertreportrequest" style="margin-top: 5px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <textarea v-model="textreportrequest" placeholder="متن گزارش"
                                          style="font-size: 11pt;margin-bottom: 10px;height: 120px;margin-top: 15px;"
                                          class="form-control"></textarea>
                                <button v-on:click="insertreportrequest" style="width: 85px;margin-bottom: 10px"
                                        class="buttommaked">ثبت گزارش
                                </button>
                                <p class="text-error"></p>
                            </div>
                        </div>

                        <div  class="table-responsive text-nowrap">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">تاریخ ثبت</th>
                                    <th scope="col">گزارش</th>
                                    <th scope="col">حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="report in allreportrequest.data">
                                    <td style="direction: ltr">@{{report.date}}</td>
                                    <td>@{{report.report}}</td>
                                    <td>
                                        <i v-on:click="deletereportrequest(report.id)"
                                           style="cursor: pointer;color: #ef5662"
                                           class="fas fa-trash fa-lg">
                                        </i>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>


                        <div v-if="withwindow<630" v-for="report in allreportrequest.data"
                             class="flat col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <ul class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                                <li>
                                    <div class="flatright">
                                        تاریخ ثبت
                                    </div>
                                    <div style="color: orangered" class="flatleft">
                                        @{{report.date}}
                                    </div>

                                </li>

                                <li>
                                    <div class="flatright">
                                        گزارش
                                    </div>
                                    <div class="flatleft">
                                        @{{report.report}}
                                    </div>
                                </li>


                                <li>
                                    <div class="flatright">
                                        حذف
                                    </div>
                                    <div class="flatleft">
                                        <i v-on:click="deletereportrequest(report.id)"
                                           style="cursor: pointer;color: #ef5662" class="fas fa-trash fa-lg"></i>
                                    </div>
                                </li>


                            </ul>

                        </div>

                        <pagination :data="allreportrequest" v-on:pagination-change-page="getreportsrequest">
                            <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                            <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                        </pagination>


                       <div v-if="get.statusrequest_id !==4 && get.statusrequest_id !==5">
                            <div v-if="get.reportrequestts.length>0" class="endwork">
                                <button v-on:click="showmodelendrequest" style="width: 110px;margin-bottom: 10px"
                                        class="buttommaked">ثبت پایان کار
                                </button>
                            </div>
                        </div>

                        <div v-if="get.statusrequest_id ==4 || get.statusrequest_id ==5">
                            <div class="endwork">
                                <button v-on:click="showmodelfacture();getfacturerequest()" style="width: 110px;margin-bottom: 10px"
                                        class="buttommaked">مشاهده فاکتور
                                </button>
                            </div>
                        </div>


                    </div>

                </div>


            </div>
        </div>
    </div>

    <div style="display: none" id="modelendrequest" class="modalmaked" tabindex="-1" role="dialog">

        <div v-for="get in detailsrequest" class="modal-header">
            <h4 style="font-size: 12pt" class="tx-primery">ثبت نهایی درخواست (@{{get.trackingcode}})</h4>
            <button v-on:click="closemodalemaked" style="border: none">
                        <span>بستن
                           <i class="fas fa-times"></i>
                        </span>
            </button>

        </div>

        <div v-for="get in detailsrequest" class="modal-body">
            <div style="margin-bottom: 35px;min-height: 85px;margin-top: 10px;float: right;width: 100%;"
                 class="divinsertrequest">

                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h4 class="tx-gray" ز style="font-size: 14pt;float: right;height: 40px;line-height: 40px;">ثبت اقلام
                        فاکتور</h4>
                    <ul style="margin-bottom: 10px" class="ulinsert">
                        <li id="liaddrizfacture_babat" class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div>
                                <label>بابت</label>
                                <input v-model="babataddrizfacture" class="form-control">
                                <p class="text-error"></p>
                            </div>
                        </li>

                        <li id="liaddrizfacture_price" class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div>
                                <label>مبلغ(تومان)</label>
                                <input type="number" min="0" v-model="priceaddrizfacture" class="form-control">
                                <p class="text-error"></p>
                            </div>

                        </li>

                        <li class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div>
                                <label style="margin-bottom: 0"></label>
                                <button v-on:click="addfeilderizfacture"
                                        style="width: 110px;margin-bottom: 10px;float: right;" class="buttommaked">
                                    افزودن <i style="margin-right: 12px;" class="far fa-plus-square"></i></button>
                            </div>
                        </li>


                    </ul>


                    <form id="formsodorfactureforrequest" method="post" action="{{route('sodorfactureforrequest')}}">
                        @csrf

                        <input type="hidden" name="idrequest" :value="get.id">
                        <ul class="ulstrizfactore">
                            {{--<li class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <i onclick="closefieldrizfacture(this)" class="tx-red far fa-window-close"></i>
                                <div>
                                    <input type="hidden" name="babat[]">
                                    <p class="tx-primery">هزینه درخواست</p>
                                </div>
                                <div>
                                    <input type="hidden" name="price[]">
                                    <p class="tx-green">4000 تومان</p>
                                </div>
                            </li>--}}

                        </ul>
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">مبلغ کل </h4>
                            <p class="pricekolrizfacture">0 تومان</p>
                        </div>

                        <div style="width: 100%">
                            <button v-on:click.stop.prevent="sodorfactureforrequest"
                                    style="width: 145px;margin-bottom: 20px;"
                                    class="buttommaked">ثبت و صدور فاکتور
                            </button>
                        </div>

                    </form>


                </div>


            </div>


        </div>


    </div>

    <div style="display: none" id="modelshowfacture" class="modalmaked" tabindex="-1" role="dialog">

        <div v-for="get in detailsrequest" class="modal-header">
            <h4 style="font-size: 10pt;line-height: 27px;" class="tx-primery">فاکتور درخواست (@{{get.trackingcode}})</h4>
            <button v-on:click="closemodalemaked" style="border: none">
                        <span>بستن
                           <i class="fas fa-times"></i>
                        </span>
            </button>

        </div>

        <div v-for="get in detailsrequest" class="modal-body bg-primery">


            <div v-for="facture in facturerequest" id="pdffactor" class="factor col-lg-7 col-md-8 col-sm-10 col-11">

                <div class="factor_header">


                    <div v-if="facture.statuspay.id==1" style="position: absolute;left: 10px;top: 14px;font-size: 9pt;color:white;background-color:#33e3ec;padding: 3px;border-radius: 4px;height: 25px;">
                        @{{facture.statuspay.title}}
                    </div>

                    <div v-if="facture.statuspay.id==2" style="position: absolute;left: 10px;top: 14px;font-size: 9pt;color:white;background-color:forestgreen;padding: 3px;border-radius: 4px;height: 25px;">
                        @{{facture.statuspay.title}}
                    </div>


                    <ul>
                        <li class="lilogo">
                            <div>
                                <img src="{{asset('/images/logo.png')}}">
                            </div>
                        </li>

                        <li class="lidate_numberfactor">
                            <div>
                                <div>
                                    <p>شماره فاکتور </p>
                                    <h4>@{{facture.numberorder_showcustomer}}</h4>
                                </div>

                                <div>
                                    <p>تاریخ</p>
                                    <h4 style="direction: ltr">@{{facture.datefactor}}</h4>
                                </div>

                            </div>


                        </li>
                    </ul>

                    <div class="factorheader">

                    </div>

                </div>

                <div class="factor_body">

                    <ul class="ulfieldfacture">
                        <li v-for="rizfacture in facture.rizfactures">
                            <div class="tx-gray col-lg-8 col-md-8 col-sm-8 col-12">@{{rizfacture.babat}}</div>
                            <div class="tx-primery col-lg-4 col-md-4 col-sm-4 col-12">@{{Number(rizfacture.price).toLocaleString()}} تومان</div>
                        </li>


                        <li style="background-color: rgb(119, 230, 209)">
                            <div style="color: #045704" class="col-lg-8 col-md-8 col-sm-8 col-12">جمع کل</div>
                            <div style="color: #045704" class="col-lg-4 col-md-4 col-sm-4 col-12">@{{Number(facture.pricekol).toLocaleString()}} تومان</div>
                        </li>

                    </ul>


                </div>

            </div>


        </div>


    </div>



@endsection


