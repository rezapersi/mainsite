<!DOCTYPE html>
<html style="overflow-x:hidden" lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#2c93bd">
    <link rel="icon" href="{{asset('/images/logos/digiyarlogo.jpg')}}" alt="دیجی یار" title="دیجی یار" sizes="16x16" type="image/x-icon">
    <title>دیجی یار</title>
    <!-- MDB icon -->

    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link href="{{asset('/css/mdb.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/compiled-4.6.0.css')}}" rel="stylesheet">
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/bootstrap-select.css')}}" rel="stylesheet">

    <link rel="icon" href="{{asset('/index/img/mdb-favicon.ico')}}" type="image/x-icon">
    <!-- Font Awesome -->
    <link href="{{asset('/index/fontawesome/css/all.css')}}" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="{{asset('/index/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('/index/css/stylesheet.css')}}" type="text/css" charset="utf-8"/>

    <link href="{{asset('/slidertouch/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('/slidertouch/dist/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('/index/css/services-styles.css')}}" rel="stylesheet">


</head>

<body >


<div id="largeloading" class="loading">
    <div>

        <h4 style="color:black">لطفا چند
            لحظه صبر نمایید ...</h4>
        <span>
    <img src="{{asset('/loading/three-dots.svg')}}">
    </span>
    </div>

</div>


<style>
    .navigation {
        width: 100%;
        height: 80px;
        position: fixed;
        background-color: white;
        bottom: 0;
    }


</style>


<div id="app">


    <!--#################################################################
     -
     -bottom navbar section
     -
    ######################################################################### -->


    <?php
    if(\Illuminate\Support\Facades\Auth::user()){
        $role = Auth::user()->leveluser_id;
        if ($role == '5') {
            $url='/panelkarshenasan/managerequests';
        } elseif ($role == '7') {
            $url='/panel/dashboard';
        }
        else {
            $url='https://admin.dgyr.ir/login';
        }

        $textloginorpanel='پنل کاربری';
        $textloginorpanel2='پنل کاربری';

    }else{
        $url='/login';
        $textloginorpanel='ورود به حساب کاربری';
        $textloginorpanel2='ورود';

    }

    ?>


    <div class="fixed-bottom lg-device-display-none bottom-navbar border-top z-depth-2" style="height:45px">

        <div>

            <div>

                <a  href="#">

                </a>
                <p style="line-height: 18px;">پشتیبانی</p>
            </div>


            <div>
                <a  href="/marketplace">
                    <i style="font-size: 13pt;color: white" class="fas fa-shopping-cart"></i>
                    <p>فروشگاه</p>
                </a>

            </div>


            <div>
                <span id="openmodalinsertrequest" v-on:click="defaultinsertrequest" data-toggle="modal"
                      data-target="#modalinsertrequest" class="indicator">
                    <i class="fa fa-plus"></i>
                </span>
            </div>

            <div>
                <a  href="/service_price">
                    <i style="font-size: 13pt;color: white" class="fas fa-calculator"></i>
                    <p>محاسبه هزینه</p>
                </a>
            </div>

            <div >
                <a  href="{{$url}}">
                    <i style="font-size: 13pt;color: white" class="fa fa-fw fa-user"></i>
                    <p>{{$textloginorpanel2}}</p>

                </a>
            </div>
        </div>


    </div>


    <!--#################################################################
     -
     -large devices navbar
     -
    ######################################################################### -->


    <nav class=" header-top-area sm-device-display-none shadow fixed-top z-depth-2 ">


        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo ">
                        <a href="/">
                            <img SRC="{{asset('/index/images/logo/logo.png')}}">
                        </a>
                    </div>
                    <!-- Search form -->
                <!--
                    <div class="md-form float-right pr-4 col-lg-6 col-md-4">
                    <i class="fa fa-fw fa-search float-right"></i>
                        <input v-model="getsearchkhadamat" style="float:right;float: right;width: 85%;"  type="text" placeholder="جستجو بین خدمات" aria-label="Search">


<div class="contentsearch">

<ul>
    <li v-for="get in searchkhadamat">
        <a :href="'/service/'+''+get.id">

        <img :SRC="'{{\App\Setting::first()->root}}img/khadamat/'+''+get.img">
        <p>@{{get.title}}</p>
</a>
    </li>

</ul>


</div>

                    </div>
-->


                    <div class="login-btn">


                        <a href="{{$url}}" type="button" class=" login-btn-shape btn-rounded z-depth-0 btn-flat ">

                           {{$textloginorpanel}}
                            <i class="fa fa-fw fa-user ml-2"></i>
                        </a>




                    </div>

                    <div class="login-btn">
                        <a id="openmodalinsertrequest" v-on:click="defaultinsertrequest" href="#"
                           class=" login-btn-shape btn-rounded z-depth-0 btn-flat" data-toggle="modal"
                           data-target="#modalinsertrequest">
                            ثبت درخواست
                        </a>
                    </div>


                    <div>
                        <a href=" tel:09010181932" class=" btn-floating btn-large z-depth-0 navbar-btns-shape ">
                            <i class="fas fa-phone btn-flat" style="color:black"></i>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </nav>

    <!--#################################################################
     -
     -sm devices navbar
     -
    ######################################################################### -->


    <nav class="dgyr-navbar navbar navbar-expand-lg navbar-light sticky-top lg-device-display-none border-bottom">
        <div class="container">
            <div class="logo">
                <a class="navbar-brand" href="/">
                    <img SRC="{{asset('/index/images/logo/logo.png')}}">
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                    aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse navbar-list " id="navbarTogglerDemo01">
                <ul class="navbar-nav ml-auto ">

                    <li class="nav-item active">
                        <a style="border-radius: 3px;background-color: #fbfbfb;font-size: 12pt" class="nav-link"
                           href="/service_price"> محاسبه هزینه خدمات<span class="sr-only">(current)</span></a>
                    </li>

                    {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link" href="#">آیتم 2</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">--}}
                    {{--آیتم 3 </a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </nav>


<!--
    <div>
     <div class="container lg-device-display-none  fixed-top">

    <div class="col-md-12 ">
      <div class="row">

        <div style="margin-top:-70px;margin-right:15px;position: absolute;">

          
          <div class=" z-depth-1 white " style="position:absolute;height:250px;width:250px;margin-top:-150px;margin-right:30px;border-left:  0px solid grey; border-radius:12px 12px;transform: rotate(-45deg);">

          </div>


          <div class=" z-depth-1 white " style="position:absolute;height:250px;width:250px;margin-top:-150px;border-right: 0px solid grey;margin-right:30px;border-radius:12px 12px;transform:rotate(45deg);">

          </div>
        </div>


      </div>
                  <div style="height:auto;position:absolute;">
            <div class=" " style="padding-top: 15px;padding-right: 130pxpx;padding-bottom: 1px;">
              <a style="width: 50px;height: 30pxpx;" href="">
                <img style="width: 50px;height: 30px;" SRC="{{asset('/index/images/logo/logo_shape.png')}}">
              </a>
            </div>
          </div>

    </div>
  </div>
  
!-->


    <!-- modale insert request -->

    <div class="modal fade" id="modalinsertrequest" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    {{--<h4 style="color: #2f2f2f" class=" modal-title w-100 f">ثبت درخواست</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="padding: 0" class="modal-body mx-3">


                    <div style="float: right;width:100%;padding-left: 0 !important;padding-right: 0 !important;">
                        <div class="divinsertrequeststeps">
                            <div class="insertrequestbody">
                                <section style="display: block" class="section1">


                                    <div class="section1_chosedmaincat">
                                        <div class="section1_chosedmaincat_right col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">

                                            <h4 style="font-size: 15pt;margin-top: 10px;margin-bottom: 5px">انتخاب
                                                سرویس</h4>
                                            {{--<h4>سرویس</h4>--}}
                                            <img src="{{asset('/images/panels/servicesicon.png')}}">

                                        </div>

                                        <div class="section1_chosedmaincat_left col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">

                                            <div style="width:100%" id="scrolling">

                                                <div class="owl-carousel">

                                                    @foreach($khadamat as  $row)
                                                        <div dataid="{{$row->id}}"
                                                             v-on:click="selectmaincat({{$row->id}})"
                                                             class="items">
                                                            <img src="{{\App\Setting::first()->root}}img/khadamat/{{$row->img}}">
                                                            <p>{{$row->title}}</p>
                                                        </div>
                                                    @endforeach

                                                </div>


                                            </div>


                                        </div>

                                    </div>
                                    <div class="section1_stepsdown">


                                        {{-- start --}}
                                        <div style="display: block" class="section1_stepsdown_step1">
                                            <h4>سرویس مورد نظر خود را انتخاب کنید</h4>

                                            <div class="search">
                                                <input v-model="searchservice" class="form-control"
                                                       placeholder="جستجوی سرویس">
                                            </div>

                                            <ul class="ulselectservice">

                                                <li v-for="get in allservice.data" :dataid="get.id"
                                                    v-on:click="selectproblem(get.id)"
                                                    class="col-lg-4 col-md-6 col-sm-6 col-6">
                                                    <div class="waves-effect waves-light">
                                                        <img src="{{asset('/images/panels/addicon.png')}}">
                                                        <p class="tx-green">@{{get.title}} </p>
                                                    </div>
                                                <li>


                                            </ul>


                                            {{-- paginate --}}

                                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                                <div class="divpaginate">
                                                    <pagination :data="allservice"
                                                                v-on:pagination-change-page="getservice">
                                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                                    </pagination>
                                                </div>
                                            </div>


                                            <div id="nextstep1" style="opacity:0" class="nextitem">
<span v-on:click="nextstep(1,'fromindex')" class="bg-primery waves-effect waves-light">

    مرحله بعد
    <i class="fas fa-chevron-left"></i>

</span>
                                            </div>
                                        </div>


                                        {{-- end --}}


                                        <div class="section1_stepsdown_step2">

                                            <h4>برند دستگاه</h4>


                                            <div class="search">
                                                <input v-model="searchberand" class="form-control"
                                                       placeholder="جستجوی برند">
                                            </div>


                                            <ul class="ulselectberand">

                                                <li v-for="get in allberand.data" :dataid="get.id"
                                                    v-on:click="selectberand(get.id)"
                                                    class="col-lg-4 col-md-6 col-sm-6 col-6">
                                                    <div class="waves-effect waves-light">
                                                        <img src="{{asset('/images/panels/addicon.png')}}">
                                                        <p class="tx-green">@{{get.title}}</p>
                                                    </div>
                                                <li>

                                            </ul>

                                            {{-- paginate --}}

                                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                                <div class="divpaginate">
                                                    <pagination :data="allberand"
                                                                v-on:pagination-change-page="getberand">
                                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                                    </pagination>
                                                </div>
                                            </div>


                                            <div id="nextstep2" style="opacity:0" class="nextitem">
            <span v-on:click="nextstep(2,'fromindex')" class="bg-primery waves-effect waves-light">

                مرحله بعد
                <i class="fas fa-chevron-left"></i>
            
            </span>
                                            </div>

                                        </div>


                                        <div class="section1_stepsdown_step3">

                                            <h4>مدل دستگاه</h4>

                                            <div class="search">
                                                <input v-model="searchmodel" class="form-control"
                                                       placeholder="جستجوی مدل">
                                            </div>


                                            <ul class="ulselectmodel">

                                                <li v-for="get in allmodel.data" :dataid="get.id"
                                                    v-on:click="selectmodel(get.id)"
                                                    class="col-lg-4 col-md-6 col-sm-6 col-6">
                                                    <div class="waves-effect waves-light">
                                                        <img src="{{asset('/images/panels/addicon.png')}}">
                                                        <p class="tx-green">@{{get.title}}</p>
                                                    </div>


                                            </ul>


                                            {{-- paginate --}}

                                            <div style="width: 100%;float: right;min-height: 0;background-color: #fbfbfb;border-radius: 5px;box-shadow: 0 0px 0px 1px #eee;">

                                                <div class="divpaginate">
                                                    <pagination :data="allmodel" v-on:pagination-change-page="getmodel">
                                                        <span style="font-family: yekan;color: #666" slot="prev-nav">&lt; قبلی</span>
                                                        <span style="font-family: yekan;color: #666" slot="next-nav">بعدی &gt;</span>
                                                    </pagination>
                                                </div>
                                            </div>


                                            <div id="nextstep3" style="opacity:0" class="nextitem">
            <span v-on:click="nextstep(3,'fromindex')" class="bg-primery waves-effect waves-light">

                مرحله بعد
                <i class="fas fa-chevron-left"></i>
            
            </span>
                                            </div>


                                        </div>


                                        <div class="section1_stepsdown_step4">

                                            <h4>ثبت توضیحات</h4>

                                            <p class="tx-red">به منظور ارائه خدمات بهتر از سوی کارشناسان ما لطفا
                                                توضیحاتی که لازم است
                                                بدانیم را در ذیل وارد نمایید.</p>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <textarea v-model="descriptionrequest"
                                          placeholder="مثال:دستگاه خاموش است و روشن نمی شود."
                                          class="col-lg-12 col-md-12 form-control"></textarea>
                                            </div>


                                            <div class="nextitem">
        <span v-on:click="nextstep(4,'fromindex')" class="bg-primery waves-effect waves-light">

            مرحله بعد
            <i class="fas fa-chevron-left"></i>
        
        </span>
                                            </div>


                                        </div>


                                    </div>

                                </section>


                                {{-- end section 1 --}}




                                {{-- section 2 --}}

                                {{-- address posti --}}

                                <section style="display: none" class="section2">

                                    <div class="insertnewaddres">

                                        <div style="box-shadow:none" class="part">

                                            <div class="part-left col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">


                                                <div class="part-body">
                                                    <ul>

                                                        <li id="liostan"
                                                            class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label>استان</label>
                                                                <select id="ostan" name="ostan"
                                                                        onchange="selectostan(this);"
                                                                        class="selectpicker"
                                                                        data-width="100%">
                                                                    <option data-val="41" value="آذربايجان شرقي">
                                                                        آذربايجان شرقي
                                                                    </option>
                                                                    <option data-val="44" value="آذربايجان غربي">
                                                                        آذربايجان غربي
                                                                    </option>
                                                                    <option data-val="45" value="اردبيل">اردبيل</option>
                                                                    <option data-val="31" value="اصفهان">اصفهان</option>
                                                                    <option data-val="26" value="البرز">البرز</option>
                                                                    <option data-val="84" value="ايلام">ايلام</option>
                                                                    <option data-val="77" value="بوشهر">بوشهر</option>
                                                                    <option data-val="21" value="تهران">تهران</option>
                                                                    <option data-val="38" value="چهار محال بختياري">چهار
                                                                        محال بختياري
                                                                    </option>
                                                                    <option data-val="56" value="خراسان جنوبي">خراسان
                                                                        جنوبي
                                                                    </option>
                                                                    <option data-val="51" value="خراسان رضوي">خراسان
                                                                        رضوي
                                                                    </option>
                                                                    <option data-val="58" value="خراسان شمالي">خراسان
                                                                        شمالي
                                                                    </option>
                                                                    <option data-val="61" value="خوزستان">خوزستان
                                                                    </option>
                                                                    <option data-val="24" value="زنجان">زنجان</option>
                                                                    <option data-val="23" value="سمنان">سمنان</option>
                                                                    <option data-val="54" value="سيستان بلوچستان">سيستان
                                                                        بلوچستان
                                                                    </option>
                                                                    <option data-val="71" value="فارس">فارس</option>
                                                                    <option data-val="28" value="قزوين">قزوين</option>
                                                                    <option data-val="25" value="قم">قم</option>
                                                                    <option data-val="87" value="کردستان">کردستان
                                                                    </option>
                                                                    <option data-val="34" value="کرمان">کرمان</option>
                                                                    <option data-val="83" value="کرمانشاه">کرمانشاه
                                                                    </option>
                                                                    <option data-val="74" value="کهکيلويه وبوير احمد">
                                                                        کهکيلويه وبوير احمد
                                                                    </option>
                                                                    <option data-val="17" value="گلستان">گلستان</option>
                                                                    <option data-val="13" value="گيلان">گيلان</option>
                                                                    <option data-val="66" value="لرستان">لرستان</option>
                                                                    <option data-val="15" value="مازندران">مازندران
                                                                    </option>
                                                                    <option data-val="86" value="مرکزي">مرکزي</option>
                                                                    <option data-val="76" value="هرمزگان">هرمزگان
                                                                    </option>
                                                                    <option data-val="81" value="همدان">همدان</option>
                                                                    <option data-val="35" value="يزد">يزد</option>
                                                                </select>
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>


                                                        <li id="licity"
                                                            class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label>شهر</label>
                                                                <span class="shahr">
            <select id="shahr" name="city" data-live-search="true"
                    class="selectpicker" data-width="100%"></select>
            </span>
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>

                                                        <li id="liplak"
                                                            class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label>پلاک</label>
                                                                <input type="number" min="0" v-model="plak"
                                                                       class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>


                                                        <li id="licodeposti"
                                                            class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label> کد پستی</label>
                                                                <input v-model="codeposti" class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>


                                                        <li id="liaddress"
                                                            class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                                            <div>
                                                                <label> آدرس کامل </label>
                                                                <input v-model="fulladdress" class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>

                                                    </ul>


                                                </div>


                                            </div>


                                        </div>


                                    </div>


                                    <div class="nextitem">
        <span v-on:click="nextstep(5,'fromindex')" class="bg-primery waves-effect waves-light">
            مرحله بعد
            <i class="fas fa-chevron-left"></i>
        </span>
                                    </div>

                                </section>


                                {{-- end section 2 --}}
                                {{-- section 3 --}}


                                <section class="section3">

                                    <div style="float: right;min-height: 300px;width: 100%;background-color: white">
                                        <div style="box-shadow:none;" class="part">

                                            <div class="part-left col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">


                                                <div style="margin-top: 30px" class="part-body">
                                                    <ul>

                                                        <li class="linameuser col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label>نام</label>
                                                                <input v-model="nameuser" class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>


                                                        <li class="lifamilyuser col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label> نام خانوادگی</label>
                                                                <input v-model="familyuser" class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>


                                                        <li class="limobileuser col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <div>
                                                                <label> موبایل </label>
                                                                <input v-model="mobileuser" class="form-control">
                                                                <p class="text-error"></p>
                                                            </div>
                                                        </li>

                                                    </ul>


                                                </div>


                                            </div>


                                        </div>


                                        <div class="nextitem">
        <span v-on:click="nextstep(6,'fromindex')" class="bg-primery waves-effect waves-light">
         ثبت نهایی
         <i class="fas fa-chevron-left"></i>
        </span>
                                        </div>

                                    </div>


                                </section>

                                {{-- end section 3 --}}



                                {{--  section 4 --}}


                                <section class="section4">

                                    <div class="finalinsertrequest">

                                        <p class="tx-green">درخواست شما با موفقیت ثبت شد</p>

                                        <h4> کارشناسان ما به زودی با شما تماس خواهند گرفت</h4>
                                        <h4 class="trackingcode"></h4>
                                        <p>پشتیبانی : {{App\Setting::first()->phonecontact}}</p>
                                    </div>

                                </section>
                                {{-- end section 4 --}}


                            </div>
                        </div>
                    </div>


                </div>

                <div class="modal-footer d-flex justify-content-center">


                </div>

            </div>
        </div>
    </div>