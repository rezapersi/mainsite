
@include('header')

<!--#################################################################
 -
 -header section desktop version
 -
######################################################################### -->


<div class="view intro-2 sm-device-display-none" id="header">
    <div class="full-bg-img"style="padding-top:300px">
        <div class="">
            <div class="container">
                <div style="padding-bottom: 160px;margin-top: -65px" class="row">
                    <div class="col-md-12 mt-5 ">
                        <div class=" text-right font-weight-bold " style="color:#424242;">
                            <h2 class="h2-responsive font-weight-bold">
                                سامانه تخصصی خدمات دنیای دیجیتال
                            </h2>
                            <br>
                            <h4 class="h4-responsive font-weight-bold" style="color:#424242;">
                                سخت افزار، نرم افزار، پشتیبانی فنی آموزش و ...
                            </h4>
                        </div>
                        <div class="row text-center ">



                            <div style="padding:25px;cursor: pointer" v-on:click="defaultinsertrequest" class=" pt-3 mt-3  z-depth-0  " data-toggle="modal" data-target="#modalinsertrequest">
                                <i style="font-size: 22pt;margin-top: 0;" class="fas fa-map-marker-alt fa-3x mb-2 cyan-text"></i>
                                <p style="font-size: 11pt;color:black;cursor: pointer" class="font-weight-normal">

                                    درخواست
                                    <br>
                                    دیجی مَن
                                </p>
                            </div>
                            <div class="border-right  mr-2 ml-2 mt-5 mb-5"></div>

                            <div style="padding:25px" class=" pt-3 mt-3  z-depth-0  " >
                                <a href="/service_price">
                                <i style="font-size: 22pt;margin-top: 0;" class="fas fa-calculator fa-3x mb-2 cyan-text"></i>
                                <p style="font-size: 11pt;color:black" class="font-weight-normal">

                                    محاسبه
                                    <br>
                                    هزینه خدمات
                                </p>

                                </a>
                            </div>
                            <div class="border-right  mr-2 ml-2 mt-5 mb-5"></div>


                            <div style="padding:25px" class="pt2 mt-2  smooth-scroll">
                                <a class="z-depth-0" href="#how-it-work">
                                    <i style="font-size: 22pt;margin-top: 0;" class=" fas fa-lightbulb fa-3x mb-2 cyan-text"></i>
                                    <p style="font-size:11pt ; color:black" class="font-weight-normal ">
                                        دیجی یار
                                        <br>
                                        چطور کار میکنه؟

                                    </p>
                                </a>
                            </div>

                            <!--<div class="col-md-11 btn-reqbtn-padding">
                                    <button v-on:click="defaultinsertrequest" class=" btn  btn-cyan  request-btn mr-5" data-toggle="modal"  data-target="#modalinsertrequest">
                                        بسپارش به ما
                                    </button>   -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--#################################################################
 -
 -header section mobile version
 -
######################################################################### -->

<section class="lg-device-display-none white" style="padding-top:25px;">
    <div class="container">
        <div class="py-2 pt-4  ">
            <div class=" text-center font-weight-bold" style="color:#212121;">
                <h2 class="  font-weight-bold">
                    دیجی یار
                </h2>


                <h2 class=" text-center font-weight-bold pb-2">
                </h2>

                <h3 class="h3-responsive ">
                    سامانه تخصصی خدمات دنیای دیجیتال
                </h3>
                <h3 class=" h3-responsive ">

                </h3>
            </div>
            <div class="container mb-0">
                <div class="row">
                    <!--  <div class="col text-center">
                      <button class="btn  btn-default sm-request-btn ">
                        ثبت درخواست
                      </button>-->
                </div>
            </div>

            <!-- Search form -->
        <!--
            <div style="position: relative;" class="active-cyan form-inline text-center search-area">

                <input style="width:100%" v-model="getsearchkhadamat" class="form-control form-control-sm search-area text-center  border " type="text"  placeholder="جستجو بین خدمات..." aria-label="Search">

                <div class="contentsearch">

<ul>
    <li v-for="get in searchkhadamat">
        <a :href="'/service/'+''+get.id">

        <img :SRC="'{{\App\Setting::first()->root}}img/khadamat/'+''+get.img">
        <p>@{{get.title}}</p>
</a>
    </li>

</ul>


</div>



            </div>
            -->
        </div>
    </div>
    <br>
    <div class="col-md-12 text-center dark-grey-text d-flex justify-content-center">
        <div class="row">
            <div style="padding:25px;cursor: pointer" v-on:click="defaultinsertrequest" class=" pt-3 mt-3  z-depth-0  " data-toggle="modal" data-target="#modalinsertrequest">
                <i style="font-size: 22pt;margin-top: 0;" class="fas fa-map-marker-alt fa-3x mb-2 cyan-text"></i>
                <p style="font-size: 11pt;color:black" class="font-weight-normal">

                    درخواست
                    <br>
                    دیجی مَن
                </p>
            </div>
            <div class="border-right  mr-2 ml-2 mt-5 mb-5"></div>

            <div style="padding:25px" class=" pt-3 mt-3  z-depth-0 ">
                <a href="/service_price">
                <i style="font-size: 22pt;margin-top: 0;" class="fas fa-calculator fa-3x mb-2 cyan-text"></i>
                <p style="font-size: 11pt;color:black" class="font-weight-normal">

                    محاسبه
                    <br>
                    هزینه خدمات
                </p>
                </a>
            </div>
        </div>
        <!--
        <div class=" z-depth-0">

          <section class=" dark-grey-text">
            <div class="col-sm-12">
              <div class="row text-center d-flex justify-content-center my-5">
                <div class="col-sm-6  col-md-6 mb-4 ">
                  <i class="fas fa-tools fa-3x mb-4 grey-text"></i>
                  <p class="font-weight-normal">چه خدماتی داریم؟</p>
                  <div class=" col-md-6 col-sm-6 mb-4">
                    <i class="fas fa-dollar-sign fa-3x mb-4 grey-text"></i>
                    <p class="font-weight-normal">چند میشه؟</p>
                  </div>
                </div>
              </div>
            </div>
          </section>
  -->

    </div>







</section>
<!--Section: Content-->


<!--#################################################################

top section fetures area

#########################################################################
-->
<!--
<section class="white-text aboutus-sec sm-device-display-none">
    <div class="container  mt-0 pt-0">
        <div class="row">
            <div class="col-12">
                <div class=" z-depth-0">
                    <div class="card-body p-0">
                        <div class="row mx-0">
                            <div class="col-lg-6 grey darken-3  accent-3 rounded-right py-5 px-md-5 text-center">
                                <div class=" float-center">
                                    <img class="img-fluid" src="{{asset('images/icons/tech-support.png')}}" alt="Responsive image" style="width:100px;height:100px">
                                </div>
                                <h4 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0">
                                    پشتیبانی تلفنی
                                </h4>
                                <p class="text-justify p-responsive">
                                    مهم نیست چه زمانی از شبانه روز به کمک نیاز داشته باشید. کارشناسان ما هفت روز هفته و
                                    به صورت 24 ساعته آماده پاسخگویی به درخواست های شما هستند.
                                    ما در تلاشیم تا ضمن حفظ امنیت اطلاعات و حریم خصوصی شما مشکل شما را به صورت تلفنی و
                                    از راه دور حل کنیم.
                                </p>
                            </div>
                            <div class="col-lg-6  rounded-left py-5 px-md-5 text-center"
                                 style="background-color:#006a74;">
                                <div class=" float-center">
                                      <img class="img-fluid  " src="{{asset('images/icons/location-service.png')}}" alt="Responsive image" style="width:100px;height:100px">
                                </div>
                                <h4 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0"> اعزام دیجی مَن به محل</h4>
                                <p class="text-justify p-responsive">
                                    فرم ثبت درخواست را پر کنید، آدرس مکان مورد نظرتان برای ارائه خدمات را مشخص کنید و
                                    تمام !!. ما کارشناس مربوطه را به محل شما ارسال میکنیم و مشکل شما را در محل شما جلوی
                                    چشمان خودتان برطرف میکنیم.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
-->

<!--
#############################################################################
          top section fetures area mobile version
############################################################################
-->

<div class=" container-fluid white lg-device-display-none ">
    <!--Section: Content-->

    <section class="team-section text-center ">

        <!-- Section heading -->

        <!-- Grid row -->
        <div class="row">
            <!--Carousel Wrapper-->
            <div> <!--  <div id="multi-item-example" class="carousel  slide carousel-multi-item mb-0" data-ride="carousel"> uncomment this div for crousel activation -->

                <!--Slides-->
                <div class="carousel-inner " role="listbox">
                    <section class="repair_man-img lg-device-display-none">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="text-left ">
                                    <img class="img-fluid img-responsive" src="{{asset('/index/images/banner/digiman_1.png')}}" alt="">

                                </div>
                            </div>
                        </div>




                </div>
                <!-- Grid row -->

    </section>
    <!--Section: Content-->
</div>

<!--
<div class="pt-3   text-center">
        <button type="button" class="list-group-item list-group-item-action text-center  " style="border:0px;"> <i style="font-size: 22pt;margin-top: 0;" class=" fas fa-search fa-3x mb-2 black-text"></i> </br>جستجو بین خدمات </button>
      </div>
-->
</section>




<!--#################################################################

services category area desktop version

#########################################################################
-->

<section class="mt-0  sm-device-display-none  ">
    <div class="">
        <div class="col-lg-12 col-md-12 ">

            <div class="row-fluid flex-row">


                <h2 class="h2-responsive  text-center pb-3">
                </h2>


            </div>
        </div>
        <!--
        <div class="arrow-sec text-left lg-device-display-none ">
          <div class="arr-item ">
            <i class="fas fa-angle-double-left arrow-left fa-3x grey-text" style="  margin-left: 20px;" aria-hidden="true"></i>
          </div>
        </div>
      -->



        <div class=" aqua-gradient" style=" ; height: auto;
  width: 100%;
  white-space: nowrap;
  overflow-x : scroll;
  overflow-y: hidden;" >




            <!--auto generated card -->
            <div class="container text-center ">
                @foreach($khadamat as $row)
                    <div class=" cover-service-category-item mb-3 mt-3 z-depth-0  hoverable"  style="width:200px;height:180px; border-style:dotted;border-width:0px;border-radius:10px; border-color: #0ccdb0;background-color:white">

                        <!-- Card -->

                        <div class=" "  >

                            <a href="/service/{{$row->id}}">
                                <!-- Card image -->
                                <div class="  text-center pt-3">
                                    <img class="card-img-top  " style="height:100px;width:100px"   src="{{\App\Setting::first()->root}}img/khadamat/{{$row->img}}" alt="Card image cap">

                                    <div class="mask rgba-white-slight"></div>

                                </div>


                                <!-- Card content -->

                            </a>
                            <!-- Card footer -->
                            <div class="pb-0 text-center">

                                <!-- Button -->
                                <!--
                                <a class="mdb-color lighten-3 rounded">
                                   <h4 class="h4-responsive">
                                    بیشتر
                                    </h4>
                                    </a>
                                  -->
                                <h5 class="card-title text-center" style="color-text:white">

                                    {{$row->title}}
                                </h5>

                            </div>

                        </div>

                        <!-- Card -->


                    </div>
                @endforeach

            </div>

        </div>

    </div>


</section>




<!--#################################################################

services category area

#########################################################################
-->

<!--
<section class="mt-4 sm-device-display-none">

    <div class="container">


        <div class="col-lg-12 col-md-12 ">

            <div class="row-fluid flex-row">


                <h2 class="h2-responsive font-weight-bold ml-3  pt-2 text-center ">
                    دسته بندی خدمات
                </h2>


            </div>
        </div>
        <div class="arrow-sec text-left lg-device-display-none  " >
            <div class="arr-item ">
                <i class="fas fa-angle-double-left arrow-left fa-3x black-text" style="font-size: 30pt;"
                   aria-hidden="true"></i>
            </div>
        </div>


        <div style="min-height:250px" class="">

            <div class="owl-carousel ">

                @foreach($khadamat as $row)
    <div style="" class=" mb-3  white">

        <div class=" z-depth-0 " style=" ">
               <a href="/service/{{$row->id}}" >


                            <div class="view avatar cyan ">
                                <img style="height:230px" class="card-img  img-fluid"
                                     alt=" ">
                                <div class="mask flex-center waves-effect  aqua-cyan-light">
                                    <p class="white-text overlay"></p>
                                </div>
                            </div>



                            <div class="card-body" >

                                <h4 style="font-size: 14pt;color:black" class="card-title text-center">

                                    {{$row->title}}
            </h4>

        </div>


        <div class="rounded-bottom  mdb-color lighten-3 text-center ">


        </div>
</a>
    </div>




</div>
@endforeach


        </div>


    </div>

</div>
<hr class="">
</section>
</br>
-->



<!-- #################################################################

slider section

########################################################################
-->


<div class="white  sm-device-display-none ">


    <!--Section: Content-->
    <section class="team-section text-center dark-grey-text container">
        <!-- Section heading -->

        <h2 class="h2-responsive py-5">مزایای دیجی یار </h2>

        <!-- Grid row -->
        <div class="row">
            <div class="col-md-6  float-center">
                <div class="">
                    <!--Avatar-->
                    <div class=" image-Responsive">
                        <img src="{{asset('/index/images/banner/banner_tech_location_blue.png')}}" class="img-fluid"style="border-width:0px;border-radius:10px; ">
                    </div>
                    <!--Content-->
                    <!--Review-->
                </div>
            </div>
            <div class="col-md-6  float-center">
                <div class="">
                    <!--Avatar-->
                    <div class="image-Responsive"style="border-width:0px;border-radius:10px; ">
                        <img src="{{asset('/index/images/banner/banner_tech_vid.png')}}" class="img-fluid"style="border-width:0px;border-radius:10px; ">
                    </div>
                    <!--Content-->
                    <!--Review-->
                </div>
            </div>



        </div>

        <!-- Grid row -->

    </section>
    <!--Section: Content-->

</div>





<!-- #################################################################

slider section

########################################################################
-->


<div class="container mt-3 pt-2  white lg-device-display-none   ">


    <!--Section: Content-->
    <section class="team-section text-center dark-grey-text">
        <!-- Section heading -->

        <h2 class="h2-responsive">مزایای دیجی یار </h2>
        <hr class="float-center dark-gray" style="margin-right:130px;margin-left:130px">

        <!-- Grid row -->
        <div class="row">

            <!--Carousel Wrapper-->
            <div id="multi-item-example" class="carousel testimonial-carousel slide carousel-multi-item mb-5" data-ride="carousel">


                <!--Indicators-->

                <ol class="carousel-indicators">
                    <li data-target="#multi-item-example" data-slide-to="0" class="active light-blue darken-4"></li>
                    <li data-target="#multi-item-example" data-slide-to="1" class="light-blue darken-4"></li>
                </ol>
                <!--Indicators-->


                <!--Slides-->
                <div class="carousel-inner " role="listbox">

                    <!--First slide-->
                    <div class="carousel-item active ">

                        <!--Grid column-->

                        <div class="col-md-12  float-center">
                            <div class="">
                                <!--Avatar-->
                                <div class=" image-Responsive">
                                    <img src="{{asset('/index/images/banner/banner_tech_location_blue.png')}}" class="img-fluid"style="border-width:0px;border-radius:10px; ">
                                </div>
                                <!--Content-->
                                <!--Review-->
                            </div>
                        </div>
                        <!--Grid column-->

                    </div>
                    <!--First slide-->

                    <!--Second slide-->
                    <div class="carousel-item">

                        <!--Grid column-->

                        <div class="col-md-12  float-center">
                            <div class="">
                                <!--Avatar-->
                                <div class="image-Responsive"style="border-width:0px;border-radius:10px; ">
                                    <img src="{{asset('/index/images/banner/banner_tech_vid.png')}}" class="img-fluid"style="border-width:0px;border-radius:10px; ">
                                </div>
                                <!--Content-->
                                <!--Review-->
                            </div>
                        </div>
                        <!--Grid column-->




                    </div>
                    <!--Second slide-->


                </div>
                <!--Slides-->
                <!--Controls-->
                <div class="">
                    <a class="" href="#multi-item-example" data-slide="prev"></a>

                    <a class="" href="#multi-item-example" data-slide="next"></a>
                </div>
                <!--Controls-->
            </div>
            <!--Carousel Wrapper-->
        </div>

        <!-- Grid row -->

    </section>
    <!--Section: Content-->

    <hr class="float-center dark-gray" style="">
</div>















<!--#################################################################

services category area mobile version

#########################################################################
-->

<section class="mt-0  white lg-device-display-none ">
    <hr class="float-center white" >
    <div class="">
        <div class="col-lg-12 col-md-12 ">

            <div class="row-fluid flex-row">


                <h2 class="h2-responsive  text-center ">
                    دسته بندی خدمات
                </h2>
                <hr class="float-center dark-gray" style="margin-right:130px;margin-left:130px">


            </div>
        </div>
        <!--
        <div class="arrow-sec text-left lg-device-display-none ">
          <div class="arr-item ">
            <i class="fas fa-angle-double-left arrow-left fa-3x grey-text" style="  margin-left: 20px;" aria-hidden="true"></i>
          </div>
        </div>
      -->



        <div  style=" background-color:#01e288; height: auto;
  width: 100%;
  white-space: nowrap;
  overflow-x : scroll;
  overflow-y: hidden;" >


            <div class="cover-service-category-item mb-3 mt-3  z-depth-0 "  style="width:10px;height:180px;padding-right:0px !important;">
                <h2 class="h2-responsive text-center pt-5 ">
                    </br>

                    <i class="fas fa-angle-right fa-2x " style="color:#000000"></i>
                </h2>

            </div>


            <!--auto generated card -->

            @foreach($khadamat as $row)
                <div class=" cover-service-category-item mb-3 mt-3 z-depth-0  hoverable"  style="width:200px;height:180px; border-style:dotted;border-width:0px;border-radius:10px; border-color: #0ccdb0;background-color:white">

                    <!-- Card -->

                    <div class=" "  >

                        <a href="/service/{{$row->id}}">
                            <!-- Card image -->
                            <div class="  text-center pt-3">
                                <img class="card-img-top  " style="height:100px;width:100px"   src="{{\App\Setting::first()->root}}img/khadamat/{{$row->img}}" alt="Card image cap">

                                <div class="mask rgba-white-slight"></div>

                            </div>


                            <!-- Card content -->

                        </a>
                        <!-- Card footer -->
                        <div class="pb-0 text-center">

                            <!-- Button -->
                            <!--
                            <a class="mdb-color lighten-3 rounded">
                               <h4 class="h4-responsive">
                                بیشتر
                                </h4>
                                </a>
                              -->
                            <h5 class="card-title text-center" style="color-text:white">

                                {{$row->title}}
                            </h5>

                        </div>

                    </div>

                    <!-- Card -->


                </div>
            @endforeach

        </div>



        <hr class="float-center dark-gray" >
    </div>


</section>








<!--#################################################################

 popular dgyr services

#########################################################################
-->
<!--
  <section class="pt-4  white lg-device-display-none ">

    <div class="">
      <div class="col-lg-12 col-md-12 ">

        <div class="row-fluid flex-row">


          <h2 class="h2-responsive  text-center ">
خدمات پرطرفدار
</h2>


                                          <hr class="float-center dark-gray" style="margin-right:130px;margin-left:130px">



        </div>
      </div>





      <div class=" aqua-gradient" style=" background-color:; height: auto;
  width: 100%;
  white-space: nowrap;
  overflow-x : scroll;
  overflow-y: hidden;" >




              <div class="cover-service-category-item mb-3 mt-3  z-depth-0 "  style="width:10px;height:180px;padding-right:0px !important;">
            <h2 class="h2-responsive text-center pt-5 ">
               </br>

               <i class="fas fa-angle-right fa-2x " style="color:#000000"></i>
            </h2>

        </div>











        @foreach($khadamat as $row)
    <div class="  cover-service-category-item mb-3 mt-3 z-depth-0 mask hoverable"  style="width:200px;height:180px; border-style:dotted;border-width:0px;border-radius:10px; border-color: #0ccdb0;background-color:#ffffff">



      <div class=" "  >

        <a href="/service/{{$row->id}}">

            <div class="  text-center pt-3">

                <div class="mask rgba-white-slight"></div>

            </div>




            </a>

          <div class="pb-0 text-center">

            </div>

          </div>




        </div>
        @endforeach

        </div>



  <hr class="float-center dark-gray" >
      </div>


    </section>



  -->




<!-- #################################################################

services area num2

########################################################################
-->
<!--
@foreach($khadamat as $row)
    <section  class="mt-0  white">
        <div  class="container">
            <div class="col-lg-12 col-md-12 ">
                <div class="row-fluid flex-row">
                    <h2 class="h2-responsive font-weight-bold ml-3  pt-5 text-center ">
                        {{$row->title}}
            </h2>
        </div>
    </div>

    <div  class="arrow-sec text-left lg-device-display-none ">
        <div class="arr-item ">
            <i class="fas fa-angle-double-left arrow-left fa-3x grey-text" style="font-size: 30pt;"
               aria-hidden="true"></i>
        </div>
    </div>

    <div  class="cover-service-category-container">


    <div style="min-height: 300px;width: 100%;">

<div  class="owl-carousel">
                @foreach($row->problems as $row2)
        <div style="width: 100%" class="hoverable  cover-service-item mb-5 rounded ">

            <div class="card ">

                <div class="view">
                    <img style="height:180px" class="card-img-top " src="{{\App\Setting::first()->root}}img/service/{{$row2->img}}" alt="Card image cap">
                                    <div class="mask flex-center waves-effect waves-light aqua-cyan-light">
                                        <p class="white-text overlay"></p>
                                    </div>

                                </div>



                                <div class="card-body">

                                    <h4 style="font-size: 13pt;" class="card-title text-center">{{$row2->title}}</h4>
                                    <h4 style="font-size: 12pt;color:red" class="card-title text-center">{{number_format($row2->tarefe)}} تومان</h4>

                                </div>

                                <div class="rounded-bottom  text-center">

                                <a v-on:click="chosedservicesforinsertmodal({{$row2->id}})" style="color:#fff !important" class="btn btn-action  mdb-color darken-4 rounded  red"> ثبت سفارش</a>
                                </div>

                            </div>

                        </div>
                        @endforeach
            </div>

    </div>

    </div>


</div>
</section>
@endforeach
        -->


<!--########################################################################
why our services
#############################################################################-->


<section style="float: right;width: 100%"  class="black-text aboutus-sec mb-0  pt-5  sm-device-display-none white ">

    <hr style="margin-left:600px;margin-right:600px">
    <!-- Grid row -->
    <div  class="container mt-0 pt-0  ">
        <div class="row">
            <div class="col-12">
                <h2 class="h2-responsiv text-center py-2">
                </h2>

                <div class="card-body p-0  ">
                    <div class="row mx-0" style="margin-bottom: 25px">
                        <div style="box-shadow: 0 0 1px 1px #eee;" class="col-lg-4 hoverable  rounded-left py-5 px-md-5 text-center  ">
                            <div class=" float-center">
                                <i class="fas fa-file-invoice-dollar mb-1 "
                                   style="font-size:50px;color:#00bcd4"></i>
                            </div>
                            <h4 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#00bcd4">
                                تعرفه شفاف </h4>
                            <hr style="margin-left:100px; margin-right:100px">
                            <p class="text-justify p-responsiv " style="color:#666">
                                هیچ هزینه اضافه ای نمی پردازید !
                                ما ابزاری را برای شما طراحی کرده ایم که  با استفاده از آن  به سادگی قبل از انجام هر کاری، می توانید هزینه تعمیر دستگاهتان را مطابق نرخنامه اتحادیه های مربوطه محاسبه کنید.
                            </p>

                            <button href="/service_price" type="button" class="btn btn-red rounded">محاسبه هزینه </button>

                        </div>
                        <div style="box-shadow: 0 0 20px 1px #eee;" class="col-lg-4  py-5 rounded-right  px-md-5 text-center hoverable  ">
                            <div class=" float-center">
                                <i class="fas fa-award mb-1" style="font-size:85px;color:#aa8d24"></i>
                            </div>
                            <h4 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#aa8d24">
                                گارانتی خدمات
                            </h4>
                            <hr style="margin-left:100px; margin-right:100px">
                            <p class="text-justify p-responsive font-weight-regular" style="color:#666">
                                با آرامش خاطر دستگاهتان را به ما بسپارید. ما همواره در تلاشیم تا با ارائه بالاترین سطح
                                خدمات پاسخگوی اعتماد شما باشیم. از این رو با تکیه بر دانش و تخصص کارشناسانمان، تمامی
                                خدمات دیجی یار با ضمانت یک ماهه و پشتیبانی رایگان ارائه می
                                شوند. </p>
                        </div>
                        <div style="box-shadow: 0 0 1px 1px #eee;" class="col-lg-4 hoverable  rounded-left py-5 px-md-5 text-center  ">
                            <div class=" float-center">
                                <i class="fas fa-user mb-1 " style="font-size:50px;color:#00bcd4"></i>
                            </div>
                            <h4 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#00bcd4">
                                کارشناسان مجرب
                            </h4>
                            <hr style="margin-left:100px; margin-right:100px">
                            <p class="text-justify p-responsive font-weight-regular " style="color:#666">
                                تخصص فقط یکی از ویژگی های تعمیرکاران ماست !
                                همه کارشناسان دیجی یار، با دیدن آموزش های تخصصی آماده حل مشکل شما به صورت حضوری و غیرحضوری هستند.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Grid row -->


</section>

<!--########################################################################
why our services mobile version
#############################################################################-->

<section  class="black-text white mb-0  pt-3  lg-device-display-none ">

    <!-- Grid row -->
    <div  class="container mt-0 pt-0 ">
        <div class="row">
            <div class="col-12">
                <hr class="float-center dark-gray" style="margin-right:130px;margin-left:130px">

                <h2 class="h2-responsive text-center">
                    چرا دیجی یار؟
                </h2>
                <hr class="float-center dark-gray" style="margin-right:130px;margin-left:130px">

                <div class="card-body p-0  ">
                    <div class="row mx-0">

                        <div style="box-shadow: 0 0 1px 1px #eee;margin-bottom: 5px" class="col-lg-4 hoverable  rounded  py-5 px-md-5 text-center  ">
                            <div class=" float-center">
                                <i class="fas fa-file-invoice-dollar mb-1" style="font-size:40px;color:#00bcd4 "></i>
                            </div>
                            <h5 class="h5-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#00bcd4">
                                تعرفه شفاف </h5>
                                <p class="text-justify p-responsiv text-muted" style="color:#212121">
                                    هیچ هزینه اضافه ای نمی پردازید. تعرفه های دیجی یار بصورت شفاف در دسترس شما هستند ، و ما
                                    ابزاری را برای شما طراحی کرده ایم که با استفاده از آن به راحتی می توانید حدود قیمت خدمات
                                    مورد نظرتان را طبق تعرفه دیجی یار قبل از انجام هر
                                    کاری محاسبه کنید. </p>

                            <button href="/service_price" type="button" class="btn btn-red rounded">محاسبه هزینه </button>

                        </div>

                        <div style="box-shadow: 0 0 1px 1px #eee;margin-bottom: 5px" class="col-lg-4  py-5 rounded  px-md-5 text-center hoverable  ">
                            <div class=" float-center">
                                <i class="fas fa-award mb-1" style="font-size:85px;color:#aa8d24"></i>
                            </div>
                            <h5 class="h5-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#aa8d24">
                                گارانتی خدمات
                            </h5>
                            <p class="text-justify p-responsive  text-muted" style="color:#212121">
                                با آرامش خاطر دستگاهتان را به ما بسپارید. ما همواره در تلاشیم تا با ارائه بالاترین سطح
                                خدمات پاسخگوی اعتماد شما باشیم. از این رو با تکیه بر دانش و تخصص کارشناسانمان، تمامی
                                خدمات دیجی یار با ضمانت یک ماهه و پشتیبانی رایگان ارائه می
                                شوند. </p>
                        </div>

                        <div style="box-shadow: 0 0 1px 1px #eee;margin-bottom: 15px" class="col-lg-4 hoverable  rounded py-5 px-md-5 text-center ">
                            <div class=" float-center">
                                <i class="fas fa-user mb-1 " style="font-size:40px;color:#00bcd4"></i>
                            </div>
                            <h5 class="h4-responsive font-weight-bold ml-3 mb-3 pb-0" style="color:#00bcd4">
                                کارشناسان مجرب
                            </h5>
                            <p class="text-justify p-responsive  text-muted " style="color:#212121">
                                تخصص فقط یکی از ویژگی های تعمیرکاران ماست ! همه کارشناسان دیجی یار، با دیدن آموزش های تخصصی آماده حل مشکل شما به صورت حضوری و غیرحضوری هستند.
                            </p>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Grid row -->


</section>










<!-- ##############################################################################
                                how DGYR Work
#################################################################################-->
<!--
<div class="pt-5 pb-5 " id="how-it-work">
    <div class="container">
        <div class="col-md-12">

            <h2 class="h2-responsiv text-center py-2 black-text">
                <i class="fas fa-lightbulb fa-3x mb-2 float-center" style="color:#ffbb33"></i>
                <br>
                    فقط چند کلیک تا ارسال کمک
                !!
            </h2>

            <ul class="nav nav-tabs nav-justified md-tabs cyan" id="myTabJust" role="tablist">
                <li class="nav-item">
                    <a class="nav-link  " id="home-tab-just" data-toggle="tab" href="#home-just" role="tab"
                       aria-controls="home-just" aria-selected="true">

                        <div class="">
                            <h2 class="h2-responsiv">
                                1
                                </h2>
                        </div>
                        ثبت درخواست
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" id="profile-tab-just" data-toggle="tab" href="#profile-just" role="tab"
                       aria-controls="profile-just" aria-selected="false">
                        <div class="">
                            <h2 class="h2-responsiv">
                                2
                                </h2>
                        </div>
                        بررسی درخواست
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" id="contact-tab-just" data-toggle="tab" href="#contact-just" role="tab"
                       aria-controls="contact-just" aria-selected="false">
                        <div class="">
                            <h2 class="h2-responsiv">
                                3
                                </h2>
                        </div>
                        حل مشکل شما
                    </a>
                </li>
            </ul>

            <div class="tab-content card pt-5 text-justify" id="myTabContentJust">
                <div class="tab-pane fade show active" id="home-just" role="tabpanel" aria-labelledby="home-tab-just">
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، </p>
                    </p>


                </div>
                <div class="tab-pane fade" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-just">
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، </p>
                    </p>
                </div>
                <div class="tab-pane fade" id="contact-just" role="tabpanel" aria-labelledby="contact-tab-just">
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، </p>

                    </p>


                    <div class=" pt-3 mt-3 btn   z-depth-0 float-left pl-5 ml-5" data-toggle="modal"
                         data-target="#qiuck-req-modal">
                        <i class=" fas fa-smile fa-3x mb-2 cyan-text"></i>
                        <p class="font-weight-normal">

                            همین الان

                            شروع کن
                        </p>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
-->



<!--########################################################################
                  FAQ
#############################################################################-->


<div class="sectionquestiondesktop" style="  background: url({{asset('/images/question.jpg')}}) no-repeat;"  >

    <section class="container">
        <h6 class="font-weight-normal text-uppercase font-small grey-text mb-3 text-center"></h6>
        <!-- Section heading -->
        <h4 style="color: white !important;text-shadow: 0px 0px 19px white;" class="font-weight-bold black-text mb-3  pt-5 text-center">سوالات متداول </h4>
        <!-- Section description -->
        <p class="lead text-muted mx-auto  mb-5 text-center"></p>
        <!-- Accordion card -->

            <div  class="col-md-12 col-lg-10 mx-auto mb-5">
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingOne1">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    دیجی یار چیست؟
                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                دیجی یار سامانه تخصصی ارائه خدمات نرم افزاری و سخت افزاری دستگاهای دیجیتال در محل است، ما به پشتوانه شبکه متخصیص خود، جهت ارتقاء سطح کیفت خدمات این حوزه گام برداشته ایم.
                            </div>
                        </div>

                    </div>


                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingTwo2">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    دیجی یار چه خدماتی ارائه میدهد ؟
                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                دیحی یار خدمات سخت افزاری و نرم افزاری  مرتبط به لپ تاپ ، کامپیوتر ، موبایل،  تبلت ، دستگاه های صوتی و تصویری از قبیل تعمیرات موبایل ، تعمیرات لپ تاپ ،  تعمیرات کامپیوتر و تعمیرات تلویزیون LED ، LCD ، PLASMAرا انجام میدهد . برخی از این خدمات عبارت اند از :
                                تعمیرات تخصصی گوشی های اپل ، سامسونگ ، هوآوی ، آنر و ...
                                خدمات موبایل از قبیل ( آبخوردگی موبایل ، ضربه خوردگی موبایل ، خاموش شدن دستگاه وحل مشکلات نرم افزاری موبایل و تبلت و ... )
                                خدمات لپ تاپ و کامپیوتر از قبیل تعمیرات تخصصی لپتاپ ،تعمیر مادربرد لپتاپ ،تعمیر هارد لپتاپ ، و ...
                                خدمات تلویزیون های LED  LCD PLASMA   از قبیل تعمیر بک لایت دستگاه نداشتن تصویر تلویزیون تعمیر برد پاور و تعمیر main برد دستگاه تعمیرپنل و ...
                            </div>
                        </div>
                    </div>


                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingTwo3">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo3" aria-expanded="false" aria-controls="collapseTwo3">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    آیا خدمات دیجی یار همراه با گارانتی ارائه میشوند؟

                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo3" class="collapse" role="tabpanel" aria-labelledby="headingTwo3" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                تمامی خدمات دیجی یار همراه با گارانتی و پشتیبانی رایگان یک ماهه انجام میشوند .


                            </div>
                        </div>

                    </div>



                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingTwo4">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo4" aria-expanded="false" aria-controls="collapseTwo4">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    دیجی یار چه مزایایی دارد؟
                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo4" class="collapse" role="tabpanel" aria-labelledby="headingTwo4" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                هدف اصلی ما تعمیر دستگاه شما تا جای ممکن همراه با بالاترین کیفت و استاندارهای لازم جهت کاهش هزینه های شما می باشد علاوه بر این موضوع که وظیفه حرفه ای دیجی یار است ما سیستم قیمت گذاری آنلاینی را طراحی کرده ایم تا با توجه به هزینه های مصوب اتحادیه های مربوطه مشتریانمان را قبل از انجام هر کاری ازهزینه تعمیر دستگاهشان مطلع کنیم .

                            </div>
                        </div>

                    </div>




                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingTwo5">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    در چه شهرهایی میتونم از خدمات دیجی یار استفاده کنم؟

                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo5" class="collapse" role="tabpanel" aria-labelledby="headingTwo5" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                در حال حاضر خدمات حضوری دیجی یار در تمام مناطق شهر تهران فعال میباشد و ما به صورت غیر حضوری میزبان شما عزیزان از سراسر ایران بزرگ هستیم.
                            </div>
                        </div>

                    </div>



                    <!-- Accordion card -->
                    <div class="card border-top border-bottom-0 border-left border-right border-light">

                        <!-- Card header -->
                        <div class="card-header border-bottom border-light" role="tab" id="headingTwo6">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo6" aria-expanded="false" aria-controls="collapseTwo6">
                                <h6 class="black-text font-weight-normal mb-0 text-right">
                                    آیا باید قبل از ثبت درخواست هزینه ای پرداخت کنم؟
                                    <i class="fas fa-angle-down rotate-icon float-left"></i>
                                </h6>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo6" class="collapse" role="tabpanel" aria-labelledby="headingTwo6" data-parent="#accordionEx">
                            <div class="card-body text-right">
                                خیر هزینه خدمات دیجی یار پس از انجام کار و رضایت مشتریان عزیز دریافت میشود .
                            </div>
                        </div>

                    </div>


                    {{--<!--Accordion wrapper-->--}}
                    {{--<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">--}}

                        <!-- Accordion card -->
                        <div class="card border-top border-bottom-0 border-left border-right border-light">

                            <!-- Card header -->
                            <div class="card-header border-bottom border-light" role="tab" id="headingTwo7">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo7" aria-expanded="false" aria-controls="collapseTwo7">
                                    <h6 class="black-text font-weight-normal mb-0 text-right">
                                        تعرفه های خدمات  دیجی یار چطور محاسبه میشوند ؟

                                        <i class="fas fa-angle-down rotate-icon float-left"></i>
                                    </h6>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseTwo7" class="collapse" role="tabpanel" aria-labelledby="headingTwo7" data-parent="#accordionEx">
                                <div class="card-body text-right">
                                    تمام هزینه ها و تعرفه های خدمات دیجی یار برار با نرخ تعرفه های مصوب و رسمی اتحادیه های مربوطه محاسبه میشوند.

                                </div>
                            </div>

                        </div>



                        <!-- Accordion wrapper -->

                    {{--</div>--}}


                </div>
            </div>

    </section>

</div>







<!--########################################################################
                  Blog section
#############################################################################-->


<!--blog section -->
<!--
<div class="container mt-5 ">


    <section class="">

        <h3 class="text-center font-weight-bold mb-5">ترفندستان</h3>


        <div class="row">



            @foreach($postblog as $row)

    <div class="col-lg-4 col-md-12 mb-lg-0 mb-4">

        <div class="card hoverable">

            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/project4.jpg"
                 alt="Card image cap">

            <div class="card-body text-justify">

                <a href="#!" class="black-text ">{{$row->post_title}}</a>

                            <p style="height: 130px;overflow: hidden;"
                               class="card-title text-muted font-small mt-3 mb-2">{{$row->content}}</p>

                            <button type="button" class="btn btn-flat text-primary p-0 mx-0">بیشتر<i
                                        class="fa fa-angle-left ml-2"></i></button>

                        </div>

                    </div>


                </div>

            @endforeach


        </div>


        <div class="text-center mt-5 mb-5 hoverable">
            <hr class="mr-5 ml-5">

            <a href="#/blog">
                همه مقالات
            </a>
            <hr class="mr-5 ml-5">
        </div>

    </section>


</div>
-->

@include('footer')
