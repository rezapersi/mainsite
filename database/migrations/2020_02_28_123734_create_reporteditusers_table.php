<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReporteditusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporteditusers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('date', 255)->nullable();
            $table->string('beforemobile', 255)->nullable();
            $table->string('newmobile', 255)->nullable();
            $table->string('beforeemail', 255)->nullable();
            $table->string('newemail', 255)->nullable();


            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');



        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporteditusers');
    }
}
